<?php

namespace App\Events\Backend\Airports;

use Illuminate\Queue\SerializesModels;

/**
 * Class AirportDeleted.
 */
class AirportDeleted
{
    use SerializesModels;

    /**
     * @var
     */
    public $airport;

    /**
     * @param $airport
     */
    public function __construct($airport)
    {
        $this->airport = $airport;
    }
}
