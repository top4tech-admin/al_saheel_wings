<?php

namespace App\Events\Backend\Currencys;

use Illuminate\Queue\SerializesModels;

/**
 * Class PageCreated.
 */
class CurrencyCreated
{
    use SerializesModels;

    /**
     * @var
     */
    public $currency;

    /**
     * @param $currency
     */
    public function __construct($currency)
    {
        $this->currency = $currency;
    }
}
