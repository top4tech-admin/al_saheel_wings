<?php

namespace App\Events\Backend\Flights;

use Illuminate\Queue\SerializesModels;

/**
 * Class PageCreated.
 */
class FlightCreated
{
    use SerializesModels;

    /**
     * @var
     */
    public $flight;

    /**
     * @param $flight
     */
    public function __construct($flight)
    {
        $this->flight = $flight;
    }
}
