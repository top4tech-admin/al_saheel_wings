<?php

namespace App\Events\Backend\Flights;

use Illuminate\Queue\SerializesModels;

/**
 * Class PageDeleted.
 */
class FlightDeleted
{
    use SerializesModels;

    /**
     * @var
     */
    public $flight;

    /**
     * @param $flight
     */
    public function __construct($flight)
    {
        $this->flight = $flight;
    }
}
