<?php

namespace App\Events\Backend\Flights;

use Illuminate\Queue\SerializesModels;

/**
 * Class PageUpdated.
 */
class FlightUpdated
{
    use SerializesModels;

    /**
     * @var
     */
    public $flight;

    /**
     * @param $flight
     */
    public function __construct($flight)
    {
        $this->flight = $flight;
    }
}
