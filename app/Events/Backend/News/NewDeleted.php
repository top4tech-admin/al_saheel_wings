<?php

namespace App\Events\Backend\News;

use Illuminate\Queue\SerializesModels;

/**
 * Class NewDeleted.
 */
class NewDeleted
{
    use SerializesModels;

    /**
     * @var
     */
    public $new;

    /**
     * @param $new
     */
    public function __construct($new)
    {
        $this->new = $new;
    }
}
