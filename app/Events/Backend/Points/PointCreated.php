<?php

namespace App\Events\Backend\Points;

use Illuminate\Queue\SerializesModels;

/**
 * Class PointCreated.
 */
class PointCreated
{
    use SerializesModels;

    /**
     * @var
     */
    public $point;

    /**
     * @param $point
     */
    public function __construct($point)
    {
        $this->point = $point;
    }
}
