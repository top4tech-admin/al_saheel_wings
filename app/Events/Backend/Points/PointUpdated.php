<?php

namespace App\Events\Backend\Points;

use Illuminate\Queue\SerializesModels;

/**
 * Class PointUpdated.
 */
class PointUpdated
{
    use SerializesModels;

    /**
     * @var
     */
    public $point;

    /**
     * @param $point
     */
    public function __construct($point)
    {
        $this->point = $point;
    }
}
