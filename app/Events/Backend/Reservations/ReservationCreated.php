<?php

namespace App\Events\Backend\Reservations;

use Illuminate\Queue\SerializesModels;

/**
 * Class ReservationCreated.
 */
class ReservationCreated
{
    use SerializesModels;

    /**
     * @var
     */
    public $reservation;

    /**
     * @param $reservation
     */
    public function __construct($reservation)
    {
        $this->reservation = $reservation;
    }
}
