<?php

namespace App\Events\Backend\Reservations;

use Illuminate\Queue\SerializesModels;

/**
 * Class ReservationDeleted.
 */
class ReservationDeleted
{
    use SerializesModels;

    /**
     * @var
     */
    public $reservation;

    /**
     * @param $reservation
     */
    public function __construct($reservation)
    {
        $this->reservation = $reservation;
    }
}
