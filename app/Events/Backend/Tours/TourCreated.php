<?php

namespace App\Events\Backend\Tours;

use Illuminate\Queue\SerializesModels;

/**
 * Class TourCreated.
 */
class TourCreated
{
    use SerializesModels;

    /**
     * @var
     */
    public $tour;

    /**
     * @param $tour
     */
    public function __construct($tour)
    {
        $this->tour = $tour;
    }
}
