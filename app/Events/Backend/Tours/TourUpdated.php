<?php

namespace App\Events\Backend\Tours;

use Illuminate\Queue\SerializesModels;

/**
 * Class TourUpdated.
 */
class TourUpdated
{
    use SerializesModels;

    /**
     * @var
     */
    public $tour;

    /**
     * @param $tour
     */
    public function __construct($tour)
    {
        $this->tour = $tour;
    }
}
