<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\Backend\Partners\DeletePartnerRequest;
use App\Http\Requests\Backend\Partners\ManagePartnerRequest;
use App\Http\Requests\Backend\Partners\StorePartnerRequest;
use App\Http\Requests\Backend\Partners\UpdatePartnerRequest;
use App\Http\Resources\PartnersResource;
use App\Models\Partner;
use App\Repositories\Backend\PartnersRepository;
use Illuminate\Http\Response;

/**
 * @group Partners Management
 *
 * Class PartnersController
 *
 * APIs for Partners Management
 *
 * @authenticated
 */
class PartnersController extends APIController
{
    /**
     * Repository.
     *
     * @var PartnersRepository
     */
    protected $repository;

    /**
     * __construct.
     *
     * @param $repository
     */
    public function __construct(PartnersRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Get all Partners.
     *
     * This endpoint provides a paginated list of all partners. You can customize how many records you want in each
     * returned response as well as sort records based on a key in specific order.
     *
     * @queryParam partner Which partner to show. Example: 12
     * @queryParam per_partner Number of records per partner. (use -1 to retrieve all) Example: 20
     * @queryParam order_by Order by database column. Example: created_at
     * @queryParam order Order direction ascending (asc) or descending (desc). Example: asc
     *
     * @responseFile status=401 scenario="api_key not provided" responses/unauthenticated.json
     * @responseFile responses/partner/partner-list.json
     *
     * @param \Illuminate\Http\ManagePartnerRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(ManagePartnerRequest $request)
    {
        $collection = $this->repository->retrieveList($request->all());

        return PartnersResource::collection($collection);
    }

    /**
     * Gives a specific Partner.
     *
     * This endpoint provides you a single Partner
     * The Partner is identified based on the ID provided as url parameter.
     *
     * @urlParam id required The ID of the Partner
     *
     * @responseFile status=401 scenario="api_key not provided" responses/unauthenticated.json
     * @responseFile responses/partner/partner-show.json
     *
     * @param ManagePartnerRequest $request
     * @param \App\Models\Partner $partner
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(ManagePartnerRequest $request, Partner $partner)
    {
        return new PartnersResource($partner);
    }

    /**
     * Create a new Partner.
     *
     * This endpoint lets you create new Partner
     *
     * @responseFile status=401 scenario="api_key not provided" responses/unauthenticated.json
     * @responseFile status=201 responses/partner/partner-store.json
     *
     * @param \App\Http\Requests\Backend\Partners\StorePartnerRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StorePartnerRequest $request)
    {
        $partner = $this->repository->create($request->validated());

        return (new PartnersResource($partner))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Update Partner.
     *
     * This endpoint allows you to update existing Partner with new data.
     * The Partner to be updated is identified based on the ID provided as url parameter.
     *
     * @urlParam id required The ID of the Partner
     *
     * @responseFile status=401 scenario="api_key not provided" responses/unauthenticated.json
     * @responseFile responses/partner/partner-update.json
     *
     * @param \App\Models\Partner $partner
     * @param \App\Http\Requests\Backend\Partners\UpdatePartnerRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdatePartnerRequest $request, Partner $partner)
    {
        $partner = $this->repository->update($partner, $request->validated());

        return new PartnersResource($partner);
    }

    /**
     * Delete Partner.
     *
     * This endpoint allows you to delete a Partner
     * The Partner to be deleted is identified based on the ID provided as url parameter.
     *
     * @urlParam id required The ID of the Partner
     *
     * @responseFile status=401 scenario="api_key not provided" responses/unauthenticated.json
     * @responseFile status=204 scenario="When the record is deleted" responses/partner/partner-destroy.json
     *
     * @param \App\Models\Partner $partner
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(DeletePartnerRequest $request, Partner $partner)
    {
        $this->repository->delete($partner);

        return response()->noContent();
    }
}
