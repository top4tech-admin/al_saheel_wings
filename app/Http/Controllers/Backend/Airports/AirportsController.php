<?php

namespace App\Http\Controllers\Backend\Airports;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Airports\CreateAirportRequest;
use App\Http\Requests\Backend\Airports\DeleteAirportRequest;
use App\Http\Requests\Backend\Airports\EditAirportRequest;
use App\Http\Requests\Backend\Airports\ManageAirportRequest;
use App\Http\Requests\Backend\Airports\StoreAirportRequest;
use App\Http\Requests\Backend\Airports\UpdateAirportRequest;
use App\Http\Responses\Backend\Airport\EditResponse;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Models\Airport;
use App\Repositories\Backend\AirportsRepository;
use Illuminate\Support\Facades\View;

class AirportsController extends Controller
{
    /**
     * @var \App\Repositories\Backend\AirportsRepository
     */
    protected $repository;

    /**
     * @param \App\Repositories\Backend\AirportsRepository $repository
     */
    public function __construct(AirportsRepository $repository)
    {
        $this->repository = $repository;
        View::share('js', ['airports']);
    }

    /**
     * @param \App\Http\Requests\Backend\Airports\ManageAirportRequest $request
     *
     * @return \App\Http\Responses\ViewResponse
     */
    public function index(ManageAirportRequest $request)
    {
        return new ViewResponse('backend.airports.index');
    }

    /**
     * @param \App\Http\Requests\Backend\Airports\CreateAirportRequest $request
     *
     * @return \App\Http\Responses\ViewResponse
     */
    public function create(CreateAirportRequest $request)
    {
        return new ViewResponse('backend.airports.create');
    }

    /**
     * @param \App\Http\Requests\Backend\Airports\StoreAirportRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function store(StoreAirportRequest $request)
    {
        $this->repository->create($request->except(['_token', '_method']));

        return new RedirectResponse(route('admin.airports.index'), ['flash_success' => __('alerts.backend.pages.created')]);
    }

    /**
     * @param \App\Models\Airport $page
     * @param \App\Http\Requests\Backend\Airports\EditAirportRequest $request
     *
     * @return \App\Http\Responses\Backend\Blog\EditResponse
     */
    public function edit(Airport $airport, EditAirportRequest $request)
    {
        return new EditResponse($airport);
    }

    /**
     * @param \App\Models\Airport $page
     * @param \App\Http\Requests\Backend\Airports\UpdateAirportRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(Airport $airport, UpdateAirportRequest $request)
    {
        $this->repository->update($airport, $request->except(['_token', '_method']));

        return new RedirectResponse(route('admin.airports.index'), ['flash_success' => __('alerts.backend.pages.updated')]);
    }

    /**
     * @param \App\Models\Airport $page
     * @param \App\Http\Requests\Backend\Airports\DeleteAirportRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function destroy(Airport $page, DeleteAirportRequest $request)
    {
        $this->repository->delete($page);

        return new RedirectResponse(route('admin.airports.index'), ['flash_success' => __('alerts.backend.pages.deleted')]);
    }
}
