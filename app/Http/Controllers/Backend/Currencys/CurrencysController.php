<?php

namespace App\Http\Controllers\Backend\Currencys;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Currencys\CreateCurrencyRequest;
use App\Http\Requests\Backend\Currencys\DeleteCurrencyRequest;
use App\Http\Requests\Backend\Currencys\EditCurrencyRequest;
use App\Http\Requests\Backend\Currencys\ManageCurrencyRequest;
use App\Http\Requests\Backend\Currencys\StoreCurrencyRequest;
use App\Http\Requests\Backend\Currencys\UpdateCurrencyRequest;
use App\Http\Responses\Backend\Currency\EditResponse;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Models\Currency;
use App\Repositories\Backend\CurrencysRepository;
use Illuminate\Support\Facades\View;

class CurrencysController extends Controller
{
    /**
     * @var \App\Repositories\Backend\CurrencysRepository
     */
    protected $repository;

    /**
     * @param \App\Repositories\Backend\CurrencysRepository $repository
     */
    public function __construct(CurrencysRepository $repository)
    {
        $this->repository = $repository;
        View::share('js', ['currencys']);
    }

    /**
     * @param \App\Http\Requests\Backend\Currencys\ManageCurrencyRequest $request
     *
     * @return \App\Http\Responses\ViewResponse
     */
    public function index(ManageCurrencyRequest $request)
    {
        return new ViewResponse('backend.currencys.index');
    }

    /**
     * @param \App\Http\Requests\Backend\Currencys\CreateCurrencyRequest $request
     *
     * @return \App\Http\Responses\ViewResponse
     */
    public function create(CreateCurrencyRequest $request)
    {
        return new ViewResponse('backend.currencys.create');
    }

    /**
     * @param \App\Http\Requests\Backend\Currencys\StoreCurrencyRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function store(StoreCurrencyRequest $request)
    {
        $this->repository->create($request->except(['_token', '_method']));

        return new RedirectResponse(route('admin.currencys.index'), ['flash_success' => __('alerts.backend.pages.created')]);
    }

    /**
     * @param \App\Models\Currency $currency
     * @param \App\Http\Requests\Backend\Currencys\EditCurrencyRequest $request
     *
     * @return \App\Http\Responses\Backend\Blog\EditResponse
     */
    public function edit(Currency $currency, EditCurrencyRequest $request)
    {
        return new EditResponse($currency);
    }

    /**
     * @param \App\Models\Currency $currency
     * @param \App\Http\Requests\Backend\Currencys\UpdateCurrencyRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(Currency $currency, UpdateCurrencyRequest $request)
    {
        $this->repository->update($currency, $request->except(['_token', '_method']));

        return new RedirectResponse(route('admin.currencys.index'), ['flash_success' => __('alerts.backend.pages.updated')]);
    }

    /**
     * @param \App\Models\Currency $currency
     * @param \App\Http\Requests\Backend\Currencys\DeleteCurrencyRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function destroy(Currency $currency, DeleteCurrencyRequest $request)
    {
        $this->repository->delete($currency);

        return new RedirectResponse(route('admin.currencys.index'), ['flash_success' => __('alerts.backend.pages.deleted')]);
    }
}
