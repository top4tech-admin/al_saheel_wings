<?php

namespace App\Http\Controllers\Backend\Flights;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Flights\CreateFlightRequest;
use App\Http\Requests\Backend\Flights\DeleteFlightRequest;
use App\Http\Requests\Backend\Flights\EditFlightRequest;
use App\Http\Requests\Backend\Flights\ManageFlightRequest;
use App\Http\Requests\Backend\Flights\StoreFlightRequest;
use App\Http\Requests\Backend\Flights\UpdateFlightRequest;
use App\Http\Responses\Backend\Flight\EditResponse;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Models\Flight;
use App\Models\Airport;
use App\Repositories\Backend\FlightsRepository;
use Illuminate\Support\Facades\View;

class FlightsController extends Controller
{
    /**
     * @var \App\Repositories\Backend\FlightsRepository
     */
    protected $repository;

    /**
     * @param \App\Repositories\Backend\FlightsRepository $repository
     */
    public function __construct(FlightsRepository $repository)
    {
        $this->repository = $repository;
        View::share('js', ['flights']);
    }

    /**
     * @param \App\Http\Requests\Backend\Flights\ManageFlightRequest $request
     *
     * @return \App\Http\Responses\ViewResponse
     */
    public function index(ManageFlightRequest $request)
    {
        return new ViewResponse('backend.flights.index');
    }

    /**
     * @param \App\Http\Requests\Backend\Flights\CreateFlightRequest $request
     *
     * @return \App\Http\Responses\ViewResponse
     */
    public function create(CreateFlightRequest $request)
    {
        $airports = Airport::all();
        return new ViewResponse('backend.flights.create',['airports' => $airports]);
    }

    /**
     * @param \App\Http\Requests\Backend\Flights\StoreFlightRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function store(StoreFlightRequest $request)
    {
        $this->repository->create($request->except(['_token', '_method']));

        return new RedirectResponse(route('admin.flights.index'), ['flash_success' => __('alerts.backend.pages.created')]);
    }

    /**
     * @param \App\Models\Flight $flight
     * @param \App\Http\Requests\Backend\Flights\EditFlightRequest $request
     *
     * @return \App\Http\Responses\Backend\Blog\EditResponse
     */
    public function edit(Flight $flight, EditFlightRequest $request)
    {
        return new EditResponse($flight);
    }

    /**
     * @param \App\Models\Flight $flight
     * @param \App\Http\Requests\Backend\Flights\UpdateFlightRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(Flight $flight, UpdateFlightRequest $request)
    {
        $this->repository->update($flight, $request->except(['_token', '_method']));

        return new RedirectResponse(route('admin.flights.index'), ['flash_success' => __('alerts.backend.pages.updated')]);
    }

    /**
     * @param \App\Models\Flight $flight
     * @param \App\Http\Requests\Backend\Flights\DeleteFlightRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function destroy(Flight $flight, DeleteFlightRequest $request)
    {
        $this->repository->delete($flight);

        return new RedirectResponse(route('admin.flights.index'), ['flash_success' => __('alerts.backend.pages.deleted')]);
    }
}
