<?php

namespace App\Http\Controllers\Backend\Flights;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Flights\ManageFlightRequest;
use App\Repositories\Backend\FlightsRepository;
use Yajra\DataTables\Facades\DataTables;

class FlightsTableController extends Controller
{
    /**
     * @var \App\Repositories\Backend\FlightRepository
     */
    protected $repository;

    /**
     * @param \App\Repositories\Backend\FlightRepository $repository
     */
    public function __construct(FlightsRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param \App\Http\Requests\Backend\Pages\ManageFlightRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageFlightRequest $request)
    {
        return Datatables::of($this->repository->getForDataTable())
            // ->filterColumn('status', function ($query, $keyword) {
            //     if (in_array(strtolower($keyword), ['active', 'inactive'])) {
            //         $query->where('pages.status', (strtolower($keyword) == 'active') ? 1 : 0);
            //     }
            // })
            // ->filterColumn('created_by', function ($query, $keyword) {
            //     $query->whereRaw('users.first_name like ?', ["%{$keyword}%"]);
            // })
            // ->editColumn('status', function ($page) {
            //     return $page->status_label;
            // })
            ->editColumn('created_at', function ($page) {
                return $page->created_at->toDateString();
            })
            ->editColumn('from_airport_id', function ($flight) {
                return $flight->fromAirport->name_ar;
            })
            ->editColumn('to_airport_id', function ($flight) {
                return $flight->toAirport->name_ar;
            })
            ->addColumn('actions', function ($page) {
                return $page->action_buttons;
            })
            ->escapeColumns(['title'])
            ->make(true);
    }
}
