<?php

namespace App\Http\Controllers\Backend\News;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Pages\CreatePageRequest;
use App\Http\Requests\Backend\Pages\DeletePageRequest;
use App\Http\Requests\Backend\Pages\EditPageRequest;
use App\Http\Requests\Backend\Pages\ManagePageRequest;
use App\Http\Requests\Backend\Pages\StorePageRequest;
use App\Http\Requests\Backend\Pages\UpdatePageRequest;
use App\Http\Responses\Backend\News\EditResponse;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Models\News;
use App\Repositories\Backend\NewsRepository;
use Illuminate\Support\Facades\View;

class NewsController extends Controller
{
    /**
     * @var \App\Repositories\Backend\NewsRepository
     */
    protected $repository;

    /**
     * @param \App\Repositories\Backend\NewsRepository $repository
     */
    public function __construct(NewsRepository $repository)
    {
        $this->repository = $repository;
        View::share('js', ['news']);
    }

    /**
     * @param \App\Http\Requests\Backend\News\ManagePageRequest $request
     *
     * @return \App\Http\Responses\ViewResponse
     */
    public function index(ManagePageRequest $request)
    {
        return new ViewResponse('backend.news.index');
    }

    /**
     * @param \App\Http\Requests\Backend\News\CreatePageRequest $request
     *
     * @return \App\Http\Responses\ViewResponse
     */
    public function create(CreatePageRequest $request)
    {
        return new ViewResponse('backend.news.create');
    }

    /**
     * @param \App\Http\Requests\Backend\News\StorePageRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function store(StorePageRequest $request)
    {
        $this->repository->create($request->except(['_token', '_method']));

        return new RedirectResponse(route('admin.news.index'), ['flash_success' => __('alerts.backend.pages.created')]);
    }

    /**
     * @param \App\Models\Page $news
     * @param \App\Http\Requests\Backend\News\EditPageRequest $request
     *
     * @return \App\Http\Responses\Backend\Blog\EditResponse
     */
    public function edit(News $news, EditPageRequest $request)
    {
        return new EditResponse($news);
    }

    /**
     * @param \App\Models\News $news
     * @param \App\Http\Requests\Backend\News\UpdatePageRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(News $news, UpdatePageRequest $request)
    {
        $this->repository->update($news, $request->except(['_token', '_method']));

        return new RedirectResponse(route('admin.news.index'), ['flash_success' => __('alerts.backend.pages.updated')]);
    }

    /**
     * @param \App\Models\Page $news
     * @param \App\Http\Requests\Backend\News\DeletePageRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function destroy(News $news, DeletePageRequest $request)
    {
        $this->repository->delete($news);

        return new RedirectResponse(route('admin.news.index'), ['flash_success' => __('alerts.backend.pages.deleted')]);
    }
}
