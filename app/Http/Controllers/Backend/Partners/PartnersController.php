<?php

namespace App\Http\Controllers\Backend\Partners;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Partners\CreatePartnerRequest;
use App\Http\Requests\Backend\Partners\DeletePartnerRequest;
use App\Http\Requests\Backend\Partners\EditPartnerRequest;
use App\Http\Requests\Backend\Partners\ManagePartnerRequest;
use App\Http\Requests\Backend\Partners\StorePartnerRequest;
use App\Http\Requests\Backend\Partners\UpdatePartnerRequest;
use App\Http\Responses\Backend\Partner\EditResponse;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Models\Partner;
use App\Repositories\Backend\PartnersRepository;
use Illuminate\Support\Facades\View;

class PartnersController extends Controller
{
    /**
     * @var \App\Repositories\Backend\PartnersRepository
     */
    protected $repository;

    /**
     * @param \App\Repositories\Backend\PartnersRepository $repository
     */
    public function __construct(PartnersRepository $repository)
    {
        $this->repository = $repository;
        View::share('js', ['partners']);
    }

    /**
     * @param \App\Http\Requests\Backend\Partners\ManagePartnerRequest $request
     *
     * @return \App\Http\Responses\ViewResponse
     */
    public function index(ManagePartnerRequest $request)
    {
        return new ViewResponse('backend.partners.index');
    }

    /**
     * @param \App\Http\Requests\Backend\Partners\CreatePartnerRequest $request
     *
     * @return \App\Http\Responses\ViewResponse
     */
    public function create(CreatePartnerRequest $request)
    {
        return new ViewResponse('backend.partners.create');
    }

    /**
     * @param \App\Http\Requests\Backend\Partners\StorePartnerRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function store(StorePartnerRequest $request)
    {
        $data = $request->except(['_token', '_method']);
        $image = $request->file('logo');
        if (isset($image)) {
            $path = $image->storePublicly('public/images');
            $data['logo'] = str_replace('public', 'storage', $path);
        }

        $this->repository->create($data);
        return new RedirectResponse(route('admin.partners.index'), ['flash_success' => __('alerts.backend.pages.created')]);
    }

    /**
     * @param \App\Models\Partner $partner
     * @param \App\Http\Requests\Backend\Partners\EditPartnerRequest $request
     *
     * @return \App\Http\Responses\Backend\Blog\EditResponse
     */
    public function edit(Partner $partner, EditPartnerRequest $request)
    {
        return new EditResponse($partner);
    }

    /**
     * @param \App\Models\Partner $partner
     * @param \App\Http\Requests\Backend\Partners\UpdatePartnerRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(Partner $partner, UpdatePartnerRequest $request)
    {
        $data = $request->except(['_token', '_method']);
        $image = $request->file('logo');
        if (isset($image)) {
            $path = $image->storePublicly('public/images');
            $data['logo'] = str_replace('public', 'storage', $path);
        }

        $this->repository->update($partner, $data);

        return new RedirectResponse(route('admin.partners.index'), ['flash_success' => __('alerts.backend.pages.updated')]);
    }

    /**
     * @param \App\Models\Partner $partner
     * @param \App\Http\Requests\Backend\Partners\DeletePartnerRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function destroy(Partner $partner, DeletePartnerRequest $request)
    {
        $this->repository->delete($partner);

        return new RedirectResponse(route('admin.partners.index'), ['flash_success' => __('alerts.backend.pages.deleted')]);
    }
}
