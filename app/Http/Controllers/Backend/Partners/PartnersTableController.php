<?php

namespace App\Http\Controllers\Backend\Partners;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Partners\ManagePartnerRequest;
use App\Repositories\Backend\PartnersRepository;
use Yajra\DataTables\Facades\DataTables;

class PartnersTableController extends Controller
{
    /**
     * @var \App\Repositories\Backend\PartnersRepository
     */
    protected $repository;

    /**
     * @param \App\Repositories\Backend\PartnersRepository $repository
     */
    public function __construct(PartnersRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param \App\Http\Requests\Backend\Partners\Manage\PartnerRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManagePartnerRequest $request)
    {
        return Datatables::of($this->repository->getForDataTable())
            ->editColumn('created_at', function ($partner) {
                return $partner->created_at->toDateString();
            })
            ->editColumn('logo', function ($partner) {
                return "<img width=\"50\" height=\"50\" src=\"" . asset($partner->logo) . "\"/>";
            })
            ->addColumn('actions', function ($partner) {
                return $partner->action_buttons;
            })
            ->escapeColumns(['title'])
            ->make(true);
    }
}
