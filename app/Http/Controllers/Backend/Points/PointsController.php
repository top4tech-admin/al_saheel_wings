<?php

namespace App\Http\Controllers\Backend\Points;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Pages\CreatePageRequest;
use App\Http\Requests\Backend\Pages\DeletePageRequest;
use App\Http\Requests\Backend\Pages\EditPageRequest;
use App\Http\Requests\Backend\Pages\ManagePageRequest;
use App\Http\Requests\Backend\Pages\StorePageRequest;
use App\Http\Requests\Backend\Pages\UpdatePageRequest;
use App\Http\Responses\Backend\Point\EditResponse;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Models\Point;
use App\Repositories\Backend\PointsRepository;
use Illuminate\Support\Facades\View;

class PointsController extends Controller
{
    /**
     * @var \App\Repositories\Backend\PointsRepository
     */
    protected $repository;

    /**
     * @param \App\Repositories\Backend\PointsRepository $repository
     */
    public function __construct(PointsRepository $repository)
    {
        $this->repository = $repository;
        View::share('js', ['points']);
    }

    /**
     * @param \App\Http\Requests\Backend\Pages\ManagePageRequest $request
     *
     * @return \App\Http\Responses\ViewResponse
     */
    public function index(ManagePageRequest $request)
    {
        return new ViewResponse('backend.points.index');
    }

    /**
     * @param \App\Http\Requests\Backend\Pages\CreatePageRequest $request
     *
     * @return \App\Http\Responses\ViewResponse
     */
    public function create(CreatePageRequest $request)
    {
        return new ViewResponse('backend.points.create');
    }

    /**
     * @param \App\Http\Requests\Backend\Pages\StorePageRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function store(StorePageRequest $request)
    {
        $this->repository->create($request->except(['_token', '_method']));

        return new RedirectResponse(route('admin.points.index'), ['flash_success' => __('alerts.backend.pages.created')]);
    }

    /**
     * @param \App\Models\Page $point
     * @param \App\Http\Requests\Backend\Pages\EditPageRequest $request
     *
     * @return \App\Http\Responses\Backend\Blog\EditResponse
     */
    public function edit(Point $point, EditPageRequest $request)
    {
        return new EditResponse($point);
    }

    /**
     * @param \App\Models\Page $point
     * @param \App\Http\Requests\Backend\Pages\UpdatePageRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(Point $point, UpdatePageRequest $request)
    {
        $this->repository->update($point, $request->except(['_token', '_method']));

        return new RedirectResponse(route('admin.points.index'), ['flash_success' => __('alerts.backend.pages.updated')]);
    }

    /**
     * @param \App\Models\Page $page
     * @param \App\Http\Requests\Backend\Pages\DeletePageRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function destroy(Point $point, DeletePageRequest $request)
    {
        $this->repository->delete($point);

        return new RedirectResponse(route('admin.points.index'), ['flash_success' => __('alerts.backend.pages.deleted')]);
    }
}
