<?php

namespace App\Http\Controllers\Backend\Points;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Pages\ManagePageRequest;
use App\Repositories\Backend\PointsRepository;
use Yajra\DataTables\Facades\DataTables;

class PointsTableController extends Controller
{
    /**
     * @var \App\Repositories\Backend\PointsRepository
     */
    protected $repository;

    /**
     * @param \App\Repositories\Backend\PointsRepository $repository
     */
    public function __construct(PointsRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param \App\Http\Requests\Backend\Pages\ManagePageRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManagePageRequest $request)
    {
        return Datatables::of($this->repository->getForDataTable())
            ->editColumn('updated_at', function ($point) {
                return $point->updated_at->toDateString();
            })
            ->addColumn('actions', function ($point) {
                return $point->action_buttons;
            })
            ->escapeColumns(['title'])
            ->make(true);
    }
}
