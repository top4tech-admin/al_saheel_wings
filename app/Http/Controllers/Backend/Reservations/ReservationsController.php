<?php

namespace App\Http\Controllers\Backend\Reservations;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Reservations\CreateReservationRequest;
use App\Http\Requests\Backend\Reservations\DeleteReservationRequest;
use App\Http\Requests\Backend\Reservations\EditReservationRequest;
use App\Http\Requests\Backend\Reservations\ManageReservationRequest;
use App\Http\Requests\Backend\Reservations\StoreReservationRequest;
use App\Http\Requests\Backend\Reservations\UpdateReservationRequest;
use App\Http\Responses\Backend\Reservation\EditResponse;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Models\Reservation;
use App\Models\Flight;
use App\Models\Currency;
use App\Repositories\Backend\ReservationsRepository;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Airport;

class ReservationsController extends Controller
{
    /**
     * @var \App\Repositories\Backend\ReservationsRepository
     */
    protected $repository;

    /**
     * @param \App\Repositories\Backend\ReservationsRepository $repository
     */
    public function __construct(ReservationsRepository $repository)
    {
        $this->repository = $repository;
        View::share('js', ['reservations']);
    }

    /**
     * @param \App\Http\Requests\Backend\Reservations\ManageReservationRequest $request
     *
     * @return \App\Http\Responses\ViewResponse
     */
    public function index(ManageReservationRequest $request)
    {
        return new ViewResponse('backend.reservations.index');
    }

    /**
     * @param \App\Http\Requests\Backend\Reservations\CreateReservationRequest $request
     *
     * @return \App\Http\Responses\ViewResponse
     */
    public function create(CreateReservationRequest $request)
    {
        $flights = Flight::with('fromAirport','toAirport')->get();
        return new ViewResponse('backend.reservations.create',['flights' => $flights]);
    }

    /**
     * @param \App\Http\Requests\Backend\Reservations\StoreReservationRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function store(StoreReservationRequest $request)
    {
        $this->repository->create($request->except(['_token', '_method']));

        // if(auth()->user()){
        //     return new RedirectResponse(route('admin.reservations.index'), ['flash_success' => __('alerts.backend.pages.created')]);
        // }else{
            // return  redirect()->route('frontend.booking-message',['total_price'=>$request->total_price]);
            // return new ViewResponse('frontend.booking-massage',['total_price' => $request->total_price,
            // 'currencies' => $currencies]);
        // }
        return redirect()->route('frontend.booking-message',['total_price'=>$request->total_price]);
    }

    /**
     * @param \App\Models\Reservation $reservation
     * @param \App\Http\Requests\Backend\Reservations\EditReservationRequest $request
     *
     * @return \App\Http\Responses\Backend\Blog\EditResponse
     */
    public function edit(Reservation $reservation, EditReservationRequest $request)
    {
        return new EditResponse($reservation);
    }

    /**
     * @param \App\Models\Reservation $reservation
     * @param \App\Http\Requests\Backend\Reservations\UpdateReservationRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(Reservation $reservation, UpdateReservationRequest $request)
    {
        $this->repository->update($reservation, $request->except(['_token', '_method']));

        return new RedirectResponse(route('admin.reservations.index'), ['flash_success' => __('alerts.backend.pages.updated')]);
    }

    /**
     * @param \App\Models\Reservation $reservation
     * @param \App\Http\Requests\Backend\Reservations\DeleteReservationRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function destroy(Reservation $reservation, DeleteReservationRequest $request)
    {
        $this->repository->delete($reservation);

        return new RedirectResponse(route('admin.reservations.index'), ['flash_success' => __('alerts.backend.pages.deleted')]);
    }
}
