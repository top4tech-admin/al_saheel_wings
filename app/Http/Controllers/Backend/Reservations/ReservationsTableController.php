<?php

namespace App\Http\Controllers\Backend\Reservations;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Reservations\ManageReservationRequest;
use App\Repositories\Backend\ReservationsRepository;
use Yajra\DataTables\Facades\DataTables;

class ReservationsTableController extends Controller
{
    /**
     * @var \App\Repositories\Backend\ReservationsRepository
     */
    protected $repository;

    /**
     * @param \App\Repositories\Backend\ReservationsRepository $repository
     */
    public function __construct(ReservationsRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param \App\Http\Requests\Backend\Reservations\ManageReservationRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageReservationRequest $request)
    {
        return Datatables::of($this->repository->getForDataTable())
            // ->filterColumn('status', function ($query, $keyword) {
            //     if (in_array(strtolower($keyword), ['active', 'inactive'])) {
            //         $query->where('reservations.status', (strtolower($keyword) == 'active') ? 1 : 0);
            //     }
            // })
            // ->filterColumn('created_by', function ($query, $keyword) {
            //     $query->whereRaw('users.first_name like ?', ["%{$keyword}%"]);
            // })
            // ->editColumn('status', function ($reservation) {
            //     return $reservation->status_label;
            // })
            ->editColumn('person_fname', function ($reservation) {
                return $reservation->person_fname.' '.$reservation->person_sname.' '.$reservation->person_tname;
            })
            ->editColumn('created_at', function ($reservation) {
                return $reservation->created_at->toDateString();
            })
            ->editColumn('flight_id', function ($reservation) {
                return $reservation->flight->toAirport->name_ar;
            })
            ->addColumn('count', function ($reservation) {
                return count($reservation->relatedPerson) + 1;
            })
            ->addColumn('actions', function ($reservation) {
                return $reservation->action_buttons;
            })
            ->escapeColumns(['title'])
            ->make(true);
    }
}
