<?php

namespace App\Http\Controllers\Backend\Tours;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Pages\CreatePageRequest;
use App\Http\Requests\Backend\Pages\DeletePageRequest;
use App\Http\Requests\Backend\Pages\EditPageRequest;
use App\Http\Requests\Backend\Pages\ManagePageRequest;
use App\Http\Requests\Backend\Pages\StorePageRequest;
use App\Http\Requests\Backend\Pages\UpdatePageRequest;
use App\Http\Responses\Backend\Tour\EditResponse;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Models\Tour;
use App\Repositories\Backend\ToursRepository;
use Illuminate\Support\Facades\View;

class ToursController extends Controller
{
    /**
     * @var \App\Repositories\Backend\ToursRepository
     */
    protected $repository;

    /**
     * @param \App\Repositories\Backend\ToursRepository $repository
     */
    public function __construct(ToursRepository $repository)
    {
        $this->repository = $repository;
        View::share('js', ['tours']);
    }

    /**
     * @param \App\Http\Requests\Backend\Pages\ManagePageRequest $request
     *
     * @return \App\Http\Responses\ViewResponse
     */
    public function index(ManagePageRequest $request)
    {
        return new ViewResponse('backend.tours.index');
    }

    /**
     * @param \App\Http\Requests\Backend\Pages\CreatePageRequest $request
     *
     * @return \App\Http\Responses\ViewResponse
     */
    public function create(CreatePageRequest $request)
    {
        return new ViewResponse('backend.tours.create');
    }

    /**
     * @param \App\Http\Requests\Backend\Pages\StorePageRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function store(StorePageRequest $request)
    {
        $this->repository->create($request->except(['_token', '_method']));

        return new RedirectResponse(route('admin.tours.index'), ['flash_success' => __('alerts.backend.pages.created')]);
    }

    /**
     * @param \App\Models\Page $tour
     * @param \App\Http\Requests\Backend\Pages\EditPageRequest $request
     *
     * @return \App\Http\Responses\Backend\Blog\EditResponse
     */
    public function edit(Tour $tour, EditPageRequest $request)
    {
        return new EditResponse($tour);
    }

    /**
     * @param \App\Models\Page $tour
     * @param \App\Http\Requests\Backend\Pages\UpdatePageRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(Tour $tour, UpdatePageRequest $request)
    {
        $this->repository->update($tour, $request->except(['_token', '_method']));

        return new RedirectResponse(route('admin.tours.index'), ['flash_success' => __('alerts.backend.pages.updated')]);
    }

    /**
     * @param \App\Models\Page $tour
     * @param \App\Http\Requests\Backend\Pages\DeletePageRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function destroy(Tour $tour, DeletePageRequest $request)
    {
        $this->repository->delete($tour);

        return new RedirectResponse(route('admin.tours.index'), ['flash_success' => __('alerts.backend.pages.deleted')]);
    }
}
