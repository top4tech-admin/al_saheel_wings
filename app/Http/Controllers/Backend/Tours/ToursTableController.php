<?php

namespace App\Http\Controllers\Backend\Tours;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Pages\ManagePageRequest;
use App\Repositories\Backend\ToursRepository;
use Yajra\DataTables\Facades\DataTables;

class ToursTableController extends Controller
{
    /**
     * @var \App\Repositories\Backend\ToursRepository
     */
    protected $repository;

    /**
     * @param \App\Repositories\Backend\ToursRepository $repository
     */
    public function __construct(ToursRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param \App\Http\Requests\Backend\Tours\ManagePageRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManagePageRequest $request)
    {
        return Datatables::of($this->repository->getForDataTable())
            ->editColumn('status', function ($page) {
                return $page->status_label;
            })
            ->editColumn('created_at', function ($page) {
                return $page->created_at->toDateString();
            })
            ->addColumn('actions', function ($page) {
                return $page->action_buttons;
            })
            ->escapeColumns(['title'])
            ->make(true);
    }
}
