<?php

namespace App\Http\Controllers;

/**
 * Class LanguageController.
 */
class CurrencyController extends Controller
{
    /**
     * @param $currency
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function swap($value,$symbol)
    {
        session()->put('value', $value);
        session()->put('symbol', $symbol);

        return redirect()->back();
    }
}
