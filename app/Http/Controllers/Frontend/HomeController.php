<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Airport;
use App\Models\Flight;
use App\Models\News;
use App\Models\Tour;
use App\Models\Point;
use App\Models\Currency;
use App\Models\Partner;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Mail;

/**
 * Class HomeController.
 */
class HomeController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $tours = Tour::take(3)->get();
        $news  = News::all();
        $airports = Airport::all();
        $currencies = Currency::all();

        return view('frontend.index')->with('airports', $airports)->with('news', $news)
            ->with('tours', $tours)->with('currencies', $currencies);
    }

    public function flightsResults(Request $req)
    {
        $from_go_date = Carbon::createFromFormat('d/m/Y', $req->depature_time);
        $to_go_date = Carbon::createFromFormat('d/m/Y', $req->depature_time);

        $range_days = 15;
        $from_go_date->subDays($range_days);
        $to_go_date->addDays($range_days);

        $flights_go = Flight::where('from_airport_id', $req->from_airport)
            ->where('to_airport_id',  $req->to_airport)
            ->whereBetween('depature_time',  [$from_go_date, $to_go_date])->get();

        $trip_go = [];
        foreach ($flights_go as $flight_1) {
            if (date('d-m', strtotime($flight_1->depature_time)) == date('m-d', strtotime($req->depature_time))) {
                $trip_go = $flight_1;
                //  $trip_go['airport_name'] = $flight_1->toAirport->name_ar;
                break;
            }
        }

        $flights_return = [];
        $trip_return = [];
        $from_return_date = "";
        $to_return_date = "";
        if (isset($req->arrival_time)) {
            $from_return_date = Carbon::createFromFormat('d/m/Y', $req->arrival_time);
            $to_return_date = Carbon::createFromFormat('d/m/Y', $req->arrival_time);

            $range_days = 15;
            $from_return_date->subDays($range_days);
            $to_return_date->addDays($range_days);

            $flights_return = Flight::where('to_airport_id', $req->from_airport)
                ->where('from_airport_id',  $req->to_airport)
                ->whereBetween('arrival_time',  [$from_return_date, $to_return_date])->get();

            foreach ($flights_return as $flight_r) {
                if (date('d-m', strtotime($flight_r->arrival_time)) == date('m-d', strtotime($req->arrival_time))) {
                    $trip_return = $flight_r;
                    break;
                }
            }
        }


        $airports = Airport::all();

        $passengers = ['adults' => $req->adults, 'children' => $req->children, 'infants' => $req->infants];

        $round_trip = $req['round_trip'];

        $currencies = Currency::all();

        return view('frontend.flights-results')
            ->with('currencies', $currencies)
            ->with('from_go_date', $from_go_date)
            ->with('to_go_date', $to_go_date)
            ->with('from_return_date', $from_return_date)
            ->with('to_return_date', $to_return_date)
            ->with('trip_go', $trip_go)
            ->with('trip_return', $trip_return)
            ->with('depature_time', $req->depature_time)
            ->with('arrival_time', $req->arrival_time)
            ->with('round_trip', $round_trip)
            ->with('flights_go', $flights_go)
            ->with('flights_return', $flights_return)
            ->with('airports', $airports)
            ->with('passengers', $passengers);
    }

    public function confirmStep(Request $req)
    {
        $flight = Flight::find($req->go_flight_id);
        $passengers = ['adults' => $req->adults, 'children' => $req->children, 'infants' => $req->infants];
        $round_trip = $req->round_trip;
        $flights_return = $req->return_flight_id;

        $currencies = Currency::all();
        return view('frontend.confirmBooking')
            ->with('currencies', $currencies)
            ->with('flight', $flight)
            ->with('passengers', $passengers)
            ->with('round_trip', $round_trip)
            ->with('flights_return', $flights_return);
    }

    public function myPoints()
    {
        $currencies = Currency::all();
        $partners = Partner::all();
        $point = false;
        return view('frontend.my_points')->with('point', $point)
            ->with('currencies', $currencies)->with('partners', $partners);
    }

    public function checkSerialNumber(Request $req)
    {
        $currencies = Currency::all();
        $partners = Partner::all();
        $point = Point::where('serial_number', $req->serial_number)->first();
        return view('frontend.my_points')->with('point', $point)
            ->with('currencies', $currencies)->with('partners', $partners);
    }

    public function aboutUs()
    {
        $currencies = Currency::all();
        return view('frontend.about-us')
            ->with('currencies', $currencies);
    }

    public function showBookMessage($total_price)
    {
        $currencies = Currency::all();
        return view('frontend.booking-massage')
            ->with('total_price', $total_price)
            ->with('currencies', $currencies);
    }

    public function displayTrip($title)
    {
        $currencies = Currency::all();
        $tour = Tour::where('title', $title)->first();
        return view('frontend.single_tour')
            ->with('tour', $tour)
            ->with('currencies', $currencies);
    }
    /*
	public function sendEmail($text) {
      $data = array('text'=> $text);
   
      Mail::send(['text'=>'mail'], $data, function($message) {
         $message->to('quanah2@xwkqguild.com', 'Tutorials Point')->subject
            ('Laravel Basic Testing Mail');
         $message->from('quanah2@xwkqguild.com','Virat Gandhi');
		});
      return "تم ارسال الرسالة بنجاح";
   }
   */
}
