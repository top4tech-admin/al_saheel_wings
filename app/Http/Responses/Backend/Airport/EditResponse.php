<?php

namespace App\Http\Responses\Backend\Airport;

use Illuminate\Contracts\Support\Responsable;

class EditResponse implements Responsable
{
    /**
     * @var \App\Models\Airport\Airport
     */
    protected $airport;

    /**
     * @param \App\Models\Airport\Airport $airport
     */
    public function __construct($airport)
    {
        $this->airport = $airport;
    }

    /**
     * toReponse.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toResponse($request)
    {
        return view('backend.airports.edit')
            ->withAirport($this->airport);
    }
}
