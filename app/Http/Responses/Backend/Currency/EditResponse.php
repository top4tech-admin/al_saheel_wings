<?php

namespace App\Http\Responses\Backend\Currency;

use Illuminate\Contracts\Support\Responsable;

class EditResponse implements Responsable
{
    /**
     * @var \App\Models\Currency\Currency
     */
    protected $currency;

    /**
     * @param \App\Models\Currency\Currency $currency
     */
    public function __construct($currency)
    {
        $this->currency = $currency;
    }

    /**
     * toReponse.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toResponse($request)
    {
        return view('backend.currencys.edit')
            ->withCurrency($this->currency);
    }
}
