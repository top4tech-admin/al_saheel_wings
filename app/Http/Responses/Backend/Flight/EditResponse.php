<?php

namespace App\Http\Responses\Backend\Flight;

use Illuminate\Contracts\Support\Responsable;
use App\Models\Airport;

class EditResponse implements Responsable
{
    /**
     * @var \App\Models\Page\Flight
     */
    protected $flight;

    /**
     * @param \App\Models\Page\Page $flight
     */
    public function __construct($flight)
    {
        $this->flight = $flight;
    }

    /**
     * toReponse.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toResponse($request)
    {
        $airports = Airport::all();
        return view('backend.flights.edit')
            ->withFlight($this->flight)
            ->withAirports($airports);
    }
}
