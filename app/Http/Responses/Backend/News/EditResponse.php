<?php

namespace App\Http\Responses\Backend\News;

use Illuminate\Contracts\Support\Responsable;

class EditResponse implements Responsable
{
    /**
     * @var \App\Models\News\News
     */
    protected $news;

    /**
     * @param \App\Models\News\News $news
     */
    public function __construct($news)
    {
        $this->news = $news;
    }

    /**
     * toReponse.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toResponse($request)
    {
        return view('backend.news.edit')
            ->withNews($this->news);
    }
}
