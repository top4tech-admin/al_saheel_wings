<?php

namespace App\Http\Responses\Backend\Partner;

use Illuminate\Contracts\Support\Responsable;

class EditResponse implements Responsable
{
    /**
     * @var \App\Models\Partner\Partner
     */
    protected $partner;

    /**
     * @param \App\Models\Partner\Partner partner
     */
    public function __construct($partner)
    {
        $this->partner = $partner;
    }

    /**
     * toReponse.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toResponse($request)
    {
        return view('backend.partners.edit')
            ->withPartner($this->partner);
    }
}

    