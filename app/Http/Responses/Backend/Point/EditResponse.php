<?php

namespace App\Http\Responses\Backend\Point;

use Illuminate\Contracts\Support\Responsable;

class EditResponse implements Responsable
{
    /**
     * @var \App\Models\Point\Point
     */
    protected $point;

    /**
     * @param \App\Models\Point\Point $point
     */
    public function __construct($point)
    {
        $this->point = $point;
    }

    /**
     * toReponse.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toResponse($request)
    {
        return view('backend.points.edit')
            ->withPoint($this->point);
    }
}
