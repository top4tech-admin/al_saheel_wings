<?php

namespace App\Http\Responses\Backend\Reservation;

use Illuminate\Contracts\Support\Responsable;
use App\Models\Flight;

class EditResponse implements Responsable
{
    /**
     * @var \App\Models\Page\Page
     */
    protected $reservation;

    /**
     * @param \App\Models\Page\Page $reservation
     */
    public function __construct($reservation)
    {
        $this->reservation = $reservation;
    }

    /**
     * toReponse.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toResponse($request)
    {
        $flights = Flight::with('fromAirport','toAirport')->get();
        return view('backend.reservations.edit')
            ->withReservation($this->reservation)
            ->withFlights($flights);
    }
}
