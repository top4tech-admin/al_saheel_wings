<?php

namespace App\Http\Responses\Backend\Tour;

use Illuminate\Contracts\Support\Responsable;

class EditResponse implements Responsable
{
    /**
     * @var \App\Models\Tour\Tour
     */
    protected $tour;

    /**
     * @param \App\Models\Tour\Tour $tour
     */
    public function __construct($tour)
    {
        $this->tour = $tour;
    }

    /**
     * toReponse.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toResponse($request)
    {
        return view('backend.tours.edit')
            ->withTour($this->tour);
    }
}
