<?php

namespace App\Models;

use App\Models\Traits\Attributes\AirportAttributes;
use App\Models\Traits\Relationships\AirportRelationships;
use App\Models\Traits\ModelAttributes;

class Airport extends BaseModel
{
    use ModelAttributes, AirportAttributes, AirportRelationships;

    /**
     * The guarded field which are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Fillable.
     *
     * @var array
     */
    
    protected $fillable = [
        'name_ar',
        'name_en',
    ];

}
