<?php

namespace App\Models;

use App\Models\Traits\Attributes\CurrencyAttributes;
use App\Models\Traits\ModelAttributes;

class Currency extends BaseModel
{
    use ModelAttributes, CurrencyAttributes;

    /**
     * The guarded field which are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];
    
    /**
     * Fillable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'symbol',
        'value',
    ];

}
