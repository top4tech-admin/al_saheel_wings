<?php

namespace App\Models;

use App\Models\Traits\Attributes\FlightAttributes;
use App\Models\Traits\ModelAttributes;
use App\Models\Traits\Relationships\FlightRelationships;

class Flight extends BaseModel
{
    use ModelAttributes, FlightRelationships, FlightAttributes;

    /**
     * The guarded field which are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Fillable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'type',
        'depature_time',
        'arrival_time',
        'stop_station',
        'grade',
        'baby_price',
        'yunge_price',
        'price',
        'tax',
        'passengers_count',
        'from_airport_id',
        'to_airport_id',
    ];
}
