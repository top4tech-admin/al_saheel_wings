<?php

namespace App\Models;

use App\Models\Traits\Attributes\NewsAttributes;
use App\Models\Traits\ModelAttributes;

class News extends BaseModel
{
    use ModelAttributes, NewsAttributes;

    /**
     * The guarded field which are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Fillable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'created_by',
        'updated_by',
    ];

}
