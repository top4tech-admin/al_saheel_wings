<?php

namespace App\Models;

use App\Models\Traits\Attributes\PointAttributes;
use App\Models\Traits\ModelAttributes;

class Point extends BaseModel
{
    use ModelAttributes, PointAttributes;

    /**
     * The guarded field which are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Fillable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'name',
        'phone',
        'serial_number',
        'email',
        'point',
        'created_by',
        'updated_by',
    ];

}
