<?php

namespace App\Models;

use App\Models\Traits\Attributes\ReservationAttributes;
use App\Models\Traits\ModelAttributes;
use App\Models\Traits\Relationships\ReservationRelationships;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reservation extends BaseModel
{
    use ModelAttributes, ReservationRelationships, ReservationAttributes;

    /**
     * The guarded field which are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Fillable.
     *
     * @var array
     */
    protected $fillable = [
        'person_fname',
        'person_sname',
        'person_tname',
        'nationality',
        'email',
        'round_trip',
        'flight_return_id',
        'passport',
        'passport_expiretion',
        'flight_id',
        'passport_country',
        'mobile',
        'birthday',
        'reservation_id'
    ];

}