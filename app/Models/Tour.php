<?php

namespace App\Models;

use App\Models\Traits\Attributes\TourAttributes;
use App\Models\Traits\ModelAttributes;

class Tour extends BaseModel
{
    use ModelAttributes, TourAttributes;

    /**
     * The guarded field which are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Fillable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'img',
        'price',
        'depature_time',
        'arrival_time',
    ];

}
