<?php

namespace App\Models\Traits\Relationships;

use App\Models\Flight;

trait AirportRelationships
{
    /**
     * Airport belongs to relationship with Flight.
     */
    public function flights_from()
    {
        return $this->hasMany(Flight::class, 'from_airport_id');
    }

    public function flights_to()
    {
        return $this->hasMany(Flight::class, 'to_airport_id');
    }
    
}
