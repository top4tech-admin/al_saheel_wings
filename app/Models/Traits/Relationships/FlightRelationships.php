<?php

namespace App\Models\Traits\Relationships;

use App\Models\Airport;
use App\Models\Reservation;

trait FlightRelationships
{
    /**
     * Airport belongs to relationship with Flight.
     */
    public function fromAirport()
    {
        return $this->belongsTo(Airport::class, 'from_airport_id');
    }

    public function toAirport()
    {
        return $this->belongsTo(Airport::class, 'to_airport_id');
    }

    public function reservations()
    {
        return $this->hasMany(Reservation::class, 'flight_id');
    }
    
    public function reservationsReturn()
    {
        return $this->hasMany(Reservation::class, 'flight_return_id');
    }
}
