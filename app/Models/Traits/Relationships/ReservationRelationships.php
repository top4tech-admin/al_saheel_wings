<?php

namespace App\Models\Traits\Relationships;

use App\Models\Flight;
use App\Models\Reservation;

trait ReservationRelationships
{
    /**
     * Page belongs to relationship with user.
     */
    public function flight()
    {
        // going flight
        return $this->belongsTo(Flight::class, 'flight_id');
    }

    /**
     * Page belongs to relationship with user.
     */
    public function flightReturn()
    {
        // Returning flight
        return $this->belongsTo(Flight::class, 'flight_return_id');
    }

    public function relatedPerson()
    {
        return $this->hasMany(Reservation::class, 'reservation_id');
    }

}
