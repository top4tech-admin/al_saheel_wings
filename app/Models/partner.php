<?php

namespace App\Models;

use App\Models\Traits\Attributes\PartnerAttributes;
use App\Models\Traits\ModelAttributes;
use App\Models\Traits\Relationships\PartnerRelationships;

class Partner extends BaseModel
{
    use  ModelAttributes, PartnerRelationships, PartnerAttributes;

    /**
     * The guarded field which are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Fillable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'phone',
        'address',
        'logo'
    ];
}
