<?php

namespace App\Repositories\Backend;

use App\Events\Backend\Airports\AirportCreated;
use App\Events\Backend\Airports\AirportDeleted;
use App\Events\Backend\Airports\AirportUpdated;
use App\Exceptions\GeneralException;
use App\Models\Airport;
use App\Repositories\BaseRepository;
use Illuminate\Support\Str;

class AirportsRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Airport::class;

    /**
     * Sortable.
     *
     * @var array
     */
    private $sortable = [
        'id',
        'name_ar',
        'name_en',
        'created_at',
        'updated_at',
    ];

    /**
     * Retrieve List.
     *
     * @var array
     * @return Collection
     */
    public function retrieveList(array $options = [])
    {
        $perPage = isset($options['per_page']) ? (int) $options['per_page'] : 20;
        $orderBy = isset($options['order_by']) && in_array($options['order_by'], $this->sortable) ? $options['order_by'] : 'created_at';
        $order = isset($options['order']) && in_array($options['order'], ['asc', 'desc']) ? $options['order'] : 'desc';
        $query = $this->query()
            ->with([
                'owner',
                'updater',
            ])
            ->orderBy($orderBy, $order);

        if ($perPage == -1) {
            return $query->get();
        }

        return $query->paginate($perPage);
    }

    /**
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
                'id',
                'name_ar',
                'name_en',
                'created_at',
                'updated_at',
            ]);
    }

    /**
     * @param array $input
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return bool
     */
    public function create(array $input)
    {
        
        if ($airport = Airport::create($input)) {
            event(new AirportCreated($airport));

            return $airport->fresh();
        }

        throw new GeneralException(__('exceptions.backend.pages.create_error'));
    }

    /**
     * Update Airport.
     *
     * @param \App\Models\Airport $airport
     * @param array $input
     */
    public function update(Airport $airport, array $input)
    {

        if ($airport->update($input)) {
            event(new AirportUpdated($airport));

            return $airport;
        }

        throw new GeneralException(
            __('exceptions.backend.pages.update_error')
        );
    }

    /**
     * @param \App\Models\Airport $airport
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function delete(Airport $airport)
    {
        if ($airport->delete()) {
            event(new AirportDeleted($airport));

            return true;
        }

        throw new GeneralException(__('exceptions.backend.pages.delete_error'));
    }
}
