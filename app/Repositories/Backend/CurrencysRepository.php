<?php

namespace App\Repositories\Backend;

use App\Events\Backend\Currencys\CurrencyCreated;
use App\Events\Backend\Currencys\CurrencyDeleted;
use App\Events\Backend\Currencys\CurrencyUpdated;
use App\Exceptions\GeneralException;
use App\Models\Currency;
use App\Repositories\BaseRepository;
use Illuminate\Support\Str;

class CurrencysRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Currency::class;

    /**
     * Sortable.
     *
     * @var array
     */
    private $sortable = [
        'id',
        'created_at',
        'updated_at',
    ];

    /**
     * Retrieve List.
     *
     * @var array
     * @return Collection
     */
    public function retrieveList(array $options = [])
    {
        $perPage = isset($options['per_page']) ? (int) $options['per_page'] : 20;
        $orderBy = isset($options['order_by']) && in_array($options['order_by'], $this->sortable) ? $options['order_by'] : 'created_at';
        $order = isset($options['order']) && in_array($options['order'], ['asc', 'desc']) ? $options['order'] : 'desc';
        $query = $this->query()
            ->with([
                'owner',
                'updater',
            ])
            ->orderBy($orderBy, $order);

        if ($perPage == -1) {
            return $query->get();
        }

        return $query->paginate($perPage);
    }

    /**
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
                'id',
                'name',
                'symbol',
                'value',
                'created_at',
            ]);
    }

    /**
     * @param array $input
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return bool
     */
    public function create(array $input)
    {

        if ($currency = Currency::create($input)) {
            event(new CurrencyCreated($currency));

            return $currency->fresh();
        }

        throw new GeneralException(__('exceptions.backend.pages.create_error'));
    }

    /**
     * Update Currency.
     *
     * @param \App\Models\Currency $currency
     * @param array $input
     */
    public function update(Currency $currency, array $input)
    {

        if ($currency->update($input)) {
            event(new CurrencyUpdated($currency));

            return $currency;
        }

        throw new GeneralException(
            __('exceptions.backend.pages.update_error')
        );
    }

    /**
     * @param \App\Models\Currency $currency
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function delete(Currency $currency)
    {
        if ($currency->delete()) {
            event(new CurrencyDeleted($currency));

            return true;
        }

        throw new GeneralException(__('exceptions.backend.pages.delete_error'));
    }
}
