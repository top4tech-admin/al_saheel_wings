<?php

namespace App\Repositories\Backend;

use App\Events\Backend\Flights\FlightCreated;
use App\Events\Backend\Flights\FlightDeleted;
use App\Events\Backend\Flights\FlightUpdated;
use App\Exceptions\GeneralException;
use App\Models\Flight;
use App\Repositories\BaseRepository;
use Illuminate\Support\Str;

class FlightsRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Flight::class;

    /**
     * Sortable.
     *
     * @var array
     */
    private $sortable = [
        'id',
        'created_at',
        'updated_at',
    ];

    /**
     * Retrieve List.
     *
     * @var array
     * @return Collection
     */
    public function retrieveList(array $options = [])
    {
        $perPage = isset($options['per_page']) ? (int) $options['per_page'] : 20;
        $orderBy = isset($options['order_by']) && in_array($options['order_by'], $this->sortable) ? $options['order_by'] : 'created_at';
        $order = isset($options['order']) && in_array($options['order'], ['asc', 'desc']) ? $options['order'] : 'desc';
        $query = $this->query()
            ->with([
                'owner',
                'updater',
            ])
            ->orderBy($orderBy, $order);

        if ($perPage == -1) {
            return $query->get();
        }

        return $query->paginate($perPage);
    }

    /**
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
                'id',
                'from_airport_id',
                'to_airport_id',
                'depature_time',
                'stop_station',
                'price',
                'created_at'
            ]);
    }

    /**
     * @param array $input
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return bool
     */
    public function create(array $input)
    {

        if ($page = Flight::create($input)) {
            event(new FlightCreated($page));

            return $page->fresh();
        }

        throw new GeneralException(__('exceptions.backend.pages.create_error'));
    }

    /**
     * Update Flight.
     *
     * @param \App\Models\Flight $page
     * @param array $input
     */
    public function update(Flight $page, array $input)
    {

        if ($page->update($input)) {
            event(new FlightUpdated($page));

            return $page;
        }

        throw new GeneralException(
            __('exceptions.backend.pages.update_error')
        );
    }

    /**
     * @param \App\Models\Flight $page
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function delete(Flight $page)
    {
        if ($page->delete()) {
            event(new FlightDeleted($page));

            return true;
        }

        throw new GeneralException(__('exceptions.backend.pages.delete_error'));
    }
}
