<?php

namespace App\Repositories\Backend;

use App\Events\Backend\News\NewCreated;
use App\Events\Backend\News\NewDeleted;
use App\Events\Backend\News\NewUpdated;
use App\Exceptions\GeneralException;
use App\Models\News;
use App\Repositories\BaseRepository;
use Illuminate\Support\Str;

class NewsRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = News::class;

    /**
     * Sortable.
     *
     * @var array
     */
    private $sortable = [
        'id',
        'title',
        'created_at',
        'updated_at',
    ];

    /**
     * Retrieve List.
     *
     * @var array
     * @return Collection
     */
    public function retrieveList(array $options = [])
    {
        $perPage = isset($options['per_page']) ? (int) $options['per_page'] : 20;
        $orderBy = isset($options['order_by']) && in_array($options['order_by'], $this->sortable) ? $options['order_by'] : 'created_at';
        $order = isset($options['order']) && in_array($options['order'], ['asc', 'desc']) ? $options['order'] : 'desc';
        $query = $this->query()
            ->with([
                'owner',
                'updater',
            ])
            ->orderBy($orderBy, $order);

        if ($perPage == -1) {
            return $query->get();
        }

        return $query->paginate($perPage);
    }

    /**
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
                'id',
                'title',
                'created_at',
            ]);
    }

    /**
     * @param array $input
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return bool
     */
    public function create(array $input)
    {

        if ($news = News::create($input)) {
            event(new NewCreated($news));

            return $news->fresh();
        }

        throw new GeneralException(__('exceptions.backend.pages.create_error'));
    }

    /**
     * Update News.
     *
     * @param \App\Models\News $news
     * @param array $input
     */
    public function update(News $news, array $input)
    {

        if ($news->update($input)) {
            event(new NewUpdated($news));

            return $news;
        }

        throw new GeneralException(
            __('exceptions.backend.pages.update_error')
        );
    }

    /**
     * @param \App\Models\News $news
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function delete(News $news)
    {
        if ($news->delete()) {
            event(new NewDeleted($news));

            return true;
        }

        throw new GeneralException(__('exceptions.backend.pages.delete_error'));
    }
}
