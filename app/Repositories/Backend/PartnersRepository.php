<?php

namespace App\Repositories\Backend;

use App\Events\Backend\Partners\PartnerCreated;
use App\Events\Backend\Partners\PartnerDeleted;
use App\Events\Backend\Partners\PartnerUpdated;
use App\Exceptions\GeneralException;
use App\Models\Partner;
use App\Repositories\BaseRepository;
use Illuminate\Support\Str;

class PartnersRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Partner::class;

    /**
     * Sortable.
     *
     * @var array
     */
    private $sortable = [
        'id',
        'created_at',
        'updated_at',
        'name',
        'phone',
        'address'
    ];

    /**
     * Retrieve List.
     *
     * @var array
     * @return Collection
     */
    public function retrieveList(array $options = [])
    {
        $perPage = isset($options['per_page']) ? (int) $options['per_page'] : 20;
        $orderBy = isset($options['order_by']) && in_array($options['order_by'], $this->sortable) ? $options['order_by'] : 'created_at';
        $order = isset($options['order']) && in_array($options['order'], ['asc', 'desc']) ? $options['order'] : 'desc';
        $query = $this->query()
            ->with([
                'owner',
                'updater',
            ])
            ->orderBy($orderBy, $order);

        if ($perPage == -1) {
            return $query->get();
        }

        return $query->paginate($perPage);
    }

    /**
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
                'id',
                'created_at',
                'updated_at',
                'name',
                'phone',
                'logo',
                'address'
            ]);
    }

    /**
     * @param array $input
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return bool
     */
    public function create(array $input)
    {

        if ($partner = Partner::create($input)) {
            event(new PartnerCreated($partner));

            return $partner->fresh();
        }

        throw new GeneralException(__('exceptions.backend.pages.create_error'));
    }

    /**
     * Update Partner.
     *
     * @param \App\Models\Partner $partner
     * @param array $input
     */
    public function update(Partner $partner, array $input)
    {

        if ($partner->update($input)) {
            event(new PartnerUpdated($partner));

            return $partner;
        }

        throw new GeneralException(
            __('exceptions.backend.pages.update_error')
        );
    }

    /**
     * @param \App\Models\Partner $partner
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function delete(Partner $partner)
    {
        if ($partner->delete()) {
            event(new PartnerDeleted($partner));

            return true;
        }

        throw new GeneralException(__('exceptions.backend.pages.delete_error'));
    }
}
