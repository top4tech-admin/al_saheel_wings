<?php

namespace App\Repositories\Backend;

use App\Events\Backend\Points\PointCreated;
use App\Events\Backend\Points\PointDeleted;
use App\Events\Backend\Points\PointUpdated;
use App\Exceptions\GeneralException;
use App\Models\Point;
use App\Repositories\BaseRepository;
use Illuminate\Support\Str;

class PointsRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Point::class;

    /**
     * Sortable.
     *
     * @var array
     */
    private $sortable = [
        'id',
        'name',
        'phone',
        'email',
        'point',
        'created_at',
        'updated_at',
    ];
    /**
     * Retrieve List.
     *
     * @var array
     * @return Collection
     */
    public function retrieveList(array $options = [])
    {
        $perPage = isset($options['per_page']) ? (int) $options['per_page'] : 20;
        $orderBy = isset($options['order_by']) && in_array($options['order_by'], $this->sortable) ? $options['order_by'] : 'created_at';
        $order = isset($options['order']) && in_array($options['order'], ['asc', 'desc']) ? $options['order'] : 'desc';
        $query = $this->query()
            ->with([
                'owner',
                'updater',
            ])
            ->orderBy($orderBy, $order);

        if ($perPage == -1) {
            return $query->get();
        }

        return $query->paginate($perPage);
    }

    /**
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
                'id',
                'name',
                'phone',
                'email',
                'point',
                'updated_at',
            ]);
    }

    /**
     * @param array $input
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return bool
     */
    public function create(array $input)
    {

        if ($page = Point::create($input)) {
            event(new PointCreated($page));

            return $page->fresh();
        }

        throw new GeneralException(__('exceptions.backend.pages.create_error'));
    }

    /**
     * Update Point.
     *
     * @param \App\Models\Point $page
     * @param array $input
     */
    public function update(Point $page, array $input)
    {

        if ($page->update($input)) {
            event(new PointUpdated($page));

            return $page;
        }

        throw new GeneralException(
            __('exceptions.backend.pages.update_error')
        );
    }

    /**
     * @param \App\Models\Point $page
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function delete(Point $page)
    {
        if ($page->delete()) {
            event(new PointDeleted($page));

            return true;
        }

        throw new GeneralException(__('exceptions.backend.pages.delete_error'));
    }
}
