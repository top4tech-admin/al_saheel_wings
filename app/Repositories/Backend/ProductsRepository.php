<?php

namespace App\Repositories\Backend;

use App\Events\Backend\Products\ProductCreated;
use App\Events\Backend\Products\ProductDeleted;
use App\Events\Backend\Products\ProductUpdated;
use App\Exceptions\GeneralException;
use App\Models\Product;
use App\Repositories\BaseRepository;
use Illuminate\Support\Str;

class ProductsRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Product::class;

    /**
     * Sortable.
     *
     * @var array
     */
    private $sortable = [
        'id',
        'created_at',
        'updated_at',
        'name',
		'desc',
		'price'
    ];

    /**
     * Retrieve List.
     *
     * @var array
     * @return Collection
     */
    public function retrieveList(array $options = [])
    {
        $perPage = isset($options['per_page']) ? (int) $options['per_page'] : 20;
        $orderBy = isset($options['order_by']) && in_array($options['order_by'], $this->sortable) ? $options['order_by'] : 'created_at';
        $order = isset($options['order']) && in_array($options['order'], ['asc', 'desc']) ? $options['order'] : 'desc';
        $query = $this->query()
            ->with([
                'owner',
                'updater',
            ])
            ->orderBy($orderBy, $order);

        if ($perPage == -1) {
            return $query->get();
        }

        return $query->paginate($perPage);
    }

    /**
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
                'name',
		'desc',
		'price'
            ]);
    }

    /**
     * @param array $input
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return bool
     */
    public function create(array $input)
    {

        if ($product = Product::create($input)) {
            event(new ProductCreated($product));

            return $product->fresh();
        }

        throw new GeneralException(__('exceptions.backend.pages.create_error'));
    }

    /**
     * Update Product.
     *
     * @param \App\Models\Product $product
     * @param array $input
     */
    public function update(Product $product, array $input)
    {

        if ($product->update($input)) {
            event(new ProductUpdated($product));

            return $product;
        }

        throw new GeneralException(
            __('exceptions.backend.pages.update_error')
        );
    }

    /**
     * @param \App\Models\Product $product
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function delete(Product $product)
    {
        if ($product->delete()) {
            event(new ProductDeleted($product));

            return true;
        }

        throw new GeneralException(__('exceptions.backend.pages.delete_error'));
    }
}
   