<?php

namespace App\Repositories\Backend;

use App\Events\Backend\Reservations\ReservationCreated;
use App\Events\Backend\Reservations\ReservationDeleted;
use App\Events\Backend\Reservations\ReservationUpdated;
use App\Exceptions\GeneralException;
use App\Models\Reservation;
use App\Repositories\BaseRepository;
use Illuminate\Support\Str;

class ReservationsRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Reservation::class;

    /**
     * Sortable.
     *
     * @var array
     */
    private $sortable = [
        'id',
        'created_at',
        'updated_at',
    ];

    /**
     * Retrieve List.
     *
     * @var array
     * @return Collection
     */
    public function retrieveList(array $options = [])
    {
        $perPage = isset($options['per_page']) ? (int) $options['per_page'] : 20;
        $orderBy = isset($options['order_by']) && in_array($options['order_by'], $this->sortable) ? $options['order_by'] : 'created_at';
        $order = isset($options['order']) && in_array($options['order'], ['asc', 'desc']) ? $options['order'] : 'desc';
        $query = $this->query()
            ->with([
                'owner',
                'updater',
            ])
            ->orderBy($orderBy, $order);

        if ($perPage == -1) {
            return $query->get();
        }

        return $query->paginate($perPage);
    }

    /**
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->where('reservation_id', null)
            ->select([
                'id',
                'person_fname',
                'person_sname',
                'person_tname',
                'nationality',
                'passport',
                'passport_expiretion',
                'flight_id',
                'created_at'
            ]);
    }

    /**
     * @param array $input
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return bool
     */
    public function create(array $input)
    {
        $first_id = null;

        if(isset($input['flight_return_id']))
        {
            $input['flight_return_id'] = $input['flight_return_id'] ?? null;
        }else{
            $input['flight_return_id'] = null;
        }
        
        if(isset($input['round_trip']))
        {
            $input['round_trip'] = $input['round_trip'] == 'round_trip' ? true: false ;
        }else{
            $input['round_trip'] = false;
        }
        

        for ($i = 0; $i < count($input['person_fname']); $i++) {
            if ($reservation = Reservation::create(
                [
                    "person_fname" => $input['person_fname'][$i],
                    "person_sname" => $input['person_sname'][$i],
                    "person_tname" => $input['person_tname'][$i],
                    "nationality" => $input['nationality'][$i],
                    "passport" => $input['passport'][$i],
                    "passport_expiretion" => $input['passport_expiretion'][$i],
                    "flight_id" => $input['flight_id'],
                    "round_trip" => $input['round_trip'],
                    "email" => $input['email'],
                    "flight_return_id" => $input['flight_return_id'],
                    "passport_country" => $input['passport_country'][$i],
                    "mobile" => $input['mobile'][$i] ?? null,
                    "birthday" => $input['birthday'][$i],
                    "reservation_id" => $first_id,
                ]
            )) {
                event(new ReservationCreated($reservation));
                $first_id = is_null($first_id) ? $reservation->id : $first_id;
            } else {
                throw new GeneralException(__('exceptions.backend.pages.create_error'));
            }
        }

        return $reservation->fresh();
    }

    /**
     * Update Reservation.
     *
     * @param \App\Models\Reservation $reservation
     * @param array $input
     */
    public function update(Reservation $reservation, array $input)
    {

        if(isset($input['flight_return_id']))
        {
            $input['flight_return_id'] = $input['flight_return_id'] ?? null;
        }else{
            $input['flight_return_id'] = null;
        }
        
        if(isset($input['round_trip']))
        {
            $input['round_trip'] = $input['round_trip'] == 'round_trip' ? true: false ;
        }else{
            $input['round_trip'] = false;
        }
        
        if ($reservation->update($input)) {
            event(new ReservationUpdated($reservation));

            return $reservation;
        }

        throw new GeneralException(
            __('exceptions.backend.pages.update_error')
        );
    }

    /**
     * @param \App\Models\Reservation $reservation
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function delete(Reservation $reservation)
    {
        if ($reservation->delete()) {
            event(new ReservationDeleted($reservation));

            return true;
        }

        throw new GeneralException(__('exceptions.backend.pages.delete_error'));
    }
}
