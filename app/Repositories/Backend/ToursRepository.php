<?php

namespace App\Repositories\Backend;

use App\Events\Backend\Tours\TourCreated;
use App\Events\Backend\Tours\TourDeleted;
use App\Events\Backend\Tours\TourUpdated;
use App\Exceptions\GeneralException;
use App\Models\Tour;
use App\Repositories\BaseRepository;
use Illuminate\Support\Str;

class ToursRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Tour::class;

    /**
     * Sortable.
     *
     * @var array
     */
    private $sortable = [
        'id',
        'title',
        'description',
        'price',
        'depature_time',
        'arrival_time',
    ];

    /**
     * Retrieve List.
     *
     * @var array
     * @return Collection
     */
    public function retrieveList(array $options = [])
    {
        $perPage = isset($options['per_page']) ? (int) $options['per_page'] : 20;
        $orderBy = isset($options['order_by']) && in_array($options['order_by'], $this->sortable) ? $options['order_by'] : 'created_at';
        $order = isset($options['order']) && in_array($options['order'], ['asc', 'desc']) ? $options['order'] : 'desc';
        $query = $this->query()
            ->with([
                'owner',
                'updater',
            ])
            ->orderBy($orderBy, $order);

        if ($perPage == -1) {
            return $query->get();
        }

        return $query->paginate($perPage);
    }

    /**
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
                'id',
                'title',
                'description',
                'price',
                'depature_time',
                'created_at',
            ]);
    }

    /**
     * @param array $input
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return bool
     */
    public function create(array $input)
    {

        if ($page = Tour::create($input)) {
            event(new TourCreated($page));

            return $page->fresh();
        }

        throw new GeneralException(__('exceptions.backend.pages.create_error'));
    }

    /**
     * Update Tour.
     *
     * @param \App\Models\Tour $page
     * @param array $input
     */
    public function update(Tour $page, array $input)
    {

        if ($page->update($input)) {
            event(new TourUpdated($page));

            return $page;
        }

        throw new GeneralException(
            __('exceptions.backend.pages.update_error')
        );
    }

    /**
     * @param \App\Models\Tour $page
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function delete(Tour $page)
    {
        if ($page->delete()) {
            event(new TourDeleted($page));

            return true;
        }

        throw new GeneralException(__('exceptions.backend.pages.delete_error'));
    }
}
