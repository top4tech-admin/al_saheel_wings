<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Flight extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('flights', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->dateTimeTz('depature_time', $precision = 0);
            $table->dateTimeTz('arrival_time', $precision = 0);
            $table->timeTz('duration');
            $table->integer('stop_station')->default(0);
            $table->string('grade');
            $table->double('price');
            $table->double('baby_price');
            $table->double('yunge_price');
            $table->double('tax')->default(0);
            $table->integer('passengers_count');

            $table->bigInteger('from_airport_id')->unsigned();
            $table->bigInteger('to_airport_id')->unsigned();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('flights');
    }
}
