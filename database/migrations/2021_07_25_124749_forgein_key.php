<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ForgeinKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('flights', function (Blueprint $table) {
            $table->foreign('from_airport_id')->references('id')->on('airports')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('to_airport_id')->references('id')->on('airports')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('flights', function (Blueprint $table) {
            $table->dropForeign('flights_from_airport_id_foreign');
            $table->dropForeign('flights_to_airport_id_foreign');
        });
    }
}
