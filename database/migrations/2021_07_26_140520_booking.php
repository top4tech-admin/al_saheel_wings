<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use phpDocumentor\Reflection\Types\Nullable;

class Booking extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('reservations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('person_fname');
            $table->string('person_sname');
            $table->string('person_tname');
            $table->string('email')->nullable();
            $table->string('nationality');
            $table->string('passport');
            $table->string('mobile')->nullable();
            $table->string('passport_country');
            $table->date('birthday');
            $table->date('passport_expiretion');
            $table->boolean('round_trip');

            $table->bigInteger('reservation_id')->unsigned()->nullable();

            $table->bigInteger('flight_id')->unsigned(); // flight one way
            $table->bigInteger('flight_return_id')->unsigned()->nullable();; // flight round trip
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('reservations');
    }
}
