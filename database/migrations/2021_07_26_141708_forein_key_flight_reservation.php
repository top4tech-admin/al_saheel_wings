<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ForeinKeyFlightReservation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('reservations', function (Blueprint $table) {
            $table->foreign('flight_id')->references('id')->on('flights')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('flight_return_id')->references('id')->on('flights')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('reservation_id')->references('id')->on('reservations')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('reservations', function (Blueprint $table) {
            $table->dropForeign('reservations_flight_id_foreign');
            $table->dropForeign('reservations_flight_return_id_foreign');
        });
    }
}
