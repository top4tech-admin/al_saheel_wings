(function () {

    FTX.Currencys = {

        list: {
        
            selectors: {
                currencys_table: $('#currencys-table'),
            },
        
            init: function () {

                this.selectors.currencys_table.dataTable({

                    processing: false,
                    serverSide: true,
                    ajax: {
                        url: this.selectors.currencys_table.data('ajax_url'),
                        type: 'post',
                        data: { status: 1, trashed: false }
                    },
                    columns: [
                        
                        { data: 'name', name: 'name' },
                        { data: 'symbol', name: 'symbol' },
                        { data: 'value', name: 'value' },
                        { data: 'created_at', name: 'created_at' },
                        { data: 'actions', name: 'actions', searchable: false, sortable: false }

                    ],
                    order: [[0, "asc"]],
                    searchDelay: 500,
                    "createdRow": function (row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    }
                });
            }
        },

        edit: {
            init: function (locale) {
                FTX.tinyMCE.init(locale);                
            }
        },
    }
})();