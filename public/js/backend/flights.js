(function () {

    FTX.Flights = {

        list: {
        
            selectors: {
                flights_table: $('#flights-table'),
            },
        
            init: function () {

                this.selectors.flights_table.dataTable({

                    processing: false,
                    serverSide: true,
                    ajax: {
                        url: this.selectors.flights_table.data('ajax_url'),
                        type: 'post',
                        data: { status: 1, trashed: false }
                    },
                    columns: [
                        
                        { data: 'from_airport_id', name: 'from_airport_id' },
                        { data: 'to_airport_id', name: 'to_airport_id' },
                        { data: 'depature_time', name: 'depature_time' },
                        { data: 'stop_station', name: 'stop_station' },
                        { data: 'price', name: 'price' },
                        { data: 'actions', name: 'actions', searchable: false, sortable: false }

                    ],
                    order: [[0, "asc"]],
                    searchDelay: 500,
                    "createdRow": function (row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    }
                });
            }
        },

        edit: {
            init: function (locale) {
                FTX.tinyMCE.init(locale);                
            }
        },
    }
})();