(function() {
    FTX.Partners = {
        list: {
            selectors: {
                partners_table: $("#partners-table")
            },

            init: function() {
                this.selectors.partners_table.dataTable({
                    processing: false,
                    serverSide: true,
                    ajax: {
                        url: this.selectors.partners_table.data("ajax_url"),
                        type: "post",
                        data: { status: 1, trashed: false }
                    },
                    columns: [
                        { data: "name", name: "name" },
                        { data: "phone", name: "phone" },
                        { data: "address", name: "address" },
                        { data: "logo", name: "logo" },
                        { data: "created_at", name: "created_at" },
                        {
                            data: "actions",
                            name: "actions",
                            searchable: false,
                            sortable: false
                        }
                    ],
                    order: [[0, "asc"]],
                    searchDelay: 500,
                    createdRow: function(row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    }
                });
            }
        },

        edit: {
            init: function(locale) {
                FTX.tinyMCE.init();
            }
        }
    };
})();
