(function () {

    FTX.Points = {

        list: {
        
            selectors: {
                points_table: $('#points-table'),
            },
        
            init: function () {

                this.selectors.points_table.dataTable({

                    processing: false,
                    serverSide: true,
                    ajax: {
                        url: this.selectors.points_table.data('ajax_url'),
                        type: 'post',
                        data: { status: 1, trashed: false }
                    },
                    columns: [

                        { data: 'name', name: 'name' },
                        { data: 'phone', name: 'phone' },
                        { data: 'point', name: 'point' },
                        { data: 'updated_at', name: 'updated_at' },
                        { data: 'actions', name: 'actions', searchable: false, sortable: false }

                    ],
                    order: [[0, "asc"]],
                    searchDelay: 500,
                    "createdRow": function (row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    }
                });
            }
        },

        edit: {
            init: function (locale) {
                FTX.tinyMCE.init();                
            }
        },
    }
})();