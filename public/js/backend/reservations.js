(function () {

    FTX.Reservations = {

        list: {
        
            selectors: {
                reservations_table: $('#reservations-table'),
            },
        
            init: function () {

                this.selectors.reservations_table.dataTable({

                    processing: false,
                    serverSide: true,
                    ajax: {
                        url: this.selectors.reservations_table.data('ajax_url'),
                        type: 'post',
                        data: { status: 1, trashed: false }
                    },
                    columns: [

                        { data: 'person_fname', name: 'person_fname' },
                        { data: 'nationality', name: 'nationality' },
                        { data: 'flight_id', name: 'flight_id' },
                        { data: 'count', name: 'count' },
                        { data: 'created_at', name: 'created_at' },
                        { data: 'actions', name: 'actions', searchable: false, sortable: false }

                    ],
                    order: [[0, "desc"]],
                    searchDelay: 500,
                    "createdRow": function (row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    }
                });
            }
        },

        edit: {
            init: function (locale) {
                FTX.tinyMCE.init(locale);                
            }
        },
    }
})();