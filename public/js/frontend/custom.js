
function showPassword(){
    var x = document.getElementById("serial_number");
    var eye = document.getElementById("eye-icon");
    if (x.type === "password") {
      x.type = "text";
      eye.style = "color:red";
    } else {
      x.type = "password";
      eye.style = "color:green";
    }
  }

function deleteAttr(i) {
    var myobj = document.getElementById("passenger_" + i);
    myobj.remove();
};

function showGoF(thi, id){
    $('#go-not-found').addClass('hidden');
    $('.checked_go_flight').html("اختيار");
    console.log($('.choice').attr("data-state"));
    $('.choice').attr("data-state","1");
    console.log($('.choice').attr("data-state"));
    $('.checked_go_flight').removeClass('checked_go_flight');
    $('.f-active-go').removeClass('f-active-go');
    thi.classList.add('f-active-go');
    $('.go-box').addClass('hidden');
    $('#flight-'+id).removeClass('hidden');
    $('#go_id').val("");
};

function showReturnF(thi, id){
    $('#return-not-found').addClass('hidden');
    $('.checked_return_flight').html("اختيار");
    $('.checked_return_flight').removeClass('checked_return_flight');
    $('.f-active-return').removeClass('f-active-return');
    thi.classList.add('f-active-return');
    $('.return-box').addClass('hidden');
    $('#flight-'+id).removeClass('hidden');
    $('#return_id').val("");
};

function goFlight(th){
    
    if(th.innerHTML == "تم الاختيار" || th.innerHTML == "Choosed" ){
        $('#go_id').val("");
    }else{
        $('#go_id').val(th.id);
    }
    th.classList.toggle('checked_go_flight');
    th.innerHTML = th.innerHTML == "تم الاختيار" ?"اختيار":"تم الاختيار" ;
};

function returnFlight(th){

    if(th.innerHTML == "تم الاختيار" || th.innerHTML == "Choosed" ){
        $('#return_id').val("");
    }else{
        $('#return_id').val(th.id);
    }
    th.classList.toggle('checked_return_flight');
    th.innerHTML = th.innerHTML == "تم الاختيار" ?"اختيار":"تم الاختيار" ;
};


var countries_en = ["Syria", "Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe"];
var countries_ar = ["الجمهورية العربية السورية", "المملكة العربية السعودية", "إثيوبيا", "أذربيجان", "أرمينيا", "أروبا", "إريتريا", "أسبانيا", "أستراليا", "إستونيا", "أفغانستان", "إقليم المحيط الهندي البريطاني", "إكوادور", "الأرجنتين", "الأردن", "الإمارات العربية المتحدة", "ألبانيا", "البرازيل", "البرتغال", "البوسنة والهرسك", "الجابون", "الجزائر", "الدانمارك", "الرأس الأخضر", "السلطة الفلسطينية", "السلفادور", "السنغال", "السودان", "السويد", "الصومال", "الصين", "العراق", "الفلبين", "الكاميرون", "الكونغو", "الكونغو (جمهورية الكونغو الديمقراطية)", "الكويت", "ألمانيا", "المجر", "المغرب", "المكسيك", "المملكة المتحدة", "المناطق الفرنسية الجنوبية ومناطق انتراكتيكا", "النرويج", "النمسا", "النيجر", "الهند", "الولايات المتحدة", "اليابان", "اليمن", "اليونان", "أنتاركتيكا", "أنتيغوا وبربودا", "أندورا", "إندونيسيا", "أنغولا", "أنغويلا", "أوروجواي", "أوزبكستان", "أوغندا", "أوكرانيا", "إيران", "أيرلندا", "أيسلندا", "إيطاليا", "بابوا-غينيا الجديدة", "باراجواي", "باربادوس", "باكستان", "بالاو", "برمودا", "بروناي", "بلجيكا", "بلغاريا", "بنجلاديش", "بنما", "بنين", "بوتان", "بوتسوانا", "بورتو ريكو", "بوركينا فاسو", "بوروندي", "بولندا", "بوليفيا", "بولينزيا الفرنسية", "بيرو", "بيلاروس", "بيليز", "تايلاند", "تايوان", "تركمانستان", "تركيا", "ترينيداد وتوباجو", "تشاد", "تشيلي", "تنزانيا", "توجو", "توفالو", "توكيلاو", "تونجا", "تونس", "تيمور الشرقية (تيمور الشرقية)", "جامايكا", "جامبيا", "جبل طارق", "جرينلاند", "جزر الأنتيل الهولندية", "جزر البتكارين", "جزر البهاما", "جزر العذراء البريطانية", "جزر العذراء، الولايات المتحدة", "جزر القمر", "جزر الكوكوس (كيلين)", "جزر المالديف", "جزر تركس وكايكوس", "جزر ساموا الأمريكية", "جزر سولومون", "جزر فايرو", "جزر فرعية تابعة للولايات المتحدة", "جزر فوكلاند (أيزلاس مالفيناس)", "جزر فيجي", "جزر كايمان", "جزر كوك", "جزر مارشال", "جزر ماريانا الشمالية", "جزيرة الكريسماس", "جزيرة بوفيه", "جزيرة مان", "جزيرة نورفوك", "جزيرة هيرد وجزر ماكدونالد", "جمهورية أفريقيا الوسطى", "جمهورية التشيك", "جمهورية الدومينيكان", "جنوب أفريقيا", "جواتيمالا", "جواديلوب", "جوام", "جورجيا", "جورجيا الجنوبية وجزر ساندويتش الجنوبية", "جيانا", "جيانا الفرنسية", "جيبوتي", "جيرسي", "جيرنزي", "دولة الفاتيكان", "دومينيكا", "رواندا", "روسيا", "رومانيا", "ريونيون", "زامبيا", "زيمبابوي", "ساموا", "سان مارينو", "سانت بيير وميكولون", "سانت فينسنت وجرينادينز", "سانت كيتس ونيفيس", "سانت لوشيا", "سانت هيلينا", "ساوتوماي وبرينسيبا", "سفالبارد وجان ماين", "سلوفاكيا", "سلوفينيا", "سنغافورة", "سوازيلاند", "سوريا", "سورينام", "سويسرا", "سيراليون", "سيريلانكا", "سيشل", "صربيا", "طاجيكستان", "عمان", "غانا", "غرينادا", "غينيا", "غينيا الاستوائية", "غينيا بيساو", "فانواتو", "فرنسا", "فنزويلا", "فنلندا", "فيتنام", "قبرص", "قطر", "قيرقيزستان", "كازاخستان", "كاليدونيا الجديدة", "كامبوديا", "كرواتيا", "كندا", "كوبا", "كوت ديفوار (ساحل العاج)", "كوريا", "كوريا الشمالية", "كوستاريكا", "كولومبيا", "كيريباتي", "كينيا", "لاتفيا", "لاوس", "لبنان", "لختنشتاين", "لوكسمبورج", "ليبيا", "ليبيريا", "ليتوانيا", "ليسوتو", "مارتينيك", "ماكاو", "ماكرونيزيا", "مالاوي", "مالطا", "مالي", "ماليزيا", "مايوت", "مدغشقر", "مصر", "مقدونيا، جمهورية يوغوسلافيا السابقة", "مملكة البحرين", "منغوليا", "موريتانيا", "موريشيوس", "موزمبيق", "مولدوفا", "موناكو", "مونتسيرات", "مونتينيغرو", "ميانمار", "ناميبيا", "ناورو", "نيبال", "نيجيريا", "نيكاراجوا", "نيوا", "نيوزيلندا", "هايتي", "هندوراس", "هولندا", "هونغ كونغ SAR", "واليس وفوتونا"];

function addcountries() {
    var str = '';
    countries_ar.forEach((item, index) => {
        str += '\
        <option value="'+ item + '">' + item + '</option>\
        ';
    }
    );
    return str;
}

$(document).ready(function () {

    "use strict";

    $("#btn-continue").click(function(){
        
        if(!$("#go_id").val()){
            $("#error_box").removeClass('hidden');
            $("#error_msg").html("لا يمكن متابعة الحجز الا بعد اختيار رحلة");
        }
        else{
            if($("#return_id").length && !$("#return_id").val() ){
                $("#error_box").removeClass('hidden');
                $("#error_msg").html("لا يمكن متابعة الحجز الا بعد اختيار رحلة العودة");
            }
        }
    });

    /*==================================

* Author        : "ThemeSine"

* Template Name : Travel HTML Template

* Version       : 1.0

==================================== */


    /*=========== TABLE OF CONTENTS ===========

1. Scroll To Top
2. Range js
3. Countdown timer
4. owl carousel
5. datepicker
6. Smooth Scroll spy
7. Animation support
======================================*/


    // 1. Scroll To Top 

    $(window).on('scroll', function () {

        if ($(this).scrollTop() > 600) {

            $('.return-to-top').fadeIn();

        } else {

            $('.return-to-top').fadeOut();

        }

    });

    $('.return-to-top').on('click', function () {

        $('html, body').animate({

            scrollTop: 0

        }, 1500);

        return false;

    });

    // 2. range js

    $("#slider-range").slider({
        range: true,
        min: 0,
        max: 12000,
        values: [2677, 9241],
        slide: function (event, ui) {
            $("#amount").val("$" + ui.values[0] + " - $" + ui.values[1]);
        }
    });
    $("#amount").val("$" + $("#slider-range").slider("values", 0) +
        " - $" + $("#slider-range").slider("values", 1));


    // Quantity Buttons Shop

    $(".qtyplus").on("click", function () {
        var b = $(this).parents(".quantity-form").find("input.qty"),
            c = parseInt(b.val(), 10) + 1,
            d = parseInt(b.attr("max"), 10);
        d || (d = 9999999999), c <= d && (b.val(c), b.change())
    });
    $(".qtyminus").on("click", function () {
        var b = $(this).parents(".quantity-form").find("input.qty"),
            c = parseInt(b.val(), 10) - 1,
            d = parseInt(b.attr("min"), 10);
        d || (d = 1), c >= d && (b.val(c), b.change())
    });


    // 3.Countdown timer 

    function makeTimer() {

        var endTime = new Date("Augest 7, 2021 12:00:00 PDT");
        var endTime = (Date.parse(endTime)) / 1000;

        var now = new Date();
        var now = (Date.parse(now) / 1000);

        var timeLeft = endTime - now;

        var days = Math.floor(timeLeft / 86400);
        var hours = Math.floor((timeLeft - (days * 86400)) / 3600);
        var minutes = Math.floor((timeLeft - (days * 86400) - (hours * 3600)) / 60);
        var seconds = Math.floor((timeLeft - (days * 86400) - (hours * 3600) - (minutes * 60)));

        if (hours < "10") { hours = "0" + hours; }
        if (minutes < "10") { minutes = "0" + minutes; }
        if (seconds < "10") { seconds = "0" + seconds; }

        $("#days").html(days + '<span class="camp">Days</span>');
        $("#hours").html(hours + '<span class="camp">Hour</span>');
        $("#minutes").html(minutes + '<span class="camp">Minute</span>');
        $("#seconds").html(seconds + '<span class="camp">Second</span>');

    }

    setInterval(function () { makeTimer(); }, 1000);

    // 4. owl carousel

    // i. #testimonial-carousel


    var owl = $('#testemonial-carousel');
    owl.owlCarousel({
        items: 3,
        margin: 0,

        loop: true,
        autoplay: true,
        smartSpeed: 1000,

        //nav:false,
        //navText:["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],

        dots: true,
        autoplayHoverPause: true,

        responsiveClass: true,
        responsive: {
            0: {
                items: 1
            },
            640: {
                items: 1
            },
            767: {
                items: 2
            },
            992: {
                items: 3
            }
        }


    });

    const options = { 
        month: '2-digit', 
        day: '2-digit',
        year: 'numeric', 
      };
      
    // 5. datepicker
    $('[data-toggle="datepicker"]').datepicker(
        {startDate : new Date().toLocaleString('en-US')}
    );

    // 6. Smooth Scroll spy

    $('.header-area').sticky({
        topSpacing: 0
    });

    //=============

    $('li.smooth-menu a').bind("click", function (event) {
        event.preventDefault();
        var anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $(anchor.attr('href')).offset().top - -1
        }, 1200, 'easeInOutExpo');
    });

    $('body').scrollspy({
        target: '.navbar-collapse',
        offset: 0
    });

    // 7.animation support

    $(window).load(function () {

        $(".about-us-txt h2").removeClass("animated fadeInUp").css({ 'opacity': '0' });
        $(".about-us-txt button").removeClass("animated fadeInDown").css({ 'opacity': '0' });
    });

    $(window).load(function () {

        $(".about-us-txt h2").addClass("animated fadeInUp").css({ 'opacity': '0' });
        $(".about-us-txt button").addClass("animated fadeInDown").css({ 'opacity': '0' });

    });


    function makeCounter() {
        var count = 100;
        return function () {
            count++;
            return count;
        };
    };
    var i = makeCounter();

    $('#addPassenger').click(function () {
        var i_value = i();
        $('#new_passenger').append('\
            <div id="passenger_'+ i_value + '" class="row mar"> \
                <div class="col-xs-12 col-sm-3">\
            <div class="def">\
            مسافر بالغ  <i onClick="deleteAttr(' + i_value + ')" class="fa fa-times remove-icon" data-toggle="tooltip" title="حذف المسافر" aria-hidden="true"></i>\
            </div>\
        </div>\
        <div class="col-xs-12 col-sm-9 border-right">\
            <div class="row">\
                <div class="col-xs-12 col-sm-4">\
                    <div class="form-seg">\
                        <label for="name">First Name</label>\
                        <input type="text" class="form-control" name="person_fname[]" />\
                    </div>\
                </div>\
\
                <div class="col-xs-12 col-sm-4">\
                    <div class="form-seg">\
                        <label for="person_sname">Father Name</label>\
                        <input type="text" class="form-control" name="person_sname[]" />\
                    </div>\
                </div>\
                <div class="col-xs-12 col-sm-4">\
                    <div class="form-seg">\
                        <label for="person_tname">Last Name</label>\
                        <input type="text" class="form-control" name="person_tname[]" />\
                    </div>\
                </div>\
                <div class="col-xs-12 col-sm-4">\
                    <div class="form-seg">\
                        <label for="mobile">Phone</label>\
                        <input type="text" class="form-control" name="mobile" pattern="0-9" />\
                    </div>\
                </div>\
                <div class="col-xs-12 col-sm-4">\
                    <div class="form-seg">\
                        <label for="birthday">Birthday</label>\
                        <input type="date" class="form-control" name="birthday[]" />\
                    </div>\
                </div>\
                <div class="col-xs-12 col-sm-4">\
                    <div class="form-seg">\
                        <label for="nationality">Nationality</label>\
                        <select class="form-control" name="nationality[]">\
                        '+addcountries()+'\
                        </select>\
                    </div>\
                </div>\
                <div class="col-xs-12 col-sm-4">\
                    <div class="form-seg">\
                        <label for="passport">Passport number</label>\
                        <input type="text" class="form-control" name="passport[]" />\
                    </div>\
                </div>\
                <div class="col-xs-12 col-sm-4">\
                    <div class="form-seg">\
                        <label for="passport_expiretion">Expire passport at</label>\
                        <input type="date" class="form-control" name="passport_expiretion[]" />\
                    </div>\
                </div>\
                <div class="col-xs-12 col-sm-4">\
                    <div class="form-seg">\
                        <label for="passport_country">Country issue passpord</label>\
                        <select class="form-control" name="passport_country[]">\
                        '+addcountries()+'\
                        </select>\
                    </div>\
                </div>\
            </div>\
            </div>\
        </div>');
    });

    $('#addYoungPassenger').click(function () {
        var i_value = i();
        $('#new_passenger').append('\
            <div id="passenger_'+ i_value + '" class="row mar"> \
                <div class="col-xs-12 col-sm-3">\
            <div class="def">\
            مسافر طفل  <i onClick="deleteAttr(' + i_value + ')" class="fa fa-times remove-icon" data-toggle="tooltip" title="حذف المسافر" aria-hidden="true"></i>\
            </div>\
        </div>\
        <div class="col-xs-12 col-sm-9 border-right">\
            <div class="row">\
                <div class="col-xs-12 col-sm-4">\
                    <div class="form-seg">\
                        <label for="name">First Name</label>\
                        <input type="text" class="form-control" name="person_fname[]" />\
                    </div>\
                </div>\
\
                <div class="col-xs-12 col-sm-4">\
                    <div class="form-seg">\
                        <label for="person_sname">Father Name</label>\
                        <input type="text" class="form-control" name="person_sname[]" />\
                    </div>\
                </div>\
                <div class="col-xs-12 col-sm-4">\
                    <div class="form-seg">\
                        <label for="person_tname">Last Name</label>\
                        <input type="text" class="form-control" name="person_tname[]" />\
                    </div>\
                </div>\
                <div class="col-xs-12 col-sm-4">\
                    <div class="form-seg">\
                        <label for="birthday">Birthday</label>\
                        <input type="date" class="form-control" name="birthday[]" />\
                    </div>\
                </div>\
                <div class="col-xs-12 col-sm-4">\
                    <div class="form-seg">\
                        <label for="nationality">Nationality</label>\
                        <select class="form-control" name="nationality[]">\
                        '+addcountries()+'\
                        </select>\
                    </div>\
                </div>\
                <div class="col-xs-12 col-sm-4">\
                    <div class="form-seg">\
                        <label for="passport">Passport number</label>\
                        <input type="text" class="form-control" name="passport[]" />\
                    </div>\
                </div>\
                <div class="col-xs-12 col-sm-4">\
                    <div class="form-seg">\
                        <label for="passport_expiretion">Expire passport at</label>\
                        <input type="date" class="form-control" name="passport_expiretion[]" />\
                    </div>\
                </div>\
                <div class="col-xs-12 col-sm-4">\
                    <div class="form-seg">\
                        <label for="passport_country">Country issue passpord</label>\
                        <select class="form-control" name="passport_country[]">\
                        '+addcountries()+'\
                        </select>\
                    </div>\
                </div>\
            </div>\
            </div>\
        </div>');
    });

    $('#addBabyPassenger').click(function () {
        var i_value = i();
        $('#new_passenger').append('\
            <div id="passenger_'+ i_value + '" class="row mar"> \
                <div class="col-xs-12 col-sm-3">\
            <div class="def">\
            مسافر رضيع  <i onClick="deleteAttr(' + i_value + ')" class="fa fa-times remove-icon" data-toggle="tooltip" title="حذف المسافر" aria-hidden="true"></i>\
            </div>\
        </div>\
        <div class="col-xs-12 col-sm-9 border-right">\
            <div class="row">\
                <div class="col-xs-12 col-sm-4">\
                    <div class="form-seg">\
                        <label for="name">First Name</label>\
                        <input type="text" class="form-control" name="person_fname[]" required/>\
                    </div>\
                </div>\
\
                <div class="col-xs-12 col-sm-4">\
                    <div class="form-seg">\
                        <label for="person_sname">Father Name</label>\
                        <input type="text" class="form-control" name="person_sname[]" required/>\
                    </div>\
                </div>\
                <div class="col-xs-12 col-sm-4">\
                    <div class="form-seg">\
                        <label for="person_tname">Last Name</label>\
                        <input type="text" class="form-control" name="person_tname[]" required/>\
                    </div>\
                </div>\
                <div class="col-xs-12 col-sm-4">\
                    <div class="form-seg">\
                        <label for="birthday">Birthday</label>\
                        <input type="date" class="form-control" name="birthday[]" required/>\
                    </div>\
                </div>\
                <div class="col-xs-12 col-sm-4">\
                    <div class="form-seg">\
                        <label for="nationality">Nationality</label>\
                        <select class="form-control" name="nationality[]">\
                        '+addcountries()+'\
                        </select>\
                    </div>\
                </div>\
                <div class="col-xs-12 col-sm-4">\
                    <div class="form-seg">\
                        <label for="passport">Passport number</label>\
                        <input type="text" class="form-control" name="passport[]" required/>\
                    </div>\
                </div>\
                <div class="col-xs-12 col-sm-4">\
                    <div class="form-seg">\
                        <label for="passport_expiretion">Expire passport at</label>\
                        <input type="date" class="form-control" name="passport_expiretion[]" required/>\
                    </div>\
                </div>\
                <div class="col-xs-12 col-sm-4">\
                    <div class="form-seg">\
                        <label for="passport_country">Country issue passpord</label>\
                        <select class="form-control" name="passport_country[]">\
                        '+addcountries()+'\
                        </select>\
                    </div>\
                </div>\
            </div>\
            </div>\
        </div>');
    });

    $('#dep_time_radio').click(
        function(){
            $('#dep_time').hide();
        }
    )

    $('#ret_time_radio').click(
        function(){
            $('#dep_time').show();
        }
    )

    // Carousel flight date
    var itemsMainDiv = ('.MultiCarousel');
    var itemsDiv = ('.MultiCarousel-inner');
    var itemWidth = "";

    $('.leftLst, .rightLst').click(function () {
        var condition = $(this).hasClass("leftLst");
        if (condition)
            click(0, this);
        else
            click(1, this)
    });

    ResCarouselSize();




    $(window).resize(function () {
        ResCarouselSize();
    });

    //this function define the size of the items
    function ResCarouselSize() {
        var incno = 0;
        var dataItems = ("data-items");
        var itemClass = ('.item');
        var id = 0;
        var btnParentSb = '';
        var itemsSplit = '';
        var sampwidth = $(itemsMainDiv).width();
        var bodyWidth = $('body').width();
        $(itemsDiv).each(function () {
            id = id + 1;
            var itemNumbers = $(this).find(itemClass).length;
            btnParentSb = $(this).parent().attr(dataItems);
            itemsSplit = btnParentSb.split(',');
            $(this).parent().attr("id", "MultiCarousel" + id);


            if (bodyWidth >= 1200) {
                incno = itemsSplit[3];
                itemWidth = sampwidth / incno;
            }
            else if (bodyWidth >= 992) {
                incno = itemsSplit[2];
                itemWidth = sampwidth / incno;
            }
            else if (bodyWidth >= 768) {
                incno = itemsSplit[1];
                itemWidth = sampwidth / incno;
            }
            else {
                incno = itemsSplit[0];
                itemWidth = sampwidth / incno;
            }
            $(this).css({ 'transform': 'translateX(-2000px)', 'width': itemWidth * itemNumbers });
            $(this).find(itemClass).each(function () {
                $(this).outerWidth(itemWidth);
            });

            $(".leftLst").removeClass("over");
            $(".rightLst").removeClass("over");

        });
    }


    //this function used to move the items
    function ResCarousel(e, el, s) {
        var leftBtn = ('.leftLst');
        var rightBtn = ('.rightLst');
        var translateXval = '';
        var divStyle = $(el + ' ' + itemsDiv).css('transform');
        var values = divStyle.match(/-?[\d\.]+/g);
        var xds = Math.abs(values[4]);
        if (e == 0) {
            translateXval = parseInt(xds) - parseInt(itemWidth * s);
            $(el + ' ' + rightBtn).removeClass("over");

            if (translateXval <= itemWidth / 2) {
                translateXval = 0;
                $(el + ' ' + leftBtn).addClass("over");
            }
        }
        else if (e == 1) {
            var itemsCondition = $(el).find(itemsDiv).width() - $(el).width();
            translateXval = parseInt(xds) + parseInt(itemWidth * s);
            $(el + ' ' + leftBtn).removeClass("over");

            if (translateXval >= itemsCondition - itemWidth / 2) {
                translateXval = itemsCondition;
                $(el + ' ' + rightBtn).addClass("over");
            }
        }
        $(el + ' ' + itemsDiv).css('transform', 'translateX(' + -translateXval + 'px)');
    }

    //It is used to get some elements from btn
    function click(ell, ee) {
        var Parent = "#" + $(ee).parent().attr("id");
        var slide = $(Parent).attr("data-slide");
        ResCarousel(ell, Parent, slide);
    } 

});

