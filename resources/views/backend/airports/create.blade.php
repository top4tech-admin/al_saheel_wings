@extends('backend.layouts.app')

@section('title', 'Airports' . ' | ' . 'create')

@section('breadcrumb-links')
    @include('backend.airports.includes.breadcrumb-links')
@endsection

@section('content')
    {{ Form::open(['route' => 'admin.airports.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id' => 'create-permission', 'files' => true]) }}

    <div class="card">
        @include('backend.airports.form')
        @include('backend.components.footer-buttons', ['cancelRoute' => 'admin.airports.index'])
    </div><!--card-->
    {{ Form::close() }}
@endsection