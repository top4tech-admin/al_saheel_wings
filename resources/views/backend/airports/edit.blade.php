@extends('backend.layouts.app')

@section('title', 'Airports' . ' | ' . 'Edit')

@section('breadcrumb-links')
    @include('backend.airports.includes.breadcrumb-links')
@endsection

@section('content')
    {{ Form::model($airport, ['route' => ['admin.airports.update', $airport], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH', 'id' => 'edit-role', 'files' => true]) }}

    <div class="card">
        @include('backend.airports.form')
        @include('backend.components.footer-buttons', [ 'cancelRoute' => 'admin.airports.index', 'id' => $airport->id ])
    </div><!--card-->
    {{ Form::close() }}
@endsection