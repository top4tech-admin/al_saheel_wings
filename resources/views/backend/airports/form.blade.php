<div class="card-body">
    <div class="row">
        <div class="col-sm-5">
            <h4 class="card-title mb-0">
            Airports
                <small class="text-muted">{{ (isset($airport)) ? 'Edit Airport' : 'Create Airport' }}</small>
            </h4>
        </div>
        <!--col-->
    </div>
    <!--row-->

    <hr>

    <div class="row mt-4 mb-4">
        <div class="col">
            <div class="form-group row">
                {{ Form::label('name_ar', "Airport AR name", ['class' => 'col-md-2 from-control-label required']) }}

                <div class="col-md-10">
                    {{ Form::text('name_ar', null, ['class' => 'form-control', 'placeholder' => 'Arabic name', 'required' => 'required']) }}
                </div>
                <!--col-->
            </div>
            <!--form-group-->

            <div class="form-group row">
                {{ Form::label('name_en', "Airport EN name", ['class' => 'col-md-2 from-control-label required']) }}

                <div class="col-md-10">
                    {{ Form::text('name_en', null, ['class' => 'form-control', 'placeholder' => 'English name', 'required' => 'required']) }}
                </div>
                <!--col-->
            </div>
            <!--form-group-->

        </div>
        <!--col-->
    </div>
    <!--row-->
</div>
<!--card-body-->

@section('pagescript')
<script type="text/javascript">
    FTX.Utils.documentReady(function() {
        FTX.Airports.edit.init("{{ config('locale.languages.' . app()->getLocale())[1] }}");
    });
</script>
@stop