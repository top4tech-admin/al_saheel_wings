@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . 'Airports')

@section('breadcrumb-links')
@include('backend.airports.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    Airports <small class="text-muted">Airport List</small>
                </h4>
            </div>
            <!--col-->
        </div>
        <!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table id="airports-table" class="table" data-ajax_url="{{ route("admin.airports.get") }}">
                        <thead>
                            <tr>
                                <th>Name Arabic</th>
                                <th>Name English</th>
                                <th data-orderable="false">Created at</th>
                                <th>Updated at</th>
                                <th>{{ trans('labels.general.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <!--col-->
        </div>
        <!--row-->

    </div>
    <!--card-body-->
</div>
<!--card-->
@endsection

@section('pagescript')
<script>
    FTX.Utils.documentReady(function() {
        FTX.Airports.list.init();
    });
</script>
@endsection