@extends('backend.layouts.app')

@section('title', 'Currencies' . ' | ' . 'Create')

@section('breadcrumb-links')
    @include('backend.currencys.includes.breadcrumb-links')
@endsection

@section('content')
    {{ Form::open(['route' => 'admin.currencys.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id' => 'create-permission', 'files' => true]) }}

    <div class="card">
        @include('backend.currencys.form')
        @include('backend.components.footer-buttons', ['cancelRoute' => 'admin.currencys.index'])
    </div><!--card-->
    {{ Form::close() }}
@endsection