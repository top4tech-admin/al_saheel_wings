@extends('backend.layouts.app')

@section('title', 'Currencies' . ' | ' . 'Edit')

@section('breadcrumb-links')
    @include('backend.currencys.includes.breadcrumb-links')
@endsection

@section('content')
    {{ Form::model($currency, ['route' => ['admin.currencys.update', $currency], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH', 'id' => 'edit-role', 'files' => true]) }}

    <div class="card">
        @include('backend.currencys.form')
        @include('backend.components.footer-buttons', [ 'cancelRoute' => 'admin.currencys.index', 'id' => $currency->id ])
    </div><!--card-->
    {{ Form::close() }}
@endsection