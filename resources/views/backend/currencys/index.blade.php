@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . 'Currencies')

@section('breadcrumb-links')
@include('backend.currencys.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                 العملات
                 العملات
                </h4>
            </div>
            <!--col-->
        </div>
        <!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table id="currencys-table" class="table" data-ajax_url="{{ route("admin.currencys.get") }}">
                        <thead>
                            <tr>
                                <th>اسم العملة</th>
                                <th>الرمز</th>
                                <th data-orderable="false">القيمة</th>
                                <th>{{ trans('labels.backend.access.pages.table.createdat') }}</th>
                                <th>{{ trans('labels.general.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <!--col-->
        </div>
        <!--row-->

    </div>
    <!--card-body-->
</div>
<!--card-->
@endsection

@section('pagescript')
<script>
    FTX.Utils.documentReady(function() {
        FTX.Currencys.list.init();
    });
</script>
@endsection