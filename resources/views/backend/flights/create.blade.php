@extends('backend.layouts.app')

@section('title', 'Flights' . ' | ' . 'Create')

@section('breadcrumb-links')
    @include('backend.flights.includes.breadcrumb-links')
@endsection

@section('content')
    {{ Form::open(['route' => 'admin.flights.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id' => 'create-permission', 'files' => true]) }}

    <div class="card">
        @include('backend.flights.form')
        @include('backend.components.footer-buttons', ['cancelRoute' => 'admin.flights.index'])
    </div><!--card-->
    {{ Form::close() }}
@endsection