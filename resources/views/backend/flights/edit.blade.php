@extends('backend.layouts.app')

@section('title', 'Flights' . ' | ' . 'Edit')

@section('breadcrumb-links')
    @include('backend.flights.includes.breadcrumb-links')
@endsection

@section('content')
    {{ Form::model($flight, ['route' => ['admin.flights.update', $flight], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH', 'id' => 'edit-role', 'files' => true]) }}

    <div class="card">
        @include('backend.flights.form')
        @include('backend.components.footer-buttons', [ 'cancelRoute' => 'admin.flights.index', 'id' => $flight->id ])
    </div><!--card-->
    {{ Form::close() }}
@endsection