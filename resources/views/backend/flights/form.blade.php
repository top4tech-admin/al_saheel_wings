<div class="card-body">
    <div class="row">
        <div class="col-sm-5">
            <h4 class="card-title mb-0">
            Flights
                <small class="text-muted">{{ (isset($flight)) ? 'Edit' : 'Create'}}</small>
            </h4>
        </div>
        <!--col-->
    </div>
    <!--row-->

    <hr>

    <div class="row mt-4 mb-4">
        <div class="col">
            <div class="form-group row">
                {{ Form::label('price', 'Price', ['class' => 'col-md-2 from-control-label required']) }}

                <div class="col-md-10">
                    {{ Form::number('price', null, ['class' => 'form-control', 'required' => 'required']) }}
                </div>
                <!--col-->
            </div>
            <!--form-group-->
            
            <div class="form-group row">
                {{ Form::label('yunge_price', 'Child Price', ['class' => 'col-md-2 from-control-label required']) }}

                <div class="col-md-10">
                    {{ Form::number('yunge_price', null, ['class' => 'form-control', 'required' => 'required']) }}
                </div>
                <!--col-->
            </div>
            <!--form-group-->

            <div class="form-group row">
                {{ Form::label('baby_price', 'Infant Price', ['class' => 'col-md-2 from-control-label required']) }}

                <div class="col-md-10">
                    {{ Form::number('baby_price', null, ['class' => 'form-control', 'required' => 'required']) }}
                </div>
                <!--col-->
            </div>
            <!--form-group-->

            <div class="form-group row">
                {{ Form::label('tax', 'Tax', ['class' => 'col-md-2 from-control-label required']) }}

                <div class="col-md-10">
                    {{ Form::number('tax', null, ['class' => 'form-control', 'required' => 'required', 'disabled' => 'disabled']) }}
                </div>
                <!--col-->
            </div>
            <!--form-group-->

            <div class="form-group row">
                {{ Form::label('name', 'Flight Name', ['class' => 'col-md-2 from-control-label required']) }}

                <div class="col-md-10">
                    {{ Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) }}
                </div>
                <!--col-->
            </div>
            <!--form-group-->

            <div class="form-group row">
                {{ Form::label('from_airport_id', 'From airport id', ['class' => 'col-md-2 from-control-label required']) }}

                <div class="col-md-10">
                    <select id="select-from" type="select" class="from-control from-control-select" name="from_airport_id" required>
                    <option value="" disabled selected>Select from</option>
                        @foreach($airports as $airport)
                            <option value="{{$airport->id}}" {{isset($flight) && $airport->id == $flight->from_airport_id ? 'selected' : ''}}>{{$airport->name_ar}}</option>
                        @endforeach
                    </select>
                    <i id="spinner" class="fa-2x fas fa-spinner fa-pulse hidden"></i>
                </div>
                <!--col-->
            </div>
            <!--form-group-->

            <div class="form-group row">
                {{ Form::label('to_airport_id', 'To airport id', ['class' => 'col-md-2 from-control-label required']) }}

                <div class="col-md-10">
                    <select id="select-to" type="select" class="from-control from-control-select" name="to_airport_id" required>
                    <option value="" disabled selected>Select to</option>
                        @foreach($airports as $airport)
                        <option value="{{$airport->id}}" {{isset($flight) && $airport->id == $flight->to_airport_id ? 'selected' : ''}}>{{$airport->name_ar}}</option>
                        @endforeach
                    </select>
                    <i id="spinner" class="fa-2x fas fa-spinner fa-pulse hidden"></i>
                </div>
                <!--col-->
            </div>
            <!--form-group-->

            <div class="form-group row">
                {{ Form::label('stop_station', 'Stop station', ['class' => 'col-md-2 from-control-label required']) }}

                <div class="col-md-10">
                    <select id="select-stop_station" type="select" class="from-control from-control-select" name="stop_station" required>
                    <option value="" disabled selected>Select Stop</option>
                        <option value="0" {{isset($flight) && $flight->stop_station == 0 ? 'selected' : ''}}>Direct</option>
                        <option value="1" {{isset($flight) && $flight->stop_station == 1 ? 'selected' : ''}}>1</option>
                        <option value="2" {{isset($flight) && $flight->stop_station == 2 ? 'selected' : ''}}>2</option>
                        <option value="3" {{isset($flight) && $flight->stop_station == 3 ? 'selected' : ''}}>3</option>
                    
                    </select>
                    <i id="spinner" class="fa-2x fas fa-spinner fa-pulse hidden"></i>
                </div>
                <!--col-->
            </div>
            <!--form-group-->

            <div class="form-group row">
                {{ Form::label('grade', 'Grade', ['class' => 'col-md-2 from-control-label required']) }}

                <div class="col-md-10">
                    <select id="grade" type="select" class="from-control from-control-select" name="grade" required>
                    <option value="" disabled selected>Select Class</option>
                        <option value="Business" {{isset($flight) && $flight->grade == "Business" ? 'selected' : ''}}>Business</option>
                        <option value="Economy" {{isset($flight) && $flight->grade == "Economy" ? 'selected' : ''}}>Economy</option>
                     </select>
                    <i id="spinner" class="fa-2x fas fa-spinner fa-pulse hidden"></i>
                </div>
                <!--col-->
            </div>
            <!--form-group-->

            <div class="form-group row">
                {{ Form::label('depature_time', 'Depature time', ['class' => 'col-md-2 from-control-label required']) }}
                <div class="col-md-10">
                    <input id="depature_time" name="depature_time" value="{{isset($flight) ? $flight->depature_time : ''}}" class="input" autocomplete="off" style="width: 195px;"/>
                </div>
                <!--col-->
            </div>
            <!--form-group-->

            <div class="form-group row">
                {{ Form::label('arrival_time', 'Arrival time', ['class' => 'col-md-2 from-control-label required']) }}
                <div class="col-md-10">
                    <input id="arrival_time" name="arrival_time" value="{{isset($flight) ? $flight->arrival_time : ''}}" class="input" autocomplete="off" style="width: 195px;"/>
                </div>
                <!--col-->
            </div>
            <!--form-group-->

        </div>
        <!--col-->
    </div>
    <!--row-->
</div>
<!--card-body-->

@section('pagescript')
<script type="text/javascript">
    FTX.Utils.documentReady(function() {
        FTX.Flights.edit.init("{{ config('locale.languages.' . app()->getLocale())[1] }}");
    });
    rome(depature_time);
    rome(arrival_time);
</script>
@stop