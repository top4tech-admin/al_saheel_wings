@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . 'Flights')

@section('breadcrumb-links')
@include('backend.flights.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                رحلات جوية
                </h4>
            </div>
            <!--col-->
        </div>
        <!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table id="flights-table" class="table" data-ajax_url="{{ route("admin.flights.get") }}">
                        <thead>
                            <tr>
                                <th>From</th>
                                <th>To</th>
                                <th data-orderable="false">Created at</th>
                                <th>Stop St</th>
                                <th>Price</th>
                                <th>{{ trans('labels.general.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <!--col-->
        </div>
        <!--row-->

    </div>
    <!--card-body-->
</div>
<!--card-->
@endsection

@section('pagescript')
<script>
    FTX.Utils.documentReady(function() {
        FTX.Flights.list.init();
    });
</script>
@endsection