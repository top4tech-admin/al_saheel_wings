@extends('backend.layouts.app')

@section('title', 'Points'. ' | ' . 'Create')

@section('breadcrumb-links')
    @include('backend.points.includes.breadcrumb-links')
@endsection

@section('content')
    {{ Form::open(['route' => 'admin.points.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id' => 'create-permission', 'files' => true]) }}

    <div class="card">
        @include('backend.points.form')
        @include('backend.components.footer-buttons', ['cancelRoute' => 'admin.points.index'])
    </div><!--card-->
    {{ Form::close() }}
@endsection