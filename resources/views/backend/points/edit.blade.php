@extends('backend.layouts.app')

@section('title', 'Points'. ' | ' . 'Edit')

@section('breadcrumb-links')
    @include('backend.points.includes.breadcrumb-links')
@endsection

@section('content')
    {{ Form::model($point, ['route' => ['admin.points.update', $point], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH', 'id' => 'edit-role', 'files' => true]) }}

    <div class="card">
        @include('backend.points.form')
        @include('backend.components.footer-buttons', [ 'cancelRoute' => 'admin.points.index', 'id' => $point->id ])
    </div><!--card-->
    {{ Form::close() }}
@endsection