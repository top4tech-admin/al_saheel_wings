<div class="card-body">
    <div class="row">
        <div class="col-sm-5">
            <h4 class="card-title mb-0">
                {{ 'Points'}}
                <small class="text-muted">{{ (isset($page)) ? 'Edit' : 'Create' }}</small>
            </h4>
        </div>
        <!--col-->
    </div>
    <!--row-->

    <hr>

    <div class="row mt-4 mb-4">
        <div class="col">
            <div class="form-group row">
                {{ Form::label('name', 'اسم الزبون', ['class' => 'col-md-2 from-control-label required']) }}

                <div class="col-md-10">
                    {{ Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) }}
                </div>
                <!--col-->
            </div>
            <!--form-group-->

            <div class="form-group row">
                {{ Form::label('serial_number', 'الرقم السري', ['class' => 'col-md-2 from-control-label required']) }}

                <div class="col-md-10">
                    {{ Form::text('serial_number', null, ['class' => 'form-control', 'required' => 'required']) }}
                </div>
                <!--col-->
            </div>
            <!--form-group-->

            <div class="form-group row">
                {{ Form::label('phone','الموبايل', ['class' => 'col-md-2 from-control-label required']) }}

                <div class="col-md-10">
                    {{ Form::text('phone', null, ['class' => 'form-control', 'required' => 'required']) }}
                </div>
                <!--col-->
            </div>
            <!--form-group-->

            <div class="form-group row">
                {{ Form::label('email', 'email', ['class' => 'col-md-2 from-control-label required']) }}

                <div class="col-md-10">
                    {{ Form::text('email', null, ['class' => 'form-control', 'required' => 'required']) }}
                </div>
                <!--col-->
            </div>
            <!--form-group-->

            <div class="form-group row">
                {{ Form::label('point', 'عدد النقاط', ['class' => 'col-md-2 from-control-label required']) }}

                <div class="col-md-2">
                    {{ Form::number('point', null, ['class' => 'form-control', 'required' => 'required']) }}
                </div>
                <div class="col-md-1">
                    <input type="number" id="val" class="form-control"/>
                </div>
                <div class="col-md-1">
                    <button type="button" class="btn btn-success" id="add" onclick="addd()">إضافة</button>
                </div>
                <div class="col-md-1">
                    <button type="button" class="btn btn-danger" id="sub" onclick="subb()">تخفيض</button>
                </div>
                <!--col-->
            </div>
            <!--form-group-->

        </div>
        <!--col-->
    </div>
    <!--row-->
</div>
<!--card-body-->

<script>
    function addd(){
        var points = document.getElementById("point").value;
        var val = document.getElementById("val").value;
        document.getElementById("point").value = points - val  + 2*val;
    }

    function subb(){
        var points = document.getElementById("point").value;
        var val = document.getElementById("val").value;
        document.getElementById("point").value = points - val;
    }
</script>
@section('pagescript')
<script type="text/javascript">
    FTX.Utils.documentReady(function() {
        FTX.Points.edit.init("{{ config('locale.languages.' . app()->getLocale())[1] }}");
    });
</script>
@stop