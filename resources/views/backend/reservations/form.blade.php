<div class="card-body">
    <div class="row">
        <div class="col-sm-5">
            <h4 class="card-title mb-0">
                {{ 'Reservations'}}
                <small class="text-muted">{{ (isset($reservation)) ? 'Edit' : 'Create' }}</small>
            </h4>
        </div>
        <!--col-->
    </div>
    <!--row-->

    <hr>

    <div class="row mt-4 mb-4">
        <div class="col">
            <div class="form-group row">
                {{ Form::label('person_fname', 'person first name', ['class' => 'col-md-2 from-control-label required']) }}

                <div class="col-md-10">
                    {{ Form::text('person_fname', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.access.pages.name'), 'required' => 'required']) }}
                </div>
                <!--col-->
            </div>
            <!--form-group-->

            <div class="form-group row">
                {{ Form::label('person_sname', 'Father name', ['class' => 'col-md-2 from-control-label required']) }}

                <div class="col-md-10">
                    {{ Form::text('person_sname', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.access.pages.name'), 'required' => 'required']) }}
                </div>
                <!--col-->
            </div>
            <!--form-group-->

            <div class="form-group row">
                {{ Form::label('person_tname', 'last name', ['class' => 'col-md-2 from-control-label required']) }}

                <div class="col-md-10">
                    {{ Form::text('person_tname', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.access.pages.name'), 'required' => 'required']) }}
                </div>
                <!--col-->
            </div>
            <!--form-group-->

            <div class="form-group row">
                {{ Form::label('nationality', 'Nationality', ['class' => 'col-md-2 from-control-label required']) }}

                <div class="col-md-10">
                    {{ Form::text('nationality', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.access.pages.name'), 'required' => 'required']) }}
                </div>
                <!--col-->
            </div>
            <!--form-group-->

            <div class="form-group row">
                {{ Form::label('passport', 'Passport No', ['class' => 'col-md-2 from-control-label required']) }}

                <div class="col-md-10">
                    {{ Form::text('passport', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.access.pages.name'), 'required' => 'required']) }}
                </div>
                <!--col-->
            </div>
            <!--form-group-->

            <div class="form-group row">
                {{ Form::label('passport_expiretion', 'Passport expiretion', ['class' => 'col-md-2 from-control-label required']) }}

                <div class="col-md-10">
                    {{ Form::date('passport_expiretion', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.access.pages.name'), 'required' => 'required']) }}
                </div>
                <!--col-->
            </div>
            <!--form-group-->

            <div class="form-group row">
                {{ Form::label('round_trip', 'Round trip', ['class' => 'col-md-2 from-control-label required']) }}
                <div class="col-md-10">
                    <input type="checkbox" id="round_trip" name="round_trip" value="round_trip"  {{$reservation['round_trip'] == "0" ? "" : "checked"}}/>
                </div>
                <!--col-->
            </div>
            <!--form-group-->

            <div class="form-group row">
                {{ Form::label('email', 'email', ['class' => 'col-md-2 from-control-label required']) }}

                <div class="col-md-10">
                    {{ Form::email('email', null, ['class' => 'form-control', 'required' => 'required']) }}
                </div>
                <!--col-->
            </div>
            <!--form-group-->

            <div class="form-group row">
                {{ Form::label('birthday', 'Birthday', ['class' => 'col-md-2 from-control-label required']) }}

                <div class="col-md-10">
                    {{ Form::text('birthday', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.access.pages.name'), 'required' => 'required']) }}
                </div>
                <!--col-->
            </div>
            <!--form-group-->

            <div class="form-group row">
                {{ Form::label('flight_id', "Flight", ['class' => 'col-md-2 from-control-label required']) }}
                
                <div class="col-md-10">
                <select id="select-cat" type="select" class="from-control from-control-select" name="flight_id" required>
                    <option value="" disabled selected>Select</option>
                    @foreach($flights as $flight)
                       <option value="{{$flight->id}}" {{$flight->id == $reservation->flight_id ? 'selected' : ''}}>({{$flight->fromAirport['name_ar']?? 'pp'}}) -> ({{$flight->toAirport['name_ar']??'hc'}}) </option>
                    @endforeach
                   
                </select>
                    <i id="spinner" class="fa-2x fas fa-spinner fa-pulse hidden"></i>
                </div>
                <!--col-->
            </div>
            <!--form-group-->

            <div class="form-group row">
                {{ Form::label('Depature Time', 'Depature Time', ['class' => 'col-md-2 from-control-label required']) }}

                <div class="col-md-10">
                    <input type="text" value="{{$flight->depature_time}}" disabled>
                </div>
                <!--col-->
            </div>
            <!--form-group-->
        </div>
        <!--col-->
    </div>
    <!--row-->
    <?php $i=1;?>
    @foreach($reservation->relatedPerson as $res)
    <?php $i++;?>
        <hr/>
        <div class="col">
        <h2><?php echo 'المسافر '.$i;?></h2>
            <div class="form-group row">
                {{ Form::label('person_fname', 'person first name', ['class' => 'col-md-2 from-control-label required']) }}

                <div class="col-md-10">
                    <input class="form-control" value="{{$res->person_fname}}" required="required" name="lkfgjd" type="text">
                </div>
                <!--col-->
            </div>
            <!--form-group-->

            <div class="form-group row">
                {{ Form::label('person_sname', 'Father name', ['class' => 'col-md-2 from-control-label required']) }}

                <div class="col-md-10">
                    <input class="form-control" value="{{$res->person_sname}}" required="required" name="lkfgjd" type="text">
                </div>
                <!--col-->
            </div>
            <!--form-group-->

            <div class="form-group row">
                {{ Form::label('person_tname', 'last name', ['class' => 'col-md-2 from-control-label required']) }}

                <div class="col-md-10">
                    <input class="form-control" value="{{$res->person_tname}}" required="required" name="lkfgjd" type="text">
                </div>
                <!--col-->
            </div>
            <!--form-group-->

            <div class="form-group row">
                {{ Form::label('nationality', 'Nationality', ['class' => 'col-md-2 from-control-label required']) }}

                <div class="col-md-10">
                    <input class="form-control" value="{{$res->nationality}}" required="required" name="lkfgjd" type="text">
                </div>
                <!--col-->
            </div>
            <!--form-group-->

            <div class="form-group row">
                {{ Form::label('passport', 'Passport No', ['class' => 'col-md-2 from-control-label required']) }}

                <div class="col-md-10">
                    <input class="form-control" value="{{$res->passport}}" required="required" name="lkfgjd" type="text">
                </div>
                <!--col-->
            </div>
            <!--form-group-->

            <div class="form-group row">
                {{ Form::label('passport_expiretion', 'Passport expiretion', ['class' => 'col-md-2 from-control-label required']) }}

                <div class="col-md-10">
                    <input class="form-control" value="{{$res->passport_expiretion}}" required="required" name="lkfgjd" type="date">
                </div>
                <!--col-->
            </div>
            <!--form-group-->
        </div>
        <!--col-->
    </div>
    <!--row-->
    @endforeach
</div>
<!--card-body-->

@section('pagescript')
<script type="text/javascript">
    FTX.Utils.documentReady(function() {
        FTX.Reservations.edit.init("{{ config('locale.languages.' . app()->getLocale())[1] }}");
    });
</script>
@stop