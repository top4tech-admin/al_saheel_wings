@extends('backend.layouts.app')

@section('title', 'Tours'. ' | ' . 'Create')

@section('breadcrumb-links')
    @include('backend.tours.includes.breadcrumb-links')
@endsection

@section('content')
    {{ Form::open(['route' => 'admin.tours.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id' => 'create-permission', 'files' => true]) }}

    <div class="card">
        @include('backend.tours.form')
        @include('backend.components.footer-buttons', ['cancelRoute' => 'admin.tours.index'])
    </div><!--card-->
    {{ Form::close() }}
@endsection