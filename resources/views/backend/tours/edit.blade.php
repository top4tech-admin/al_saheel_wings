@extends('backend.layouts.app')

@section('title', 'Tours'. ' | ' . 'Edit')

@section('breadcrumb-links')
    @include('backend.tours.includes.breadcrumb-links')
@endsection

@section('content')
    {{ Form::model($tour, ['route' => ['admin.tours.update', $tour], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH', 'id' => 'edit-role', 'files' => true]) }}

    <div class="card">
        @include('backend.tours.form')
        @include('backend.components.footer-buttons', [ 'cancelRoute' => 'admin.tours.index', 'id' => $tour->id ])
    </div><!--card-->
    {{ Form::close() }}
@endsection