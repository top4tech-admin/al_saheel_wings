<div class="card-body">
    <div class="row">
        <div class="col-sm-5">
            <h4 class="card-title mb-0">
                Tours
                <small class="text-muted">{{ (isset($page)) ? 'Edit' : 'Create' }}</small>
            </h4>
        </div>
        <!--col-->
    </div>
    <!--row-->

    <hr>

    <div class="row mt-4 mb-4">
        <div class="col">
            <div class="form-group row">
                {{ Form::label('title', 'عنوان الرحلة', ['class' => 'col-md-2 from-control-label required']) }}

                <div class="col-md-10">
                    {{ Form::text('title', null, ['class' => 'form-control', 'required' => 'required']) }}
                </div>
                <!--col-->
            </div>
            <!--form-group-->

            <div class="form-group row">
                {{ Form::label('prie', 'تكلفة الرحلة', ['class' => 'col-md-2 from-control-label required']) }}

                <div class="col-md-10">
                    {{ Form::number('price', null, ['class' => 'form-control', 'required' => 'required']) }}
                </div>
                <!--col-->
            </div>
            <!--form-group-->

            <div class="form-group row">
                {{ Form::label('description', 'وصف الرحلة', ['class' => 'col-md-2 from-control-label required']) }}

                <div class="col-md-10">
                    {{ Form::textarea('description', null, ['class' => 'form-control']) }}
                </div>
                <!--col-->
            </div>
            <!--form-group-->


            <div class="form-group row">
                {{ Form::label('image', 'رابط الصورة الرئيسية', ['class' => 'col-md-2 from-control-label required']) }}

                <div class="col-md-10">
                    {{ Form::text('img', null, ['class' => 'form-control', 'required' => 'required']) }}
                </div>
                <!--col-->
            </div>
            <!--form-group-->

            <div class="form-group row">
                {{ Form::label('depature_time', 'تاريخ انطلاق الرحلة', ['class' => 'col-md-2 from-control-label required']) }}
                <div class="col-md-10">
                    <input id="depature_time" name="depature_time" value="{{isset($tour) ? $tour->depature_time : ''}}" class="input" style="width: 195px;" required/>
                </div>
                <!--col-->
            </div>
            <!--form-group-->

            <div class="form-group row">
                {{ Form::label('arrival_time', 'تاريخ العودة', ['class' => 'col-md-2 from-control-label required']) }}
                <div class="col-md-10">
                    <input id="arrival_time" name="arrival_time" value="{{isset($tour) ? $tour->arrival_time : ''}}" class="input" style="width: 195px;" required/>
                </div>
                <!--col-->
            </div>
            <!--form-group-->

        </div>
        <!--col-->
    </div>
    <!--row-->
</div>
<!--card-body-->

@section('pagescript')
<script type="text/javascript">
    FTX.Utils.documentReady(function() {
        FTX.Tours.edit.init("{{ config('locale.languages.' . app()->getLocale())[1] }}");
    });
    rome(depature_time);
    rome(arrival_time);
</script>
@stop