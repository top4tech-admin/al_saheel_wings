@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . 'Tours')

@section('breadcrumb-links')
@include('backend.tours.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    Tours <small class="text-muted">List</small>
                </h4>
            </div>
            <!--col-->
        </div>
        <!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table id="tours-table" class="table" data-ajax_url="{{ route("admin.tours.get") }}">
                        <thead>
                            <tr>
                                <th>title</th>
                                <th>price</th>
                                <th data-orderable="false">time</th>
                                <th>{{ trans('labels.backend.access.pages.table.createdat') }}</th>
                                <th>{{ trans('labels.general.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <!--col-->
        </div>
        <!--row-->

    </div>
    <!--card-body-->
</div>
<!--card-->
@endsection

@section('pagescript')
<script>
    FTX.Utils.documentReady(function() {
        FTX.Tours.list.init();
    });
</script>
@endsection