@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . 'flights')

@section('content')
<!--about-us start -->
<section id="home" class="about-us flights-cover">
    <div class="container">
        <div class="about-us-content">
            <div class="row">
                <div class="col-sm-12">
                    <div class="single-about-us">
                        <div class="about-us-txt flights-txt">
                            <h2>
                            @lang('custom.frontend.company_name_all')
                            </h2>
                            <!--/.about-btn-->
                        </div>
                        <!--/.about-us-txt-->
                    </div>
                    <!--/.single-about-us-->
                </div>
                <!--/.col-->
                <div class="col-sm-0">
                    <div class="single-about-us">

                    </div>
                    <!--/.single-about-us-->
                </div>
                <!--/.col-->
            </div>
            <!--/.row-->
        </div>
        <!--/.about-us-content-->
    </div>
    <!--/.container-->

</section>
<!--/.about-us-->
<!--about-us end -->

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2 mar email-box">
            <h1 class="text-center">@lang('custom.frontend.who_we')</h1>
            <hr/>
            <p class="text-center">
            @lang('custom.frontend.who_we_text')<br/><b>@lang('custom.frontend.university_services')</b> - <b>@lang('custom.frontend.import_export')</b> - <b>@lang('custom.frontend.shipping')</b>
                @lang('custom.frontend.established')
            </p>
        </div>

        <div class="col-md-6 mar">
            <h2>@lang('custom.frontend.payment')</h2>
            <ul>
                <li>1- @lang('custom.frontend.payment_1')</li>
                <li>2- @lang('custom.frontend.payment_2')
                <li>3- @lang('custom.frontend.payment_3')
            </ul>
        </div>

        <div class="col-md-6 mar">
            <div class="mapouter">
                <div class="gmap_canvas"><iframe width="600" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=syria%20tartus%20%D8%A7%D9%84%D8%B3%D9%84%D8%B7%D9%86%20%D9%83%D8%A7%D9%81%D9%8A%D9%87&t=&z=17&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://fmovies-online.net"></a><br>
                    <style>
                        .mapouter {
                            position: relative;
                            text-align: right;
                            height: 500px;
                            width: 600px;
                        }
                    </style><a href="https://www.embedgooglemap.net">map for website</a>
                    <style>
                        .gmap_canvas {
                            overflow: hidden;
                            background: none !important;
                            height: 500px;
                            width: 600px;
                        }
                    </style>
                </div>
            </div>
        </div>
    </div>
</div>
@include('frontend.includes.footer')

<script src="{{asset('js/frontend/jquery.js')}}"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->

<!--modernizr.min.js-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>


<!--bootstrap.min.js-->
<script src="{{asset('js/frontend/bootstrap.min.js')}}"></script>

<!-- bootsnav js -->
<script src="{{asset('js/frontend/bootsnav.js')}}"></script>

<!-- jquery.filterizr.min.js -->
<script src="{{asset('js/frontend/jquery.filterizr.min.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>

<!--jquery-ui.min.js-->
<script src="{{asset('js/frontend/jquery-ui.min.js')}}"></script>

<!-- counter js -->
<script src="{{asset('js/frontend/jquery.counterup.min.js')}}"></script>
<script src="{{asset('js/frontend/waypoints.min.js')}}"></script>

<!--owl.carousel.js-->
<script src="{{asset('js/frontend/owl.carousel.min.js')}}"></script>

<!-- jquery.sticky.js -->
<script src="{{asset('js/frontend/jquery.sticky.js')}}"></script>

<!--datepicker.js-->
<script src="{{asset('js/frontend/datepicker.js')}}"></script>

<!--Custom JS-->
<script src="{{asset('js/frontend/custom.js')}}"></script>

@endsection