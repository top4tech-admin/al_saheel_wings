@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . 'flights')

@section('content')
<!--about-us start -->
<section id="home" class="about-us flights-cover">
    <div class="container">
        <div class="about-us-content">
            <div class="row">
                <div class="col-sm-12">
                    <div class="single-about-us">
                        <div class="about-us-txt flights-txt">
                            <h2>
                            @lang('custom.frontend.company_name_all')
                            </h2>
                            <!--/.about-btn-->
                        </div>
                        <!--/.about-us-txt-->
                    </div>
                    <!--/.single-about-us-->
                </div>
                <!--/.col-->
                <div class="col-sm-0">
                    <div class="single-about-us">

                    </div>
                    <!--/.single-about-us-->
                </div>
                <!--/.col-->
            </div>
            <!--/.row-->
        </div>
        <!--/.about-us-content-->
    </div>
    <!--/.container-->

</section>
<!--/.about-us-->
<!--about-us end -->

<div class="container">
    <div class="row mar">
        <div class="alert alert-success h3">
        @lang('custom.frontend.reservation_registered')
        </div>
        <div class="alert alert-danger h3">
            <ul>
        <li>@lang('custom.frontend.pay') <b>{{$total_price}}</b> @lang('custom.frontend.pay_within')</li>
        <li>@lang('custom.frontend.pay_foreing')</li>
        </ul>
        </div>
        <div class="alert alert-warning h3">
            <h2>@lang('custom.frontend.payment')</h2>
            <ul>
                <li>1- @lang('custom.frontend.payment_1')</li>
                <li>2- @lang('custom.frontend.payment_2')
                <li>3- @lang('custom.frontend.payment_3')
            </ul>
        </div>
        <div class="text-center">
            <a href="https://wa.me/0954444724?text=مرحبا أجنحة الساحل .. لدي استفسار عن " target="_blank">@lang('custom.frontend.whatsApp')<i class="fa fa-whatsapp fa-4x"></i></a>
        </div>
        <div class="mar"></div>
    </div>
</div>

@include('frontend.includes.footer')

<script src="{{asset('js/frontend/jquery.js')}}"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->

<!--modernizr.min.js-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>


<!--bootstrap.min.js-->
<script src="{{asset('js/frontend/bootstrap.min.js')}}"></script>

<!-- bootsnav js -->
<script src="{{asset('js/frontend/bootsnav.js')}}"></script>

<!-- jquery.filterizr.min.js -->
<script src="{{asset('js/frontend/jquery.filterizr.min.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>

<!--jquery-ui.min.js-->
<script src="{{asset('js/frontend/jquery-ui.min.js')}}"></script>

<!-- counter js -->
<script src="{{asset('js/frontend/jquery.counterup.min.js')}}"></script>
<script src="{{asset('js/frontend/waypoints.min.js')}}"></script>

<!--owl.carousel.js-->
<script src="{{asset('js/frontend/owl.carousel.min.js')}}"></script>

<!-- jquery.sticky.js -->
<script src="{{asset('js/frontend/jquery.sticky.js')}}"></script>

<!--datepicker.js-->
<script src="{{asset('js/frontend/datepicker.js')}}"></script>

<!--Custom JS-->
<script src="{{asset('js/frontend/custom.js')}}"></script>

@endsection