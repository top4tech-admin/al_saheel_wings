@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . 'flights')

@section('content')
<!--about-us start -->
<section id="home" class="about-us flights-cover">
    <div class="container">
        <div class="about-us-content">
            <div class="row">
                <div class="col-sm-12">
                    <div class="single-about-us">
                        <div class="about-us-txt flights-txt">
                            <h2>
                            @lang('custom.frontend.company_name_all') 
                            </h2>
                            <!--/.about-btn-->
                        </div>
                        <!--/.about-us-txt-->
                    </div>
                    <!--/.single-about-us-->
                </div>
                <!--/.col-->
                <div class="col-sm-0">
                    <div class="single-about-us">

                    </div>
                    <!--/.single-about-us-->
                </div>
                <!--/.col-->
            </div>
            <!--/.row-->
        </div>
        <!--/.about-us-content-->
    </div>
    <!--/.container-->

</section>
<!--/.about-us-->
<!--about-us end -->

<div class="container pad pad-down">
    <form action="{{route('frontend.reservations.store')}}" method="post">
        @csrf
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <div class="form-seg">
                @lang('custom.frontend.flight_from') @langrtl
                {{$flight->fromAirport->name_ar}}
                @else
                {{$flight->fromAirport->name_en}}
                @endlangrtl
                @lang('custom.frontend.to')  
                @langrtl
                {{$flight->toAirport->name_ar}}
                @else
                {{$flight->toAirport->name_en}}
                @endlangrtl
                                                            
                    <br />
                    @lang('custom.frontend.date'): {{date('Y/m/d', strtotime($flight->depature_time))}}
                    <br />
                    @lang('custom.frontend.depature_time'): {{date('H:m', strtotime($flight->depature_time))}}
                    <br />
                    @lang('custom.frontend.adult_cost'): {{price($flight->price)}}
                </div>
            </div>
            <hr />
            <div class="col-xs-12 col-sm-12">
                <div class="form-seg">
                    @lang('custom.frontend.enter_your_details')
                </div>
            </div>
        </div>

        <div id="passenger_'+ i_value + '" class="row mar">
            <div class="col-xs-12 col-sm-3">
                <div class="def">
                @lang('custom.frontend.Traveler_data')
                </div>
            </div>
            <div class="col-xs-12 col-sm-9 border-right">
                <div class="row">
                    <div class="col-xs-12 col-sm-4">
                        <div class="form-seg">
                            <label for="name">@lang('custom.frontend.first_name')</label>
                            <input type="text" class="form-control" name="person_fname[]" required/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="form-seg">
                            <label for="person_sname">@lang('custom.frontend.father_name')</label>
                            <input type="text" class="form-control" name="person_sname[]" required/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="form-seg">
                            <label for="person_tname">@lang('custom.frontend.last_name')</label>
                            <input type="text" class="form-control" name="person_tname[]" required/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="form-seg">
                            <label for="mobile">@lang('custom.frontend.phone')</label>
                            <input type="text" class="form-control" name="mobile" required/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="form-seg">
                            <label for="mobile">@lang('custom.frontend.email')</label>
                            <input type="email" class="form-control" name="email" required/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="form-seg">
                            <label for="birthday">@lang('custom.frontend.birthday')</label>
                            <input type="date" class="form-control" name="birthday[]" required/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="form-seg">
                            <label for="nationality">@lang('custom.frontend.nationality')</label>
                            <select class="form-control" name="nationality[]" required>
                                @langrtl
                                @include('frontend.includes.countries_ar')
                                @else
                                @include('frontend.includes.countries')
                                @endlangrtl
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="form-seg">
                            <label for="passport">@lang('custom.frontend.passport_number')</label>
                            <input type="text" class="form-control" name="passport[]" required/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="form-seg">
                            <label for="passport_expiretion">@lang('custom.frontend.expire_passport')</label>
                            <input type="date" class="form-control" name="passport_expiretion[]" required/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="form-seg">
                            <label for="passport_country">@lang('custom.frontend.country_passpord')</label>
                            <select class="form-control" name="passport_country[]" required>
                                @langrtl
                                @include('frontend.includes.countries_ar')
                                @else
                                @include('frontend.includes.countries')
                                @endlangrtl
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <input type="radio" name="round_trip" hidden checked value="{{$round_trip}}"/>
        @if(isset($flights_return))
        <input type="number" value="{{$flights_return}}" name="flight_return_id" hidden />
        @endif

        @for($i = 1 ; $i < $passengers['adults']; $i++)
        <div id="passenger_'+ i_value + '" class="row mar">
            <div class="col-xs-12 col-sm-3">
                <div class="def">
                @lang('custom.frontend.country_passpord')
                </div>
            </div>
            <div class="col-xs-12 col-sm-9 border-right">
                <div class="row">
                    <div class="col-xs-12 col-sm-4">
                        <div class="form-seg">
                            <label for="name">@lang('custom.frontend.first_name')</label>
                            <input type="text" class="form-control" name="person_fname[]" required/>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-4">
                        <div class="form-seg">
                            <label for="person_sname">@lang('custom.frontend.father_name')</label>
                            <input type="text" class="form-control" name="person_sname[]" required/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="form-seg">
                            <label for="person_tname">@lang('custom.frontend.last_name')</label>
                            <input type="text" class="form-control" name="person_tname[]" required/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="form-seg">
                            <label for="mobile">@lang('custom.frontend.phone')</label>
                            <input type="text" class="form-control" name="mobile" required/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="form-seg">
                            <label for="birthday">@lang('custom.frontend.birthday')</label>
                            <input type="date" class="form-control" name="birthday[]" required/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="form-seg">
                            <label for="nationality">@lang('custom.frontend.nationality')</label>
                            <select class="form-control" name="nationality[]" required>
                                @langrtl
                                @include('frontend.includes.countries_ar')
                                @else
                                @include('frontend.includes.countries')
                                @endlangrtl
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="form-seg">
                            <label for="passport">@lang('custom.frontend.passport_number')</label>
                            <input type="text" class="form-control" name="passport[]" required/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="form-seg">
                            <label for="passport_expiretion">@lang('custom.frontend.expire_passport')</label>
                            <input type="date" class="form-control" name="passport_expiretion[]" required/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="form-seg">
                            <label for="passport_country">@lang('custom.frontend.country_passpord')</label>
                            <select class="form-control" name="passport_country[]" required>
                                @langrtl
                                @include('frontend.includes.countries_ar')
                                @else
                                @include('frontend.includes.countries')
                                @endlangrtl
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endfor

        @for($i = 0 ; $i < $passengers['children']; $i++)
        <div id="passenger_'+ i_value + '" class="row mar">
            <div class="col-xs-12 col-sm-3">
                <div class="def">
                @lang('custom.frontend.child_passenger')
                </div>
            </div>
            <div class="col-xs-12 col-sm-9 border-right">
                <div class="row">
                    <div class="col-xs-12 col-sm-4">
                        <div class="form-seg">
                            <label for="name">@lang('custom.frontend.first_name')</label>
                            <input type="text" class="form-control" name="person_fname[]" required/>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-4">
                        <div class="form-seg">
                            <label for="person_sname">@lang('custom.frontend.father_name')</label>
                            <input type="text" class="form-control" name="person_sname[]" required/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="form-seg">
                            <label for="person_tname">@lang('custom.frontend.last_name')</label>
                            <input type="text" class="form-control" name="person_tname[]" required/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="form-seg">
                            <label for="birthday">@lang('custom.frontend.birthday')</label>
                            <input type="date" class="form-control" name="birthday[]" required/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="form-seg">
                            <label for="nationality">@lang('custom.frontend.nationality')</label>
                            <select class="form-control" name="nationality[]" required>
                                @langrtl
                                @include('frontend.includes.countries_ar')
                                @else
                                @include('frontend.includes.countries')
                                @endlangrtl
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="form-seg">
                            <label for="passport">@lang('custom.frontend.passport_number')</label>
                            <input type="text" class="form-control" name="passport[]" required/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="form-seg">
                            <label for="passport_expiretion">@lang('custom.frontend.expire_passport')</label>
                            <input type="date" class="form-control" name="passport_expiretion[]" required/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="form-seg">
                            <label for="passport_country">@lang('custom.frontend.country_passpord')</label>
                            <select class="form-control" name="passport_country[]" required>
                                @langrtl
                                @include('frontend.includes.countries_ar')
                                @else
                                @include('frontend.includes.countries')
                                @endlangrtl
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endfor

        @for($i = 0 ; $i < $passengers['infants']; $i++)
        <div id="passenger_'+ i_value + '" class="row mar">
            <div class="col-xs-12 col-sm-3">
                <div class="def">
                @lang('custom.frontend.infant_passenger')
                </div>
            </div>
            <div class="col-xs-12 col-sm-9 border-right">
                <div class="row">
                    <div class="col-xs-12 col-sm-4">
                        <div class="form-seg">
                            <label for="name">@lang('custom.frontend.first_name')</label>
                            <input type="text" class="form-control" name="person_fname[]" required/>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-4">
                        <div class="form-seg">
                            <label for="person_sname">@lang('custom.frontend.father_name')</label>
                            <input type="text" class="form-control" name="person_sname[]" required/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="form-seg">
                            <label for="person_tname">@lang('custom.frontend.last_name')</label>
                            <input type="text" class="form-control" name="person_tname[]" required/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="form-seg">
                            <label for="birthday">@lang('custom.frontend.birthday')</label>
                            <input type="date" class="form-control" name="birthday[]" required/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="form-seg">
                            <label for="nationality">@lang('custom.frontend.nationality')</label>
                            <select class="form-control" name="nationality[]" required>
                                @langrtl
                                @include('frontend.includes.countries_ar')
                                @else
                                @include('frontend.includes.countries')
                                @endlangrtl
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="form-seg">
                            <label for="passport">@lang('custom.frontend.passport_number')</label>
                            <input type="text" class="form-control" name="passport[]" required/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="form-seg">
                            <label for="passport_expiretion">@lang('custom.frontend.expire_passport')</label>
                            <input type="date" class="form-control" name="passport_expiretion[]" required/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="form-seg">
                            <label for="passport_country">@lang('custom.frontend.country_passpord')</label>
                            <select class="form-control" name="passport_country[]" required>
                                @langrtl
                                @include('frontend.includes.countries_ar')
                                @else
                                @include('frontend.includes.countries')
                                @endlangrtl
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endfor

        <!-- <div id="new_passenger"></div> -->

        <div class="row mar">
            <!-- <div class="col-xs-6 col-sm-2">
                <div class="btn-group">
                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        أضف مسافر جديد <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        <li><a id="addPassenger">بالغ</a></li>
                        <li><a id="addYoungPassenger">طفل دون 14 سنة</a></li>
                        <li><a id="addBabyPassenger">رضيع دون 2 سنة</a></li>
                    </ul>
                </div>
            </div> -->
            <div class="alert alert-warning">
                <div class="h3">@lang('custom.frontend.invoice_details')</div>
                <table class="table table-bordered">
                <thead>
                    <tr>
                        <th scope="col">@lang('custom.frontend.the_passenger')</th>
                        <th scope="col">@lang('custom.frontend.price')</th>
                        <th scope="col">@lang('custom.frontend.count')</th>
                        <th scope="col">@lang('custom.frontend.total_price')</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">@lang('custom.frontend.adult_passenger')</th>
                        <td>{{price($flight->price)}}</td>
                        <td>{{$passengers['adults']}}</td>
                        <td>{{price(intval($flight->price) * floatval($passengers['adults']))}}</td>
                    </tr>
                    @if(floatval($passengers['children']) > 0)
                    <tr>
                        <th scope="row">@lang('custom.frontend.child_passenger')</th>
                        <td>{{price($flight->yunge_price)}}</td>
                        <td>{{$passengers['children']}}</td>
                        <td>{{price($flight->yunge_price * floatval($passengers['children']))}}</td>
                    </tr>
                    @endif
                    @if(floatval($passengers['infants']) > 0)
                    <tr> 
                        <th scope="row">@lang('custom.frontend.infant_passenger')</th>
                        <td>{{price($flight->baby_price)}}</td>
                        <td>{{$passengers['infants']}}</td>
                        <td>{{price($flight->baby_price * floatval($passengers['infants']))}}</td>
                    </tr>
                    @endif
                    <tr>
                        <td colspan="4">@lang('custom.frontend.total_sum_price'): {{price(intval($flight->price) * floatval($passengers['adults']) + $flight->yunge_price * floatval($passengers['children']) + $flight->baby_price * floatval($passengers['infants']))}}</td>
                    </tr>
                </tbody>
            </table>
            </div>
            <div class="col-xs-6 col-sm-2">
            <input type="text" class="hidden" name="flight_id" value="{{$flight->id}}"/>
                <input type="number" class="hidden" name="total_price" value="{{intval(price($flight->price)) * floatval($passengers['adults']) + $flight->yunge_price * floatval($passengers['children']) + $flight->baby_price * floatval($passengers['infants'])}}"/>
                <input type="submit" value="{{__('custom.frontend.confirm_reservation')}}" class="btn btn-success"/>
            </div>
        </div>
    </form>
</div>

@include('frontend.includes.footer')

<script src="{{asset('js/frontend/jquery.js')}}"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->

<!--modernizr.min.js-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>


<!--bootstrap.min.js-->
<script src="{{asset('js/frontend/bootstrap.min.js')}}"></script>

<!-- bootsnav js -->
<script src="{{asset('js/frontend/bootsnav.js')}}"></script>

<!-- jquery.filterizr.min.js -->
<script src="{{asset('js/frontend/jquery.filterizr.min.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>

<!--jquery-ui.min.js-->
<script src="{{asset('js/frontend/jquery-ui.min.js')}}"></script>

<!-- counter js -->
<script src="{{asset('js/frontend/jquery.counterup.min.js')}}"></script>
<script src="{{asset('js/frontend/waypoints.min.js')}}"></script>

<!--owl.carousel.js-->
<script src="{{asset('js/frontend/owl.carousel.min.js')}}"></script>

<!-- jquery.sticky.js -->
<script src="{{asset('js/frontend/jquery.sticky.js')}}"></script>

<!--datepicker.js-->
<script src="{{asset('js/frontend/datepicker.js')}}"></script>

<!--Custom JS-->
<script src="{{asset('js/frontend/custom.js')}}"></script>
@endsection