@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . 'flights')

@section('content')
<!--about-us start -->
<section id="home" class="about-us flights-cover">
    <div class="container">
        <div class="about-us-content">
            <div class="row">
                <div class="col-sm-12">
                    <div class="single-about-us">
                        <div class="about-us-txt flights-txt">
                            <h2>
                                @lang('custom.frontend.company_name_all')
                            </h2>
                            <!--/.about-btn-->
                        </div>
                        <!--/.about-us-txt-->
                    </div>
                    <!--/.single-about-us-->
                </div>
                <!--/.col-->
                <div class="col-sm-0">
                    <div class="single-about-us">

                    </div>
                    <!--/.single-about-us-->
                </div>
                <!--/.col-->
            </div>
            <!--/.row-->
        </div>
        <!--/.about-us-content-->
    </div>
    <!--/.container-->

</section>
<!--/.about-us-->
<!--about-us end -->


<div class="container">
    <div class="row">
        <div class="col-sm-8 pad">
            @if(is_null($flights_go) || !isset($flights_go[0]) || empty($flights_go))
            <div class="text-center">
                <div class="row box" id="go-not-found">
                    <p>@lang('custom.frontend.flights_not_found')
                </div>
            </div>
            @else
            <p class="h4"> @lang('custom.frontend.flight_from') <b>
                @langrtl
                    {{$flights_go[0]->FromAirport->name_ar}}
                    @else
                    {{$flights_go[0]->FromAirport->name_en}}
                @endlangrtl
                </b> @lang('custom.frontend.to') <b>
                @langrtl
                    {{$flights_go[0]->toAirport->name_ar}}
                    @else
                    {{$flights_go[0]->toAirport->name_en}}
                @endlangrtl</b>:
                <!-- ss -->
            <div class="row">

                <div class="MultiCarousel" data-items="3,3,5,5" data-slide="1" id="MultiCarousel" data-interval="1000">
                    <div class="MultiCarousel-inner">
                        <?php
                        $begin = new DateTime($from_go_date);
                        $end = new DateTime($to_go_date);

                        $interval = DateInterval::createFromDateString('1 day');
                        $period = new DatePeriod($begin, $interval, $end);
                        $i = 0;
                        $active = "";

                        foreach ($period as $dt) {

                            if (
                                !empty($trip_go)
                                && $dt->format("d-m") == date('m-d', strtotime($depature_time)) && date("d-m", strtotime($trip_go->depature_time)) == date('m-d', strtotime($depature_time))
                            ) {
                                $active = "f-active-go hand";
                            } else {
                                $active = "hand";
                            }
                            if (isset($flights_go[$i]) && $dt->format("Y-m-d") == date('Y-m-d', strtotime($flights_go[$i]->depature_time))) {
                                echo "<div class=\"item go " . $active . "\" onclick=\"showGoF(this," . $flights_go[$i]->id . ")\">
                                        <div class=\" pad15\">
                                            <p class=\"h6\">" . $dt->format("M d\n") . "</p>
                                            <p>" . $dt->format("l\n") . "</p>
                                            <p><b> " . price($flights_go[$i]->price) . "</b></p>
                                        </div>
                                    </div>";
                                $i++;
                            } else {
                                echo "<div class=\"item \">
                                        <div class=\" pad15\">
                                            <p class=\"h6\">" . $dt->format("M d\n") . "</p>
                                            <p>" . $dt->format("l\n") . "</p>
                                            <p>" . __('custom.frontend.not_found') . " </p>
                                        </div>
                                    </div>";
                            }
                        }
                        ?>
                    </div>
                    <button class="btn btn-primary leftLst">
                        <</button>
                            <button class="btn btn-primary rightLst">></button>
                </div>
            </div>
            @if(!empty($trip_go))
            <div class="go-box row box" id="flight-{{$trip_go->id}}">
                <div class="flight-box">
                    <div class="row">
                        <div class="box-city col-xs-4">
                            <div class="box-time">{{date('H:i',strtotime($trip_go->depature_time))}}</div>
                            <div class="box-air">
                                @langrtl
                                {{$trip_go->FromAirport->name_ar}}
                                @else
                                {{$trip_go->FromAirport->name_en}}
                                @endlangrtl
                            </div>
                        </div>
                        <div class="col-xs-4 align-items-center line"><i class="fa fa-plane plane-icon"></i></div>
                        <div class="box-city col-xs-4">
                            <div class="box-time">{{date('H:i',strtotime($trip_go->arrival_time))}}</div>
                            <div class="box-air">
                                @langrtl
                                {{$trip_go->toAirport->name_ar}}
                                @else
                                {{$trip_go->toAirport->name_en}}
                                @endlangrtl
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="book-flight">
                            <div class="col-md-4 text-left">
                                <span><b>{{price($trip_go->price)}}</b></span>
                            </div>
                            <div class="col-md-5 text-left">
                                <span>{{date("Y F  d",strtotime($trip_go->depature_time))}}</span>
                            </div>
                            <div class="col-md-3 pull-right">
                                <button onclick="goFlight(this)" id="{{$trip_go->id}}"  class="choice about-view travel-btn">@lang('custom.frontend.choose')</button>
                            </div>
                            <!--/.travel-btn-->
                        </div>
                    </div>
                </div>
            </div>
            @else
            <div class="row box" id="go-not-found">
                <p>@lang('custom.frontend.today_no_flights')
            </div>
            @endif

            @foreach($flights_go as $flight_go)
            <div class="go-box row box hidden" id="flight-{{$flight_go->id}}">
                <div class="flight-box">
                    <div class="row">
                        <div class="box-city col-xs-4">
                            <div class="box-time">{{date('H:i',strtotime($flight_go->depature_time))}}</div>
                            <div class="box-air">
                                @langrtl
                                {{$flight_go->FromAirport->name_ar}}
                                @else
                                {{$flight_go->FromAirport->name_en}}
                                @endlangrtl
                            </div>
                        </div>
                        <div class="col-xs-4 align-items-center line"><i class="fa fa-plane plane-icon"></i></div>
                        <div class="box-city col-xs-4">
                            <div class="box-time">{{date('H:i',strtotime($flight_go->arrival_time))}}</div>
                            <div class="box-air">
                                @langrtl
                                {{$flight_go->toAirport->name_ar}}
                                @else
                                {{$flight_go->toAirport->name_en}}
                                @endlangrtl
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="book-flight">
                            <div class="col-md-4 text-left">
                                <span><b>{{price($flight_go->price)}}</b></span>
                            </div>
                            <div class="col-md-5 text-left">
                                <span>{{date("Y F  d",strtotime($flight_go->depature_time))}}</span>
                            </div>
                            <div class="col-md-3 pull-right">
                                <button onclick="goFlight(this)" id="{{$flight_go->id}}" class="about-view travel-btn">@lang('custom.frontend.choose')</button>
                            </div>
                            <!--/.travel-btn-->
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            @endif

            @if(isset($flights_return[0]))
            @if(is_null($flights_return) || !isset($flights_go) || !isset($flights_return[0]) || empty($flights_return))
            <div class="text-center">
                <div class="row box" id="return-not-found">
                    <p>@lang('custom.frontend.return_flights_not_found')
                </div>
            </div>
            @else
            <p class="h4 top-pad"> @lang('custom.frontend.flight_return_from') <b>
            @langrtl
                    {{$flights_return[0]->FromAirport->name_ar}}
                    @else
                    {{$flights_return[0]->FromAirport->name_en}}
                    @endlangrtl
                </b> @lang('custom.frontend.to') <b>
                @langrtl
                    {{$flights_return[0]->toAirport->name_ar}}
                    @else
                    {{$flights_return[0]->toAirport->name_en}}
                    @endlangrtl
                     </b>:
                <!-- ss -->

            <div class="row">

                <div class="MultiCarousel" data-items="3,3,5,5" data-slide="1" id="MultiCarousel" data-interval="1000">
                    <div class="MultiCarousel-inner">
                        <?php
                        $begin = new DateTime($from_return_date);
                        $end = new DateTime($to_return_date);

                        $interval = DateInterval::createFromDateString('1 day');
                        $period = new DatePeriod($begin, $interval, $end);
                        $i = 0;
                        $active = "";

                        foreach ($period as $dt) {

                            if (
                                !empty($trip_return)
                                && $dt->format("d-m") == date('m-d', strtotime($arrival_time)) && date("d-m", strtotime($trip_return->depature_time)) == date('m-d', strtotime($arrival_time))
                            ) {
                                $active = "f-active-return hand";
                            } else {
                                $active = "hand";
                            }

                            if (isset($flights_return[$i]) && $dt->format("Y-m-d") == date('Y-m-d', strtotime($flights_return[$i]->depature_time))) {
                                echo "<div class=\"item return " . $active . "\" onclick=\"showReturnF(this," . $flights_return[$i]->id . ")\">
                                        <div class=\" pad15\">
                                            <p class=\"h6\">" . $dt->format("M d\n") . "</p>
                                            <p>" . $dt->format("l\n") . "</p>
                                            <p><b> " . price($flights_return[$i]->price) . "</b></p>
                                        </div>
                                    </div>";
                                $i++;
                            } else {
                                echo "<div class=\"item \">
                                        <div class=\" pad15\">
                                            <p class=\"h6\">" . $dt->format("M d\n") . "</p>
                                            <p>" . $dt->format("l\n") . "</p>
                                            <p>" . __('custom.frontend.not_found') . "</p>
                                        </div>
                                    </div>";
                            }
                        }
                        ?>
                    </div>
                    <button class="btn btn-primary leftLst">
                        <</button>
                            <button class="btn btn-primary rightLst">></button>
                </div>
            </div>

            @if(!empty($trip_return))
            <div class="return-box row box">
                <div class="flight-box">
                    <div class="row">
                        <div class="box-city col-xs-4">
                            <div class="box-time">{{date('H:i',strtotime($trip_return->depature_time))}}</div>
                            <div class="box-air">
                                @langrtl
                                {{$trip_return->FromAirport->name_ar}}
                                @else
                                {{$trip_return->FromAirport->name_en}}
                                @endlangrtl
                            </div>
                        </div>
                        <div class="col-xs-4 align-items-center line"><i class="fa fa-plane plane-icon"></i></div>
                        <div class="box-city col-xs-4">
                            <div class="box-time">{{date('H:i',strtotime($trip_return->arrival_time))}}</div>
                            <div class="box-air">
                                @langrtl
                                {{$trip_return->toAirport->name_ar}}
                                @else
                                {{$trip_return->toAirport->name_en}}
                                @endlangrtl
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="book-flight">
                            <div class="col-md-4 text-left">
                                <span><b>{{price($trip_return->price)}}</b></span>
                            </div>
                            <div class="col-md-5 text-left">
                                <span>{{date("Y F  d",strtotime($trip_return->depature_time))}}</span>
                            </div>
                            <div class="col-md-3 pull-right">
                                <button onclick="returnFlight(this)" id="{{$trip_return->id}}" class="about-view travel-btn">@lang('custom.frontend.choose')</button>
                            </div>
                            <!--/.travel-btn-->
                        </div>
                    </div>
                </div>
            </div>

            @else
            <div class="row box" id="return-not-found">
                <p>@lang('custom.frontend.today_no_flights')
            </div>
            @endif

            @foreach($flights_return as $flight_return)
            <div class="return-box row box hidden" id="flight-{{$flight_return->id}}">
                <div class="flight-box">
                    <div class="row">
                        <div class="box-city col-xs-4">
                            <div class="box-time">{{date('H:i',strtotime($flight_return->depature_time))}}</div>
                            <div class="box-air">
                                @langrtl
                                {{$flight_return->FromAirport->name_ar}}
                                @else
                                {{$flight_return->FromAirport->name_en}}
                                @endlangrtl
                            </div>
                        </div>
                        <div class="col-xs-4 align-items-center line"><i class="fa fa-plane plane-icon"></i></div>
                        <div class="box-city col-xs-4">
                            <div class="box-time">{{date('H:i',strtotime($flight_return->arrival_time))}}</div>
                            <div class="box-air">
                                @langrtl
                                {{$flight_return->toAirport->name_ar}}
                                @else
                                {{$flight_return->toAirport->name_en}}
                                @endlangrtl
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="book-flight">
                            <div class="col-md-4 text-left">
                                <b><span>{{price($flight_return->price)}}</span></b>
                            </div>
                            <div class="col-md-5 text-left">
                                <span>{{date("Y F  d",strtotime($flight_return->depature_time))}}</span>
                            </div>
                            <div class="col-md-3 pull-right">
                                <button onclick="returnFlight(this)" id="{{$flight_return->id}}" class="about-view travel-btn">@lang('custom.frontend.choose')</button>
                            </div>
                            <!--/.travel-btn-->
                        </div>
                    </div>
                </div>
            </div>
            @endforeach


            @endif
            @else
            <div class="text-center">
                <div class="row box" id="return-not-found">
                    <p>@lang('custom.frontend.return_flights_not_found')
                </div>
            </div>
            @endif
            <div class="row hidden" id="error_box">
                <div class="alert alert-danger">
                    <p id="error_msg"></p>
                </div>
            </div>

            <div class="col-md-3">
                <form action="{{route('frontend.confirm-steps')}}" method="get">
                    @csrf
                    <input type="radio" name="round_trip" hidden checked value="{{$round_trip}}" />
                    @if(isset($flights_return[0]))
                    <input type="number" id="return_id" name="return_flight_id" hidden required />
                    @endif
                    <input type="number" id="go_id" name="go_flight_id" hidden required />
                    <input type="adults" value="{{$passengers['adults']}}" name="adults" hidden />
                    <input type="children" value="{{$passengers['children']}}" name="children" hidden />
                    <input type="infants" value="{{$passengers['infants']}}" name="infants" hidden />
                    <input type="submit" id="btn-continue" value="{{__('custom.frontend.continue_to_booking')}}" class="about-view travel-btn"></input>
                </form>
            </div>
            <!--/.travel-btn-->
        </div>
        <div class="col-sm-4 ba_img">

            <form action="{{route('frontend.flights-results')}}" method="get">
                @csrf
                <div class="trip-circle top-pad">
                    <div class="single-trip-circle" id="ret_time_radio">
                        <input type="radio" id="radio01" name="round_trip" value="round_trip" checked />
                        <label for="radio01">
                            <span class="round-boarder">
                                <span class="round-boarder1"></span>
                            </span>@lang('custom.frontend.round_trip')
                        </label>
                    </div>
                    <!--/.single-trip-circle-->
                    <div class="single-trip-circle" id="dep_time_radio">
                        <input type="radio" id="radio02" value="go" name="round_trip" />
                        <label for="radio02">
                            <span class="round-boarder">
                                <span class="round-boarder1"></span>
                            </span>@lang('custom.frontend.go')
                        </label>
                    </div>
                    <!--/.single-trip-circle-->
                </div>
                <!--/.trip-circle-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="single-tab-select-box">

                            <h2>@lang('custom.frontend.from')</h2>

                            <div class="travel-select-icon">
                                <select class="form-control" name="from_airport" required>

                                    <option value="" selected disabled>@lang('custom.frontend.choice_airport')</option><!-- /.option-->

                                    @foreach($airports as $airport)
                                    @langrtl
                                    <option value="{{$airport->id}}">{{$airport->name_ar}}</option><!-- /.option-->
                                    @else
                                    <option value="{{$airport->id}}">{{$airport->name_en}}</option><!-- /.option-->
                                    @endlangrtl
                                    @endforeach

                                </select><!-- /.select-->
                            </div><!-- /.travel-select-icon -->
                        </div>
                        <!--/.single-tab-select-box-->
                    </div>
                    <!--/.col-->

                    <div class="col-sm-12">
                        <div class="single-tab-select-box">

                            <h2>@lang('custom.frontend.to')</h2>

                            <div class="travel-select-icon">
                                <select class="form-control" name="to_airport" required>

                                    <option value="" selected disabled>@lang('custom.frontend.choice_airport')</option><!-- /.option-->

                                    @foreach($airports as $airport)
                                    @langrtl
                                    <option value="{{$airport->id}}">{{$airport->name_ar}}</option><!-- /.option-->
                                    @else
                                    <option value="{{$airport->id}}">{{$airport->name_en}}</option><!-- /.option-->
                                    @endlangrtl
                                    @endforeach

                                </select><!-- /.select-->
                            </div><!-- /.travel-select-icon -->

                        </div>
                        <!--/.single-tab-select-box-->
                    </div>
                    <!--/.col-->
                    <div class="col-sm-12">
                        <div class="single-tab-select-box">

                            <h2>@lang('custom.frontend.grade')</h2>
                            <div class="travel-select-icon">
                                <select class="form-control ">

                                    <option value="" selected disabled>@lang('custom.frontend.choice_grade')</option><!-- /.option-->

                                    <option value="Business">@lang('custom.frontend.business')</option><!-- /.option-->

                                    <option value="economy">@lang('custom.frontend.economy')</option><!-- /.option-->

                                </select><!-- /.select-->
                            </div><!-- /.travel-select-icon -->
                        </div>
                        <!--/.single-tab-select-box-->
                    </div>
                    <!--/.col-->

                </div>
                <!--/.row-->

                <div class="row">
                    <div class="col-sm-12">
                        <div class="single-tab-select-box">
                            <h2>@lang('custom.frontend.depature')</h2>
                            <div class="travel-check-icon">
                                <!-- <form action="#"> -->
                                <input type="text" name="depature_time" autocomplete="off" class="form-control" data-toggle="datepicker" required>
                                <!-- </form> -->
                            </div><!-- /.travel-check-icon -->
                        </div>
                        <!--/.single-tab-select-box-->
                    </div>
                    <!--/.col-->

                    <div class="col-sm-12" id="dep_time">
                        <div class="single-tab-select-box">
                            <h2>@lang('custom.frontend.arrival')</h2>
                            <div class="travel-check-icon">
                                <!-- <form action="#"> -->
                                <input type="text" name="arrival_time" autocomplete="off" class="form-control" data-toggle="datepicker" required>
                                <!-- </form> -->
                            </div><!-- /.travel-check-icon -->
                        </div>
                        <!--/.single-tab-select-box-->
                    </div>
                    <!--/.col-->

                    <div class="col-sm-12">
                        <div class="single-tab-select-box">

                            <h2>@lang('custom.frontend.adults')</h2>

                            <div class="travel-select-icon">
                                <select class="form-control" name="adults">

                                    @for($i = 1; $i < 10 ;$i++) <option value="{{$i}}">{{$i}}</option><!-- /.option-->
                                        @endfor

                                </select><!-- /.select-->
                            </div><!-- /.travel-select-icon -->
                        </div>
                        <!--/.single-tab-select-box-->
                    </div>
                    <!--/.col-->

                    <div class="col-sm-12">
                        <div class="single-tab-select-box">

                            <h2>@lang('custom.frontend.children') <span class="small">(@lang('custom.frontend.down_12'))</span></h2>

                            <div class="travel-select-icon">
                                <select class="form-control" name="children">

                                    @for($i = 0; $i < 9 ;$i++) <option value="{{$i}}">{{$i}}</option><!-- /.option-->
                                        @endfor

                                </select><!-- /.select-->
                            </div><!-- /.travel-select-icon -->
                        </div>
                        <!--/.single-tab-select-box-->
                    </div>
                    <!--/.col-->

                    <div class="col-sm-12">
                        <div class="single-tab-select-box">

                            <h2>@lang('custom.frontend.infants') <span class="small">(@lang('custom.frontend.down_2'))</span></h2>

                            <div class="travel-select-icon">
                                <select class="form-control" name="infants">

                                    @for($i = 0; $i < 3 ;$i++) <option value="{{$i}}">{{$i}}</option><!-- /.option-->
                                        @endfor

                                </select><!-- /.select-->
                            </div><!-- /.travel-select-icon -->
                        </div>
                        <!--/.single-tab-select-box-->
                    </div>
                    <!--/.col-->

                    <div class="col-sm-12">
                        <div class="about-btn">
                            <input type="submit" value="{{__('custom.frontend.search')}}" class="about-view flight-btn"></input>
                            <!--/.travel-btn-->
                        </div>
                        <!--/.about-btn-->
                    </div>
                    <!--/.col-->

                </div>
                <!--/.row-->
            </form>

        </div>
    </div>
</div>


@include('frontend.includes.footer')


<script src="{{asset('js/frontend/jquery.js')}}"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->

<!--modernizr.min.js-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>


<!--bootstrap.min.js-->
<script src="{{asset('js/frontend/bootstrap.min.js')}}"></script>

<!-- bootsnav js -->
<script src="{{asset('js/frontend/bootsnav.js')}}"></script>

<!-- jquery.filterizr.min.js -->
<script src="{{asset('js/frontend/jquery.filterizr.min.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>

<!--jquery-ui.min.js-->
<script src="{{asset('js/frontend/jquery-ui.min.js')}}"></script>

<!-- counter js -->
<script src="{{asset('js/frontend/jquery.counterup.min.js')}}"></script>
<script src="{{asset('js/frontend/waypoints.min.js')}}"></script>

<!--owl.carousel.js-->
<script src="{{asset('js/frontend/owl.carousel.min.js')}}"></script>

<!-- jquery.sticky.js -->
<script src="{{asset('js/frontend/jquery.sticky.js')}}"></script>

<!--datepicker.js-->
<script src="{{asset('js/frontend/datepicker.js')}}"></script>

<!--Custom JS-->
<script src="{{asset('js/frontend/custom.js')}}"></script>

@endsection