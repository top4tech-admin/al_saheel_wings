
<!-- footer-copyright start -->
<footer class="footer-copyright">
	<div class="container">
		<div class="footer-content">
			<div class="row">

				<div class="col-sm-3">
					<div class="single-footer-item">
						<div class="footer-logo">
							<a href="index.html">
							@langrtl
							<span>@lang('custom.frontend.company_name2')</span> @lang('custom.frontend.company_name1')
							@else
							<span>@lang('custom.frontend.company_name1')</span> @lang('custom.frontend.company_name2')
							@endlangrtl
							</a>
							<p>
							@lang('custom.frontend.general_Director')
							</p>
							<p>
							@lang('custom.frontend.general_Director_name')
							</p>
						</div>
					</div>
					<!--/.single-footer-item-->
				</div>
				<!--/.col-->

				<div class="col-sm-3">
					<div class="single-footer-item">
						<h2>@lang('custom.frontend.services')</h2>
						<div class="single-footer-txt">
							<p><a>@lang('custom.frontend.Book_flights')</a></p>
							<p><a>@lang('custom.frontend.booking_hotel')</a></p>
							<p><a>@lang('custom.frontend.visa')</a></p>
							<p><a>@lang('custom.frontend.organizing_tours')</a></p>
							<p><a>@lang('custom.frontend.honeymoon')</a></p>
						</div>
						<!--/.single-footer-txt-->
					</div>
					<!--/.single-footer-item-->

				</div>
				<!--/.col-->

				<div class="col-sm-3">
					<div class="single-footer-item">
						<h2>@lang('custom.frontend.featured_distination')</h2>
						<div class="single-footer-txt">
							<p><a>@lang('custom.frontend.baghdad')</a></p>
							<p><a>@lang('custom.frontend.yerevan')</a></p>
							<p><a>@lang('custom.frontend.erbil')</a></p>
							<p><a>@lang('custom.frontend.tehran')</a></p>
							<p><a>@lang('custom.frontend.moscow')</a></p>
							<p><a>@lang('custom.frontend.sharjah')</a></p>
						</div>
						<!--/.single-footer-txt-->
					</div>
					<!--/.single-footer-item-->
				</div>
				<!--/.col-->

				<div class="col-sm-3">
					<div class="single-footer-item text-center">
						<h2 class="text-left">@lang('custom.frontend.contact_us')</h2>
						<div class="single-footer-txt text-left">
							<p><i class="fa fa-phone"></i> 963-433003</p>
							<p><i class="fa fa-phone"></i> 963-043315550</p>
							<p><i class="fa fa-mobile"></i> 963-954444724</p>
							<p class="foot-email"><a href="#">info@tnest.com</a></p>
							<p>@lang('custom.frontend.country_city')</p>
							<p>@lang('custom.frontend.address')</p>
						</div>
						<!--/.single-footer-txt-->
					</div>
					<!--/.single-footer-item-->
				</div>
				<!--/.col-->

			</div>
			<!--/.row-->

		</div>
		<!--/.footer-content-->
		<hr>
		<div class="foot-icons ">
			<ul class="footer-social-links list-inline list-unstyled">
				<li><a href="https://www.facebook.com/%D8%A3%D8%AC%D9%86%D8%AD%D8%A9-%D8%A7%D9%84%D8%B3%D8%A7%D8%AD%D9%84-%D9%84%D9%84%D8%B3%D9%8A%D8%A7%D8%AD%D8%A9-%D9%88%D8%A7%D9%84%D8%B3%D9%81%D8%B1-2632971770063190/" target="_blank" class="foot-icon-bg-1"><i class="fa fa-facebook fa-2x"></i></a></li>
				<li><a href="https://wa.me/09630954444724?text=مرحبا أجنحة الساحل .. لدي استفسار عن " target="_blank" class="foot-icon-bg-2"><i class="fa fa-whatsapp fa-2x"></i></a></li>
				<li><a href="https://t.me/alsahel_wings_syria" target="_blank" class="foot-icon-bg-3"><i class="fa fa-telegram fa-2x"></i></a></li>
			</ul>
			<p>&copy; 2021 - {{date('Y')}} <a href="{{url('/')}}">Al-Saheel Wings</a></p>

		</div>
		<!--/.foot-icons-->
		<div id="scroll-Top">
			<i class="fa fa-angle-double-up return-to-top" id="scroll-top" data-toggle="tooltip" data-placement="top" title="" data-original-title="Back to Top" aria-hidden="true"></i>
		</div>
		<!--/.scroll-Top-->
	</div><!-- /.container-->

</footer><!-- /.footer-copyright-->
<!-- footer-copyright end -->