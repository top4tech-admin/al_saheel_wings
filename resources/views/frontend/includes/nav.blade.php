<!-- main-menu Start -->
<header class="top-area">
	<div class="header-area">
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="logo">
						<a href="{{route('frontend.index')}}">
							@langrtl
							<span>@lang('custom.frontend.company_name2')</span> @lang('custom.frontend.company_name1')
							@else
							<span>@lang('custom.frontend.company_name1')</span> @lang('custom.frontend.company_name2')
							@endlangrtl
						</a>
					</div><!-- /.logo-->
				</div><!-- /.col-->
				<div class="col-sm-9">
					<div class="main-menu">

						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<i class="fa fa-bars"></i>
							</button><!-- / button-->
						</div><!-- /.navbar-header-->
						<div class="collapse navbar-collapse">
							<ul class="nav navbar-nav navbar-right">
								<!-- <li class="smooth-menu"><a href="#home">الرئيسية</a></li> -->

								<li class="{{active_class(request()->path() == '/')}}"><a href="{{url('/')}}">@lang('custom.frontend.home')</a></li>
								<li class="{{active_class(request()->path() == 'about-us')}}"><a href="{{route('frontend.about-us')}}">@lang('custom.frontend.contact_us')</a></li>
								<!--hidden-->
								<li role="presentation" class="dropdown hidden">
									<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
										@lang('custom.frontend.currencies') <span class="caret"></span>
									</a>
									<ul class="dropdown-menu">
										@if(!empty($currencies))
										<li class="pad-5 {{active_currency('USD')}}"><a class="pad-5" href="{{url('/currencie/1/USD')}}">دولار USD</a></li>
										@foreach($currencies as $currency)
										<li class="pad-5 {{active_currency($currency->symbol)}}"><a class="pad-5" href="{{url('/currencie/'.$currency->value.'/'.$currency->symbol)}}">{{$currency->name}}</a></li>
										@endforeach
									</ul>
									@endif
								</li>
								<!--hidden-->
								<li class="{{active_class(request()->path() == 'check-points')}}"><a href="{{route('frontend.mypoints')}}">@lang('custom.frontend.my_points')</a></li>
								<li class="">
									<b>@include('includes.partials.lang')</b>
								</li>

								<li>
									<button class="book-btn">
										@guest
										<a href="{{route('frontend.auth.login')}}">@lang('navs.frontend.login')
										</a>
										@else
										<a href="{{ route('admin.dashboard') }}" class="dropdown-item">@lang('navs.frontend.user.administration')</a>
										@endguest
									</button>
								</li>
								<!--/.project-btn-->
							</ul>
						</div><!-- /.navbar-collapse -->
					</div><!-- /.main-menu-->
				</div><!-- /.col-->
			</div><!-- /.row -->
			<div class="home-border"></div><!-- /.home-border-->
		</div><!-- /.container-->
	</div><!-- /.header-area -->

</header><!-- /.top-area-->
<!-- main-menu End -->