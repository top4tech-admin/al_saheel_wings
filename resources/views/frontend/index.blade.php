@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')

<!--[if lte IE 9]>
	<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade
		your browser</a> to improve your experience and security.</p>
<![endif]-->


<!--about-us start -->
<section id="home" class="about-us">
	<div class="container">
		<div class="about-us-content">
			<div class="row">
				<div class="col-sm-12">
					<div class="single-about-us">
						<div class="about-us-txt">
							<h2>
							@lang('custom.frontend.company_name_all') <br /> @lang('custom.frontend.travel_tourism')
							</h2>
							<!--/.about-btn-->
						</div>
						<!--/.about-us-txt-->
					</div>
					<!--/.single-about-us-->
				</div>
				<!--/.col-->
				<div class="col-sm-0">
					<div class="single-about-us">

					</div>
					<!--/.single-about-us-->
				</div>
				<!--/.col-->
			</div>
			<!--/.row-->
		</div>
		<!--/.about-us-content-->
	</div>
	<!--/.container-->

</section>
<!--/.about-us-->
<!--about-us end -->

<!--travel-box start-->
<!--hidden-->
<section class="travel-box hidden">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="single-travel-boxes">
					<div id="desc-tabs" class="desc-tabs">

						<ul class="nav nav-tabs" role="tablist">

							<li role="presentation">
								<a href="#flights" aria-controls="flights" role="tab" data-toggle="tab">
									<i class="fa fa-plane"></i>
									@lang('custom.frontend.flights') 
								</a>
							</li>
							<!-- 
							<li role="presentation" class="active">
								<a href="#tours" aria-controls="tours" role="tab" data-toggle="tab">
									<i class="fa fa-tree"></i>
									رحلات سياحية
								</a>
							</li> -->

						</ul>

						<!-- Tab panes -->
						<div class="tab-content">

							<div role="tabpanel" class="tab-pane active fade in" id="flights">
								<div class="tab-para">
									<form action="{{route('frontend.flights-results')}}" method="get">
										@csrf
										<div class="trip-circle">
											<div class="single-trip-circle" id="ret_time_radio">
												<input type="radio" id="radio01" name="round_trip" value="round_trip" checked />
												<label for="radio01">
													<span class="round-boarder">
														<span class="round-boarder1"></span>
													</span>@lang('custom.frontend.round_trip') 
												</label>
											</div>
											<!--/.single-trip-circle-->
											<div class="single-trip-circle" id="dep_time_radio">
												<input type="radio" id="radio02" value="go" name="round_trip" />
												<label for="radio02">
													<span class="round-boarder">
														<span class="round-boarder1"></span>
													</span>@lang('custom.frontend.go')
												</label>
											</div>
											<!--/.single-trip-circle-->
										</div>
										<!--/.trip-circle-->
										<div class="row">
											<div class="col-lg-4 col-md-4 col-sm-12">
												<div class="single-tab-select-box">

													<h2>@lang('custom.frontend.from')</h2>

													<div class="travel-select-icon">
														<select class="form-control" name="from_airport" required>

															<option value="" selected disabled>@lang('custom.frontend.choice_airport')</option><!-- /.option-->

															@foreach($airports as $airport)
															@langrtl
															<option value="{{$airport->id}}">{{$airport->name_ar}}</option><!-- /.option-->
															@else
															<option value="{{$airport->id}}">{{$airport->name_en}}</option><!-- /.option-->
															@endlangrtl
															@endforeach

														</select><!-- /.select-->
													</div><!-- /.travel-select-icon -->
												</div>
												<!--/.single-tab-select-box-->
											</div>
											<!--/.col-->

											<div class="col-lg-4 col-md-4 col-sm-12">
												<div class="single-tab-select-box">

													<h2>@lang('custom.frontend.to')</h2>

													<div class="travel-select-icon">
														<select class="form-control" name="to_airport" required>

															<option value="" selected disabled>@lang('custom.frontend.choice_airport')</option><!-- /.option-->

															@foreach($airports as $airport)
															@langrtl
															<option value="{{$airport->id}}">{{$airport->name_ar}}</option><!-- /.option-->
															@else
															<option value="{{$airport->id}}">{{$airport->name_en}}</option><!-- /.option-->
															@endlangrtl
															@endforeach

														</select><!-- /.select-->
													</div><!-- /.travel-select-icon -->

												</div>
												<!--/.single-tab-select-box-->
											</div>
											<!--/.col-->
											<div class="col-lg-3 col-md-3 col-sm-4">
												<div class="single-tab-select-box">

													<h2>@lang('custom.frontend.grade')</h2>
													<div class="travel-select-icon">
														<select class="form-control ">

															<option value="" selected disabled>@lang('custom.frontend.choice_grade')</option><!-- /.option-->

															<option value="Business">@lang('custom.frontend.business')</option><!-- /.option-->

															<option value="Economy">@lang('custom.frontend.economy')</option><!-- /.option-->

														</select><!-- /.select-->
													</div><!-- /.travel-select-icon -->
												</div>
												<!--/.single-tab-select-box-->
											</div>
											<!--/.col-->

										</div>
										<!--/.row-->

										<div class="row">
											<div class="col-lg-2 col-md-3 col-sm-4">
												<div class="single-tab-select-box">
													<h2>@lang('custom.frontend.depature')</h2>
													<div class="travel-check-icon">
														<!-- <form action="#"> -->
														<input type="text" name="depature_time" class="form-control" autocomplete="off" data-toggle="datepicker" required />
														<!-- </form> -->
													</div><!-- /.travel-check-icon -->
												</div>
												<!--/.single-tab-select-box-->
											</div>
											<!--/.col-->

											<div id="dep_time" class="col-lg-2 col-md-3 col-sm-4">
												<div class="single-tab-select-box">
													<h2>@lang('custom.frontend.arrival')</h2>
													<div class="travel-check-icon">
														<!-- <form action="#"> -->
														<input type="text" name="arrival_time" class="form-control" autocomplete="off" data-toggle="datepicker" required />
														<!-- </form> -->
													</div><!-- /.travel-check-icon -->
												</div>
												<!--/.single-tab-select-box-->
											</div>
											<!--/.col-->

											<div class="col-lg-2 col-md-2 col-sm-12">
												<div class="single-tab-select-box">

													<h2>@lang('custom.frontend.adults')</h2>

													<div class="travel-select-icon">
														<select class="form-control" name="adults">

															@for($i = 1; $i < 10 ;$i++) <option value="{{$i}}">{{$i}}</option><!-- /.option-->
																@endfor

														</select><!-- /.select-->
													</div><!-- /.travel-select-icon -->
												</div>
												<!--/.single-tab-select-box-->
											</div>
											<!--/.col-->

											<div class="col-lg-3 col-md-3 col-sm-12">
												<div class="single-tab-select-box">

													<h2>@lang('custom.frontend.children') <span class="small">(@lang('custom.frontend.down_12'))</span></h2>

													<div class="travel-select-icon">
														<select class="form-control" name="children">

															@for($i = 0; $i < 9 ;$i++) <option value="{{$i}}">{{$i}}</option><!-- /.option-->
																@endfor

														</select><!-- /.select-->
													</div><!-- /.travel-select-icon -->
												</div>
												<!--/.single-tab-select-box-->
											</div>
											<!--/.col-->

											<div class="col-lg-3 col-md-3 col-sm-12">
												<div class="single-tab-select-box">

													<h2>@lang('custom.frontend.infants') <span class="small">(@lang('custom.frontend.down_2'))</span></h2>

													<div class="travel-select-icon">
														<select class="form-control" name="infants">

															@for($i = 0; $i < 3 ;$i++) <option value="{{$i}}">{{$i}}</option><!-- /.option-->
																@endfor

														</select><!-- /.select-->
													</div><!-- /.travel-select-icon -->
												</div>
												<!--/.single-tab-select-box-->
											</div>
											<!--/.col-->

											<div class="clo-sm-5">
												<div class="about-btn pull-right">
													<input type="submit" value="{{__('custom.frontend.search')}}" class="about-view travel-btn"></input>
													<!--/.travel-btn-->
												</div>
												<!--/.about-btn-->
											</div>
											<!--/.col-->

										</div>
										<!--/.row-->
									</form>
								</div>

							</div>
							<!--/.tabpannel-->

							<div role="tabpanel" class="tab-pane fade in" id="tours">
								<div class="tab-para">

									<div class="row">
										<div class="col-lg-4 col-md-4 col-sm-12">
											<div class="single-tab-select-box">

												<h2>الوجهات</h2>

												<div class="travel-select-icon">
													<select class="form-control ">

														<option value="default">enter your destination country</option><!-- /.option-->

														<option value="turkey">turkey</option><!-- /.option-->

														<option value="russia">russia</option><!-- /.option-->
														<option value="egept">egypt</option><!-- /.option-->

													</select><!-- /.select-->
												</div><!-- /.travel-select-icon -->

												<div class="travel-select-icon">
													<select class="form-control ">

														<option value="default">enter your destination location</option><!-- /.option-->

														<option value="istambul">istambul</option><!-- /.option-->

														<option value="mosko">mosko</option><!-- /.option-->
														<option value="cairo">cairo</option><!-- /.option-->

													</select><!-- /.select-->
												</div><!-- /.travel-select-icon -->

											</div>
											<!--/.single-tab-select-box-->
										</div>
										<!--/.col-->

										<div class="col-lg-2 col-md-3 col-sm-4">
											<div class="single-tab-select-box">
												<h2>check in</h2>
												<div class="travel-check-icon">
													<form action="#">
														<input type="text" name="check_in" class="form-control" data-toggle="datepicker" placeholder="12 -01 - 2017 ">
													</form>
												</div><!-- /.travel-check-icon -->
											</div>
											<!--/.single-tab-select-box-->
										</div>
										<!--/.col-->

										<div class="col-lg-2 col-md-3 col-sm-4">
											<div class="single-tab-select-box">
												<h2>check out</h2>
												<div class="travel-check-icon">
													<form action="#">
														<input type="text" name="check_out" class="form-control" data-toggle="datepicker" placeholder="22 -01 - 2017 ">
													</form>
												</div><!-- /.travel-check-icon -->
											</div>
											<!--/.single-tab-select-box-->
										</div>
										<!--/.col-->

										<div class="col-lg-2 col-md-1 col-sm-4">
											<div class="single-tab-select-box">
												<h2>duration</h2>
												<div class="travel-select-icon">
													<select class="form-control ">

														<option value="default">5</option><!-- /.option-->

														<option value="10">10</option><!-- /.option-->

														<option value="15">15</option><!-- /.option-->
														<option value="20">20</option><!-- /.option-->

													</select><!-- /.select-->
												</div><!-- /.travel-select-icon -->
											</div>
											<!--/.single-tab-select-box-->
										</div>
										<!--/.col-->

										<div class="col-lg-2 col-md-1 col-sm-4">
											<div class="single-tab-select-box">
												<h2>members</h2>
												<div class="travel-select-icon">
													<select class="form-control ">

														<option value="default">1</option><!-- /.option-->

														<option value="2">2</option><!-- /.option-->

														<option value="4">4</option><!-- /.option-->
														<option value="8">8</option><!-- /.option-->

													</select><!-- /.select-->
												</div><!-- /.travel-select-icon -->
											</div>
											<!--/.single-tab-select-box-->
										</div>
										<!--/.col-->

									</div>
									<!--/.row-->

									<div class="row">
										<div class="col-sm-5">
											<div class="travel-budget">
												<div class="row">
													<div class="col-md-3 col-sm-4">
														<h3>budget : </h3>
													</div>
													<!--/.col-->
													<div class="co-md-9 col-sm-8">
														<div class="travel-filter">
															<div class="info_widget">
																<div class="price_filter">

																	<div id="slider-range"></div>
																	<!--/.slider-range-->

																	<div class="price_slider_amount">
																		<input type="text" id="amount" name="price" placeholder="Add Your Price" />
																	</div>
																	<!--/.price_slider_amount-->
																</div>
																<!--/.price-filter-->
															</div>
															<!--/.info_widget-->
														</div>
														<!--/.travel-filter-->
													</div>
													<!--/.col-->
												</div>
												<!--/.row-->
											</div>
											<!--/.travel-budget-->
										</div>
										<!--/.col-->
										<div class="clo-sm-7">
											<div class="about-btn travel-mrt-0 pull-right">
												<button class="about-view travel-btn">
													بحث
												</button>
												<!--/.travel-btn-->
											</div>
											<!--/.about-btn-->
										</div>
										<!--/.col-->

									</div>
									<!--/.row-->

								</div>
								<!--/.tab-para-->

							</div>
							<!--/.tabpannel-->

						</div>
						<!--/.tab content-->
					</div>
					<!--/.desc-tabs-->
				</div>
				<!--/.single-travel-box-->
			</div>
			<!--/.col-->
		</div>
		<!--/.row-->
	</div>
	<!--/.container-->

</section>
<!--/.travel-box-->
<!--travel-box end-->

<!--service start-->
<section id="service" class="service">
	<div class="container">

		<div class="service-counter text-center">

			<div class="col-md-4 col-sm-4">
				<div class="single-service-box">
					<div class="service-img">
						<img src="img/frontend/user/service/s1.png" alt="service-icon" />
					</div>
					<!--/.service-img-->
					<div class="service-content">
						<h2>
							<p>
							@lang('custom.frontend.our_goal')
							</p>
						</h2>
						<p>@lang('custom.frontend.our_goal_text')</p>
					</div>
					<!--/.service-content-->
				</div>
				<!--/.single-service-box-->
			</div>
			<!--/.col-->

			<div class="col-md-4 col-sm-4">
				<div class="single-service-box">
					<div class="service-img">
						<img src="img/frontend/user/service/s2.png" alt="service-icon" />
					</div>
					<!--/.service-img-->
					<div class="service-content">
						<h2>
							<p>
							@lang('custom.frontend.our_message')
</p>
						</h2>
						<p>@lang('custom.frontend.our_message_text')</p>
					</div>
					<!--/.service-content-->
				</div>
				<!--/.single-service-box-->
			</div>
			<!--/.col-->

			<div class="col-md-4 col-sm-4">
				<div class="single-service-box">
					<div class="statistics-img">
						<img src="img/frontend/user/service/s3.png" alt="service-icon" />
					</div>
					<!--/.service-img-->
					<div class="service-content">

						<h2>
						<p>
						@lang('custom.frontend.our_view')
						</p>
						</h2>
						<p>@lang('custom.frontend.our_view_text')</p>
					</div>
					<!--/.service-content-->
				</div>
				<!--/.single-service-box-->
			</div>
			<!--/.col-->

		</div>
		<!--/.statistics-counter-->
	</div>
	<!--/.container-->

</section>
<!--/.service-->
<!--service end-->

<!--info start-->
<section id="service" class="height-313">
	<div class="fluid-container info-img">
		
	</div>
	<!--/.container-->
</section>
<!--/.service-->
<!--info end-->

<!--galley start-->
<section id="gallery" class="gallery">
	<div class="container">
		<div class="gallery-details">
			<div class="gallary-header text-center">
				<h2>
				@lang('custom.frontend.featured_distination')
				</h2>
				<p>
				@lang('custom.frontend.questions')
				</p>
			</div>
			<!--/.gallery-header-->
			<div class="gallery-box">
				<div class="gallery-content">
					<div class="filtr-container">
						<div class="row">

							<div class="col-md-6 col-xs-12">
								<div class="filtr-item">
									<img src="img/frontend/user/gallary/g1.jpg" alt="portfolio image" />
									<div class="item-title">
										<a href="#">
										@lang('custom.frontend.baghdad')
										</a>
									</div><!-- /.item-title -->
								</div><!-- /.filtr-item -->
							</div><!-- /.col -->

							<div class="col-md-6">
								<div class="filtr-item">
									<img src="img/frontend/user/gallary/g2.jpg" alt="portfolio image" />
									<div class="item-title">
										<a href="#">
										@lang('custom.frontend.yerevan')
										</a>
									</div> <!-- /.item-title-->
								</div><!-- /.filtr-item -->
							</div><!-- /.col -->

							<div class="col-md-4">
								<div class="filtr-item">
									<img src="img/frontend/user/gallary/g3.jpg" alt="portfolio image" />
									<div class="item-title">
										<a href="#">
										@lang('custom.frontend.moscow')
										</a>
									</div><!-- /.item-title -->
								</div><!-- /.filtr-item -->
							</div><!-- /.col -->

							<div class="col-md-4">
								<div class="filtr-item">
									<img src="img/frontend/user/gallary/g4.jpg" alt="portfolio image" />
									<div class="item-title">
										<a href="#">
										@lang('custom.frontend.tehran')
										</a>
									</div> <!-- /.item-title-->
								</div><!-- /.filtr-item -->
							</div><!-- /.col -->

							<div class="col-md-4">
								<div class="filtr-item">
									<img src="img/frontend/user/gallary/g5.jpg" alt="portfolio image" />
									<div class="item-title">
										<a href="#">
										@lang('custom.frontend.erbil')
										</a>
									</div> <!-- /.item-title-->
								</div><!-- /.filtr-item -->
							</div><!-- /.col -->

							<div class="col-md-8">
								<div class="filtr-item">
									<img src="img/frontend/user/gallary/g6.jpg" alt="portfolio image" />
									<div class="item-title">
										<a href="#">
										@lang('custom.frontend.sharjah')
										</a>
									</div> <!-- /.item-title-->
								</div><!-- /.filtr-item -->
							</div><!-- /.col -->

						</div><!-- /.row -->

					</div><!-- /.filtr-container-->
				</div><!-- /.gallery-content -->
			</div>
			<!--/.galley-box-->
		</div>
		<!--/.gallery-details-->
	</div>
	<!--/.container-->

</section>
<!--/.gallery-->
<!--gallery end-->

<!-- testemonial Start -->
<section class="testemonial">
	<div class="container">

		<div class="gallary-header text-center">
			<h2>
			@lang('custom.frontend.top_news')
			</h2>
			<p>
			@lang('custom.frontend.top_news_text')
			</p>

		</div>
		<!--/.gallery-header-->

		<div class="owl-carousel owl-theme" id="testemonial-carousel">

			@foreach($news as $anews)
			<div class="home1-testm item">
				<div class="home1-testm-single text-center">
					<!--/.home1-testm-img-->
					<div class="home1-testm-txt">
						<span class="icon section-icon">
							<i class="fa fa-quote-left" aria-hidden="true"></i>
						</span>
						<p>
							{{$anews->title}}
						</p>
						<h3>
							<a href="">
								{{date("Y / F",strtotime($anews->updated_at))}}
							</a>
						</h3>
					</div>
					<!--/.home1-testm-txt-->
				</div>
				<!--/.home1-testm-single-->

			</div>
			<!--/.item-->
			@endforeach

		</div>
		<!--/.testemonial-carousel-->
	</div>
	<!--/.container-->

</section>
<!--/.testimonial-->
<!-- testemonial End -->

<!--packages start-->
<section id="pack" class="packages">
	<div class="container">
		<div class="gallary-header text-center">
			<h2>
			@lang('custom.frontend.activities')
			</h2>
			<p>
			@lang('custom.frontend.activities_text')
			</p>
		</div>
		<!--/.gallery-header-->
		<div class="packages-content">
			<div class="row">

				@foreach($tours as $tour)
				<div class="col-md-4 col-sm-6">
					<div class="single-package-item">
						<div class="img-container"><img src="{{$tour->img}}" alt="package-place"></div>
						<div class="single-package-item-txt">
							<h3>{{$tour->title}} <span class="pull-right">{{price($tour->price)}}</span></h3>
							<div class="packages-para">
								<p>
									<span>
										<i class="fa fa-angle-right"></i> @lang('custom.frontend.restaurants')
									</span>
									<i class="fa fa-angle-right"></i> @lang('custom.frontend.hotel')
								</p>
							</div>
							<!--/.packages-para-->
							<div class="packages-review">
								<p>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
								</p>
							</div>
							<!--/.packages-review-->
							<div class="about-btn">
								<a class="about-view packages-btn" href="{{route('frontend.single.trip',$tour->title)}}">
								@lang('custom.frontend.details')
								</a>
							</div>
							<!--/.about-btn-->
						</div>
						<!--/.single-package-item-txt-->
					</div>
					<!--/.single-package-item-->

				</div>
				<!--/.col-->
				@endforeach

			</div>
			<!--/.row-->
		</div>
		<!--/.packages-content-->
	</div>
	<!--/.container-->

</section>
<!--/.packages-->
<!--packages end-->


<!--subscribe start-->
<section id="subs" class="subscribe">
	<div class="container">
		<div class="subscribe-title text-center">
			<h2>
			@lang('custom.frontend.help')
			</h2>
			<p>
			@lang('custom.frontend.messages')
			</p>
		</div>
		<form action="{{route('frontend.contact.send')}}" method="post">
			@csrf
			<div class="row">
				<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
					<textarea name="message" class="form-control email-box" rows="12" placeholder="{{__('custom.frontend.message_context')}}"></textarea>
				</div>
			</div>

			<input name="name" class="hidden" value="name" />

			<div class="row">
				<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
					<div class="custom-input-group">
						<input type="email" name="email" class="form-control pad-right" placeholder="{{__('custom.frontend.email')}}">
						<button class="appsLand-btn subscribe-btn">@lang('custom.frontend.send')</button>
						<div class="clearfix"></div>
						<i class="fa fa-envelope"></i>
					</div>
				</div>
			</div>
			@if(config('access.captcha.contact'))
			<div class="row">
				<div class="col">
					@captcha
					{{ html()->hidden('captcha_status', 'true') }}
				</div>
				<!--col-->
			</div>
			<!--row-->
			@endif
		</form>
	</div>

</section>
<!--subscribe end-->

@include('frontend.includes.footer')

<script src="{{asset('js/frontend/jquery.js')}}"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->

<!--modernizr.min.js-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>


<!--bootstrap.min.js-->
<script src="{{asset('js/frontend/bootstrap.min.js')}}"></script>

<!-- bootsnav js -->
<script src="{{asset('js/frontend/bootsnav.js')}}"></script>

<!-- jquery.filterizr.min.js -->
<script src="{{asset('js/frontend/jquery.filterizr.min.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>

<!--jquery-ui.min.js-->
<script src="{{asset('js/frontend/jquery-ui.min.js')}}"></script>

<!-- counter js -->
<script src="{{asset('js/frontend/jquery.counterup.min.js')}}"></script>
<script src="{{asset('js/frontend/waypoints.min.js')}}"></script>

<!--owl.carousel.js-->
<script src="{{asset('js/frontend/owl.carousel.min.js')}}"></script>

<!-- jquery.sticky.js -->
<script src="{{asset('js/frontend/jquery.sticky.js')}}"></script>

<!--datepicker.js-->
<script src="{{asset('js/frontend/datepicker.js')}}"></script>

<!--Custom JS-->
<script src="{{asset('js/frontend/custom.js')}}"></script>

@endsection