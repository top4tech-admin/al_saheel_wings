<!DOCTYPE html>
@langrtl
<html class="no-js" lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="rtl">
@else
<html class="no-js" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@endlangrtl

<head>
	<!-- META DATA -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

	<!--font-family-->
	<link href="https://fonts.googleapis.com/css?family=Rufina:400,700" rel="stylesheet" />

	<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet" />

	<!-- TITLE OF SITE -->
	<title>@yield('title', 'AlSahel-Wings')</title>

	<!-- favicon img -->
	<link rel="shortcut icon" type="image/icon" href="logo/favicon.png" />

	<!--font-awesome.min.css-->
	<link rel="stylesheet" href="{{asset('css/frontend/font-awesome.min.css')}}" />

	<!--animate.css-->
	<link rel="stylesheet" href="{{asset('css/frontend/animate.css')}}" />

	<!--hover.css-->
	<link rel="stylesheet" href="{{asset('css/frontend/hover-min.css')}}">

	<!--datepicker.css-->
	<link rel="stylesheet" href="{{asset('css/frontend/datepicker.css')}}">

	<!--owl.carousel.css-->
	<link rel="stylesheet" href="{{asset('css/frontend/owl.carousel.min.css')}}">
	<link rel="stylesheet" href="{{asset('css/frontend/owl.theme.default.min.css')}}" />

	<!-- range css-->
	<link rel="stylesheet" href="{{asset('css/frontend/jquery-ui.min.css')}}" />

	<!--bootstrap.min.css-->
	<link rel="stylesheet" href="{{asset('css/frontend/bootstrap.min.css')}}" />

	@langrtl
	<link rel="stylesheet" href="{{asset('css/frontend/bootstrap.rtl.full.min.css')}}" />
	@endlangrtl

	<!-- bootsnav -->
	<link rel="stylesheet" href="{{asset('css/frontend/bootsnav.css')}}" />

	<!--style.css-->
	<link rel="stylesheet" href="{{asset('css/frontend/style.css')}}" />

	<!--responsive.css-->
	<link rel="stylesheet" href="{{asset('css/frontend/responsive.css')}}" />

	<!--responsive.css-->
	<link rel="stylesheet" href="{{asset('css/frontend/custom.css')}}" />

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<meta name="description" content="@yield('meta_description', 'AlSahel Wings')">
	<meta name="author" content="@yield('meta_author', 'Top4tech')">
	@yield('meta')
	<!-- Matomo -->
	<script>
		var _paq = window._paq = window._paq || [];
		/* tracker methods like "setCustomDimension" should be called before "trackPageView" */
		_paq.push(['trackPageView']);
		_paq.push(['enableLinkTracking']);
		(function() {
			var u = "//127.0.0.1/matomo/";
			_paq.push(['setTrackerUrl', u + 'matomo.php']);
			_paq.push(['setSiteId', '1']);
			_paq.forEach(function(entry) {
				console.log(entry);
			});
			var d = document,
				g = d.createElement('script'),
				s = d.getElementsByTagName('script')[0];
			g.async = true;
			g.src = u + 'matomo.js';
			s.parentNode.insertBefore(g, s);
		})();
	</script>
	<!-- End Matomo Code -->

</head>

<body>
	@include('includes.partials.read-only')

	<div id="app">
		@include('includes.partials.logged-in-as')
		@include('frontend.includes.nav')


		{{-- @include('includes.partials.messages')--}}
		@yield('content')

	</div><!-- #app -->

	@include('includes.partials.ga')
</body>

</html>