@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . 'flights')

@section('content')
<!--about-us start -->
<section id="home" class="about-us flights-cover">
    <div class="container">
        <div class="about-us-content">
            <div class="row">
                <div class="col-sm-12">
                    <div class="single-about-us">
                        <div class="about-us-txt flights-txt">
                            <h2>
                                @lang('custom.frontend.company_name_all')
                            </h2>
                            <!--/.about-btn-->
                        </div>
                        <!--/.about-us-txt-->
                    </div>
                    <!--/.single-about-us-->
                </div>
                <!--/.col-->
                <div class="col-sm-0">
                    <div class="single-about-us">

                    </div>
                    <!--/.single-about-us-->
                </div>
                <!--/.col-->
            </div>
            <!--/.row-->
        </div>
        <!--/.about-us-content-->
    </div>
    <!--/.container-->

</section>
<!--/.about-us-->
<!--about-us end -->

<div class="container">
    <div class="row mar">
        <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
            @if(!isset($point))
            <div class="alert alert-danger">
                <span class="h3"> @lang('custom.frontend.serial_error').</span>
            </div>
            @endif
        </div>
        <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
            <div class="custom-input-group">
                <form action="{{route('frontend.check.serial_number')}}" method="post">
                    @csrf
                    <input type="password" id="serial_number" name="serial_number" class="form-control pad-right" placeholder="{{__('custom.frontend.enter_serial')}}">
                    <input type="submit" class="appsLand-btn subscribe-btn" value="{{__('custom.frontend.check')}}" />
                </form>
                <div class="clearfix"></div>
                <i id="eye-icon" class="fa fa-eye" style="color:green" onclick="showPassword()"></i>
            </div>
        </div>
        <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 mar">
            @if(isset($point) && $point != false)
            <div class="alert alert-warning">
                <p class="h3">@lang('custom.frontend.Hello') <b>{{$point->name}}</b></p>
                <p class="h3">@lang('custom.frontend.balance') <b>{{$point->point}}</b> @lang('custom.frontend.point')</p>
            </div>
            @endif
        </div>
    </div>
    <div class="row mar">
        <p class="h3">يمكنك صرف نقاطك من خلال احدى هذه الجهات:</p>

        @foreach($partners as $partner)
        <div class="col-md-4 mar">
            <div class="alert alert-danger">
                <img src="{{asset($partner->logo)}}" />
                <p class="h3">{{$partner->name}}</p>
                <p class="h4">هاتف: {{$partner->phone}}</p>
                <p class="h4">عنوان: {{$partner->address}}</p>
            </div>
        </div>
        @endforeach


    </div>
</div>

@include('frontend.includes.footer')

<script src="{{asset('js/frontend/jquery.js')}}"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->

<!--modernizr.min.js-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>


<!--bootstrap.min.js-->
<script src="{{asset('js/frontend/bootstrap.min.js')}}"></script>

<!-- bootsnav js -->
<script src="{{asset('js/frontend/bootsnav.js')}}"></script>

<!-- jquery.filterizr.min.js -->
<script src="{{asset('js/frontend/jquery.filterizr.min.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>

<!--jquery-ui.min.js-->
<script src="{{asset('js/frontend/jquery-ui.min.js')}}"></script>

<!-- counter js -->
<script src="{{asset('js/frontend/jquery.counterup.min.js')}}"></script>
<script src="{{asset('js/frontend/waypoints.min.js')}}"></script>

<!--owl.carousel.js-->
<script src="{{asset('js/frontend/owl.carousel.min.js')}}"></script>

<!-- jquery.sticky.js -->
<script src="{{asset('js/frontend/jquery.sticky.js')}}"></script>

<!--datepicker.js-->
<script src="{{asset('js/frontend/datepicker.js')}}"></script>

<!--Custom JS-->
<script src="{{asset('js/frontend/custom.js')}}"></script>

@endsection