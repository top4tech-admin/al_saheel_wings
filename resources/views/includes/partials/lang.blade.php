
    @foreach(array_keys(config('locale.languages')) as $lang)
        @if($lang != app()->getLocale())
            <a href="{{ url('/lang/'.$lang) }}">@lang('menus.language-picker.langs.'.$lang)</a>
        @endif
    @endforeach

