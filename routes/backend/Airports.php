<?php

// airports Management
Route::group(['namespace' => 'Airports'], function () {
    Route::resource('airports', 'AirportsController', ['except' => ['show']]);

    //For DataTables
    Route::post('airports/get', 'AirportsTableController')->name('airports.get');
});