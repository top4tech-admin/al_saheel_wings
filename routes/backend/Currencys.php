<?php

// Currencys Management
Route::group(['namespace' => 'Currencys'], function () {
    Route::resource('currencys', 'CurrencysController', ['except' => ['show']]);

    //For DataTables
    Route::post('currencys/get', 'CurrencysTableController')->name('currencys.get');
});
