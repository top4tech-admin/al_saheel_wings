<?php

// Flights Management
Route::group(['namespace' => 'Flights'], function () {
    Route::resource('flights', 'FlightsController', ['except' => ['show']]);

    //For DataTables
    Route::post('flights/get', 'FlightsTableController')->name('flights.get');
});
