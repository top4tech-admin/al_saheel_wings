<?php

// Points Management
Route::group(['namespace' => 'Points'], function () {
    Route::resource('points', 'PointsController', ['except' => ['show']]);

    //For DataTables
    Route::post('points/get', 'PointsTableController')->name('points.get');
});
