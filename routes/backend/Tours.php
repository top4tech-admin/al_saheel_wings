<?php

// Tours Management
Route::group(['namespace' => 'Tours'], function () {
    Route::resource('tours', 'ToursController', ['except' => ['show']]);

    //For DataTables
    Route::post('tours/get', 'ToursTableController')->name('tours.get');
});
