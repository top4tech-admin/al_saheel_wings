<?php

// Reservations Management
Route::group(['namespace' => 'Reservations'], function () {
    Route::resource('reservations', 'ReservationsController', ['except' => ['show']]);

    //For DataTables
    Route::post('reservations/get', 'ReservationsTableController')->name('reservations.get');
});
