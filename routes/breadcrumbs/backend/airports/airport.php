<?php

Breadcrumbs::for('admin.airports.index', function ($trail) {
    $trail->push('Airport management', route('admin.airports.index'));
});

Breadcrumbs::for('admin.airports.create', function ($trail) {
    $trail->parent('admin.airports.index');
    $trail->push('Create Airport', route('admin.airports.create'));
});

Breadcrumbs::for('admin.airports.edit', function ($trail, $id) {
    $trail->parent('admin.airports.index');
    $trail->push('Edit Airport', route('admin.airports.edit', $id));
});
