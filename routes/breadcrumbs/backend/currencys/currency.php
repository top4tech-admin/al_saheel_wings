<?php

Breadcrumbs::for('admin.currencys.index', function ($trail) {
    $trail->push('Currencies management', route('admin.currencys.index'));
});

Breadcrumbs::for('admin.currencys.create', function ($trail) {
    $trail->parent('admin.currencys.index');
    $trail->push('Create Currency', route('admin.currencys.create'));
});

Breadcrumbs::for('admin.currencys.edit', function ($trail, $id) {
    $trail->parent('admin.currencys.index');
    $trail->push('Edit Currency', route('admin.currencys.edit', $id));
});
