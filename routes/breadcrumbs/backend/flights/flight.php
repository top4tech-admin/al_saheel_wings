<?php

Breadcrumbs::for('admin.flights.index', function ($trail) {
    $trail->push('Flights management', route('admin.flights.index'));
});

Breadcrumbs::for('admin.flights.create', function ($trail) {
    $trail->parent('admin.flights.index');
    $trail->push('Create Flight', route('admin.flights.create'));
});

Breadcrumbs::for('admin.flights.edit', function ($trail, $id) {
    $trail->parent('admin.flights.index');
    $trail->push('Edit Flight', route('admin.flights.edit', $id));
});
