<?php

Breadcrumbs::for('admin.news.index', function ($trail) {
    $trail->push('News management', route('admin.news.index'));
});

Breadcrumbs::for('admin.news.create', function ($trail) {
    $trail->parent('admin.news.index');
    $trail->push('Create New', route('admin.news.create'));
});

Breadcrumbs::for('admin.news.edit', function ($trail, $id) {
    $trail->parent('admin.news.index');
    $trail->push('Edit New', route('admin.news.edit', $id));
});
