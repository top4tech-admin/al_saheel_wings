<?php

Breadcrumbs::for('admin.points.index', function ($trail) {
    $trail->push('Point management', route('admin.points.index'));
});

Breadcrumbs::for('admin.points.create', function ($trail) {
    $trail->parent('admin.points.index');
    $trail->push('Create Point', route('admin.points.create'));
});

Breadcrumbs::for('admin.points.edit', function ($trail, $id) {
    $trail->parent('admin.points.index');
    $trail->push('Edit Point', route('admin.points.edit', $id));
});
