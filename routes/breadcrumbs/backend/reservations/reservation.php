<?php

Breadcrumbs::for('admin.reservations.index', function ($trail) {
    $trail->push('Reservations management', route('admin.reservations.index'));
});

Breadcrumbs::for('admin.reservations.create', function ($trail) {
    $trail->parent('admin.reservations.index');
    $trail->push('Create Reservation', route('admin.reservations.create'));
});

Breadcrumbs::for('admin.reservations.edit', function ($trail, $id) {
    $trail->parent('admin.reservations.index');
    $trail->push('Edit Reservation', route('admin.reservations.edit', $id));
});
