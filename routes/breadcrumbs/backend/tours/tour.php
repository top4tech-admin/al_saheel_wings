<?php

Breadcrumbs::for('admin.tours.index', function ($trail) {
    $trail->push('Tour management', route('admin.tours.index'));
});

Breadcrumbs::for('admin.tours.create', function ($trail) {
    $trail->parent('admin.tours.index');
    $trail->push('Create Tour', route('admin.tours.create'));
});

Breadcrumbs::for('admin.tours.edit', function ($trail, $id) {
    $trail->parent('admin.tours.index');
    $trail->push('Edit Tour', route('admin.tours.edit', $id));
});
