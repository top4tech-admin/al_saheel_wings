<?php

use App\Http\Controllers\Frontend\ContactController;
use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\Frontend\User\AccountController;
use App\Http\Controllers\Frontend\User\DashboardController;
use App\Http\Controllers\Frontend\User\ProfileController;
use App\Http\Controllers\Backend\Reservations\ReservationsController;

/*
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'.
 */
Route::get('/', [HomeController::class, 'index'])->name('index');
Route::get('contact', [ContactController::class, 'index'])->name('contact');
Route::post('contact/send', [ContactController::class, 'send'])->name('contact.send');

/*
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 * These routes can not be hit if the password is expired
 */
Route::group(['middleware' => ['auth', 'password_expires']], function () {
    Route::group(['namespace' => 'User', 'as' => 'user.'], function () {
        // User Dashboard Specific
        Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');

        // User Account Specific
        Route::get('account', [AccountController::class, 'index'])->name('account');

        // User Profile Specific
        Route::patch('profile/update', [ProfileController::class, 'update'])->name('profile.update');
    });
});


Route::get('/flights-results', [HomeController::class, 'flightsResults'])->name('flights-results');
Route::get('/confirm-steps', [HomeController::class, 'confirmStep'])->name('confirm-steps');


Route::post('/reservations/store', [ReservationsController::class, 'store'])->name('reservations.store');
Route::get("/check-points", [HomeController::class, 'myPoints'])->name('mypoints'); 
Route::get("/about-us", [HomeController::class, 'aboutUs'])->name('about-us'); 
Route::get("/booking-message/{total_price}", [HomeController::class, 'showBookMessage'])->name('booking-message'); //frontend.mypoints
Route::post("/check-points", [HomeController::class, 'checkSerialNumber'])->name('check.serial_number'); //frontend.check.serial_number
Route::get("/tour/{title}", [HomeController::class, 'displayTrip'])->name('single.trip'); //frontend.check.serial_number
