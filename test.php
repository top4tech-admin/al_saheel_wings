<?php

try {

    $myfile = fopen("routes\api.php", "r") or die("Unable to open file!");
    $contents = fread($myfile, filesize("routes\api.php"));
    fclose($myfile);

    $myfile = fopen("routes\api.php", 'w+');
    $txt = "// Auto-Generated:  
    Route::apiResource('yyy', 'uuuuController'); \n";

    $newstr = substr_replace($contents, $txt, -4, 0);
    fwrite($myfile, $newstr);
    fclose($myfile);

	echo  nl2br($contents);
} catch (\Throwable $e) {
	echo $e->getMessage();
} finally {
	if ($myfile) {
		fclose($myfile);
	}
}